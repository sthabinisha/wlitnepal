<?php
/**
 * The Shortcode
 */
function volunteer_testimonial_slider_shortcode( $atts ) {
	
	$output = $posts = $categories = $testimonial_type = $designation = '';
	
	extract( 
		shortcode_atts( 
			array(
				'testimonial_type'			=> 'list',
				'list_column'				=> 'one_column',
				'designation'				=> 'yes',
				'posts' 					=> '3',
				'categories' 				=> 'all',
				'excerpt_length' 			=> '',
				'items'						=> '2',
				'items_scroll' 				=> '1',
				'auto_play' 				=> 'true',					
				'timeout_duration' 			=> '5000',
				'infinite_loop' 			=> 'false',
				'margin' 					=> '30',
				'items_tablet'				=> '2',
				'items_mobile_landscape'	=> '1',
				'items_mobile_portrait'		=> '1',
				'navigation' 				=> 'true',
				'pagination' 				=> 'true',	
			), $atts 
		) 
	);
	
	global $post;
	
	// Slider Configuration
	$data_attr = '';

	if( isset( $items ) && $items != '' ) {
		$data_attr .= ' data-items="' . $items . '" ';
	}
	
	if( isset( $items_scroll ) && $items_scroll != '' ) {
		$data_attr .= ' data-slideby="' . $items_scroll . '" ';
	}
	
	if( isset( $items_tablet ) && $items_tablet != '' ) {
		$data_attr .= ' data-items-tablet="' . $items_tablet . '" ';
	}
	
	if( isset( $items_mobile_landscape ) && $items_mobile_landscape != '' ) {
		$data_attr .= ' data-items-mobile-landscape="' . $items_mobile_landscape . '" ';
	}
	
	if( isset( $items_mobile_portrait ) && $items_mobile_portrait != '' ) {
		$data_attr .= ' data-items-mobile-portrait="' . $items_mobile_portrait . '" ';
	}
	
	if( isset( $auto_play ) && $auto_play != '' ) {
		$data_attr .= ' data-autoplay="' . $auto_play . '" ';
	}
	if( isset( $timeout_duration ) && $timeout_duration != '' ) {
		$data_attr .= ' data-autoplay-timeout="' . $timeout_duration . '" ';
	}
	
	if( isset( $infinite_loop ) && $infinite_loop != '' ) {
		$data_attr .= ' data-loop="' . $infinite_loop . '" ';
	}
	
	if( isset( $margin ) && $margin != '' ) {
		$data_attr .= ' data-margin="' . $margin . '" ';
	}
	
	if( isset( $pagination ) && $pagination != '' ) {
		$data_attr .= ' data-pagination="' . $pagination . '" ';
	}
	if( isset( $navigation ) && $navigation != '' ) {
		$data_attr .= ' data-navigation="' . $navigation . '" ';
	}	
	
	/**
	 * Testimonial Query Args
	 */
	$args = array(
		'post_type' 		=> 'tpath_testimonial',
		'posts_per_page' 	=> $posts,
		'orderby' 		 	=> 'date',
		'order' 		 	=> 'DESC',
	);
		
	if( ! ( $categories == 'all' ) ) {
		
		$category_id = (int)$categories;
		
		$args['tax_query'] = array( array(
								'taxonomy' 	=> 'testimonial_categories',
								'field' 	=> 'id',
								'terms' 	=> $category_id
							) );
	}	
		
	$testimonial_query = new WP_Query( $args );
	
	// Classes
	$main_classes = '';
	$excerpt_limit = '';
	if( isset( $testimonial_type ) && $testimonial_type != '' ) {
		$main_classes .= ' type-' . $testimonial_type;
		
	}
	if( isset( $designation ) && $designation != '' ) {
		$main_classes .= ' designation-' . $designation;
	}
	
	if( isset( $excerpt_length ) && $excerpt_length == '' ) {
		$excerpt_limit = '60';
	} else {
		$excerpt_limit = $excerpt_length;
	}
	
	if( $testimonial_query->have_posts() ) {
		
		if( isset( $testimonial_type ) && $testimonial_type != 'list' ) {

		$output = '<div class="testimonial-slider-wrapper tpath-testimonial'. $main_classes .'">';
			$output .= '<div id="testimonial-slider" class="tpath-owl-carousel owl-carousel testimonial-carousel-slider"'.$data_attr.'>';
		}else {
		$output = '<div class="testimonial-list-wrapper tpath-testimonial  '.$list_column.' '. $main_classes .'">';
			$output .= '<div id="testimonial-list" class="testimonial-list">';
		}
				while($testimonial_query->have_posts()) : $testimonial_query->the_post();
		
				$author_designation = $company_name = $company_url = $company_info = '';
				
				$testimonial_img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'testimonial');	
				$author_designation = get_post_meta( $post->ID, 'volunteer_author_designation', true );
				$company_name = get_post_meta( $post->ID, 'volunteer_author_company_name', true );
				$company_url = get_post_meta( $post->ID, 'volunteer_author_company_url', true );
				if( $company_url != '' ) {
					$company_info = '<span class="author-company"> - <a href="'. esc_url( $company_url ) .'" target="_blank">'. esc_html( $company_name ) .'</a></span>';
				} 
			
				$output .= '<div class="testimonial-item hvr-float-shadow">';
				
					// Author Bottom
						$output .= '<div class="testimonial-author">';
							$output .= '<div class="author-image">';
								$output .= '<img class="img-responsive" src="'.$testimonial_img[0].'" alt="'.get_the_title().'" />';	
							$output .= '</div>';														
							$output .= '<div class="testimonial-detail">';
								$output .= '<div class="testimonial-info">';
									$output .= '<h5 class="client-author-name">'.get_the_title().'</h5>';
									if( $designation == 'yes' ) {
										$output .= '<p class="client-author-info"><span class="author-designation">'.$author_designation.'</span>' . $company_info .'</p>';
									}
								$output .= '</div>';
							$output .= '</div>';
						$output .= '</div>';
				
					// Testimonials Content
					$output .= '<div class="testimonial-content">';
						$output .= '<div class="testimonial-text">';
						$output .= '<i class="fa fa-quote-left"></i>';
							$output .= wp_trim_words(get_the_content(), $excerpt_limit, '');
						$output .= '</div>';
					$output .= '</div>';
					
				$output .= '</div>';

				endwhile;
	
			$output .= '</div>';
		$output .= '</div>';
		
	}
	
	wp_reset_postdata();
	
	return $output;
}
add_shortcode( 'volunteer_vc_testimonial', 'volunteer_testimonial_slider_shortcode' );

/**
 * The VC Element Config Functions
 */
function volunteer_vc_testimonial_shortcode() {
	vc_map( 
		array(
			"icon" 			=> 'tpath-vc-block',
			"name" 			=> esc_html__( 'Testimonial', 'volunteer' ),
			"base" 			=> 'volunteer_vc_testimonial',
			"category" 		=> esc_html__( 'Theme Addons', 'volunteer' ),		
			"params" 		=> array(
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__( "Testimonials Style", "volunteer" ),
					"param_name" 	=> "testimonial_type",
					"value" 		=> array_flip(array(
									'list' 	 => 'List',
									'slider'  => 'Slider',
									)),
				),
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__( "List Column Style", "volunteer" ),
					"param_name" 	=> "list_column",
					"value" 		=> array_flip(array(
									'one_column' 	 => 'One Column',
									'two_column' 	 => 'Two Column',
									)),
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> esc_html__("How Many Posts to Show?", "volunteer"),
					"param_name" 	=> "posts",
					"value" 		=> "3",
					"description" 	=> ""
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> esc_html__("Excerpt Limit", "volunteer"),
					"param_name" 	=> "excerpt_length",
					"value" 		=> "",
					"description" 	=> ""
				),
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__( "Display Author Designation", "volunteer" ),
					"param_name" 	=> "designation",
					"value" 		=> array_flip(array(
									'yes' 	 => 'Yes',
									'no' 	 => 'No'
									)),
				),
				// Slider
				array(
					"type"			=> "textfield",
					"heading"		=> esc_html__( "Items to Display", "volunteer" ),
					"param_name"	=> "items",
					'admin_label'	=> true,
					"group"			=> esc_html__( "Slider", "volunteer" ),
				),
				array(
					"type"			=> "textfield",
					"heading"		=> esc_html__( "Items to Scrollby", "volunteer" ),
					"param_name"	=> "items_scroll",
					"group"			=> esc_html__( "Slider", "volunteer" ),
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> esc_html__( "Auto Play", 'volunteer' ),
					'param_name'	=> "auto_play",
					'admin_label'	=> true,				
					'value'			=> array(
						esc_html__( 'True', 'volunteer' )	=> 'true',
						esc_html__( 'False', 'volunteer' )	=> 'false',
					),
					"group"			=> esc_html__( "Slider", "volunteer" ),
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> esc_html__( 'Timeout Duration (in milliseconds)', 'volunteer' ),
					'param_name'	=> "timeout_duration",
					'value'			=> "5000",
					'dependency'	=> array(
						'element'	=> "auto_play",
						'value'		=> 'true'
					),
					"group"			=> esc_html__( "Slider", "volunteer" ),
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> esc_html__( "Infinite Loop", 'volunteer' ),
					'param_name'	=> "infinite_loop",
					'value'			=> array(
						esc_html__( 'False', 'volunteer' )	=> 'false',
						esc_html__( 'True', 'volunteer' )	=> 'true',
					),
					"group"			=> esc_html__( "Slider", "volunteer" ),
				),
				array(
					"type"			=> "textfield",
					"heading"		=> esc_html__( "Margin ( Items Spacing )", "volunteer" ),
					"param_name"	=> "margin",
					'admin_label'	=> true,
					"group"			=> esc_html__( "Slider", "volunteer" ),
				),
				array(
					"type"			=> "textfield",
					"heading"		=> esc_html__( "Items To Display in Tablet", "volunteer" ),
					"param_name"	=> "items_tablet",
					"group"			=> esc_html__( "Slider", "volunteer" ),
				),
				array(
					"type"			=> "textfield",
					"heading"		=> esc_html__( "Items To Display In Mobile Landscape", "volunteer" ),
					"param_name"	=> "items_mobile_landscape",
					"group"			=> esc_html__( "Slider", "volunteer" ),
				),
				array(
					"type"			=> "textfield",
					"heading"		=> esc_html__( "Items To Display In Mobile Portrait", "volunteer" ),
					"param_name"	=> "items_mobile_portrait",
					"group"			=> esc_html__( "Slider", "volunteer" ),
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Navigation", "volunteer" ),
					"param_name"	=> "navigation",
					"value"			=> array(
						esc_html__( "Yes", "volunteer" )	=> "true",
						esc_html__( "No", "volunteer" )		=> "false" ),
					"group"			=> esc_html__( "Slider", "volunteer" ),
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Pagination", "volunteer" ),
					"param_name"	=> "pagination",
					"value"			=> array(
						esc_html__( "Yes", "volunteer" )	=> "true",
						esc_html__( "No", "volunteer" )		=> "false" ),
					"group"			=> esc_html__( "Slider", "volunteer" ),
				),
			)
		) 
	);
	
}
add_action( 'vc_before_init', 'volunteer_vc_testimonial_shortcode');