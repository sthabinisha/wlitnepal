<?php 
/**
 * The Shortcode  
 */ 
function volunteer_google_map_shortcode( $atts, $content = NULL ) { 
	
	$output = '';
	
	$atts = vc_map_get_attributes( 'volunteer_google_map', $atts );
	extract( $atts );
	
	$addresses = explode('|', $address);
		
	if( $map_overlay == "true" && $hue_color == '' ) {
		$hue_color = "#f06626";
	}
	
	if( isset( $marker_image ) && $marker_image == '' ) {
		$new_marker_image = VOLUNTEER_THEME_URL . '/images/map-marker.png';
	} else if( isset( $marker_image ) && $marker_image != '' ) {
		$marker_image_id = preg_replace( '/[^\d]/', '', $marker_image );
		$new_marker_image_src = wp_get_attachment_image_src( $marker_image_id, 'full' );
		if ( ! empty( $new_marker_image_src[0] ) ) {
			$new_marker_image = $new_marker_image_src[0];
		}
	}
		
	$data_attr = '';
	$data_attr = ' data-type="'. $map_type .'"';
	$data_attr .= ' data-zoom="'. $map_zoom .'"';
	$data_attr .= ' data-scrollwheel="'. $scroll_wheel .'"';
	$data_attr .= ' data-zoomcontrol="'. $zoom_control .'"';
	if( $map_overlay == "true" ) {
		$data_attr .= ' data-hue="'. $hue_color .'"';
	}
	if( $show_marker == "true" ) {
		$data_attr .= ' data-markerimg="'. $new_marker_image .'"';
		$data_attr .= ' data-marker="yes"';
	} else {
		$data_attr .= ' data-marker="no"';
	}	
	$data_attr .= ' data-address="'. $addresses[0] .'"';
	$data_attr .= ' data-addresses="'. $address .'"';
	$data_attr .= ' data-title="'. $title .'"';
	$data_attr .= ' data-content="' . str_replace( '"', "'", $info_content ) .'"';
	
	if( isset( $map_width ) && $map_width != '' ) {
		$map_styles = ' style="width: '.$map_width.'; ';
		if( isset( $map_height ) && $map_height != '' ) {
			$map_styles .= 'height: '.$map_height.';"';
		} else {
			$map_styles .= '"';
		}
	}
	
	$output .= '<div class="gmap-wrapper">';
		$output .= '<div class="gmap_canvas"'. $data_attr .''.$map_styles.'>';
		$output .= '</div>';
	$output .= '</div>';
	
	return $output;
}
add_shortcode( 'volunteer_google_map', 'volunteer_google_map_shortcode' );

/**
 * The VC Element Config Functions
 */
function volunteer_vc_google_map_shortcode() {
	vc_map( 
		array(
			"icon" 			=> 'tpath-vc-block',
			"name" 			=> esc_html__( 'Google Map', 'volunteer' ),
			"base" 			=> 'volunteer_google_map',
			"category" 		=> esc_html__( 'Theme Addons', 'volunteer' ),
			"params" 		=> array(
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__("Map Type", 'volunteer'),
					"param_name" 	=> "map_type",
					"admin_label" 	=> true,
					"value" 		=> array(
						esc_html__( "Roadmap", "volunteer" )	=> "roadmap",
						esc_html__( "Satellite", "volunteer" )	=> "satellite",
						esc_html__( "Hybrid", "volunteer" )		=> "hybrid",
						esc_html__( "Terrain", "volunteer" )	=> "terrain"
					),
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> esc_html__("Map Width", 'volunteer'),
					"param_name" 	=> "map_width",
					"value" 		=> '100%',
					"admin_label" 	=> true,					
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> esc_html__("Map Height", 'volunteer'),
					"param_name" 	=> "map_height",
					"value" 		=> '500px',
					"admin_label" 	=> true,					
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> esc_html__("Map Zoom Level", 'volunteer'),
					"param_name" 	=> "map_zoom",
					"value" 		=> '12',
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Map Scrollwheel", "volunteer" ),
					"param_name"	=> "scroll_wheel",
					"value"			=> array(
						esc_html__( "Yes", "volunteer" )	=> "true",
						esc_html__( "No", "volunteer" )		=> "false",
					),
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Map Zoom Control", "volunteer" ),
					"param_name"	=> "zoom_control",
					"value"			=> array(
						esc_html__( "Yes", "volunteer" )	=> "true",
						esc_html__( "No", "volunteer" )		=> "false",
					),
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Map Overlay", "volunteer" ),
					"param_name"	=> "map_overlay",
					"value"			=> array(
						esc_html__( "Yes", "volunteer" )	=> "true",
						esc_html__( "No", "volunteer" )		=> "false",
					),
				),
				array(
					"type"			=> "colorpicker",
					"heading"		=> esc_html__( "Map Overlay Color", "volunteer" ),
					"param_name"	=> "hue_color",						
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Show Marker", "volunteer" ),
					"param_name"	=> "show_marker",
					"value"			=> array(
						esc_html__( "Yes", "volunteer" )	=> "true",
						esc_html__( "No", "volunteer" )		=> "false",
					),
				),				
				array(
					"type"			=> "attach_image",
					"heading"		=> esc_html__( "Marker Image", "volunteer" ),
					"param_name"	=> "marker_image",
					"value"			=> "",
				),
				array(
					"type"			=> "textarea",
					"heading"		=> esc_html__( "Latitude/ Longtitude", "volunteer" ),
					"param_name"	=> "address",
					'admin_label' 	=> true,
					"description" 	=> esc_html__( "Add latitude/longtitude to show marker on map. To show multiple marker locations on map, to separate latitude/longtitude by using | symbol. <br />Ex: -33.867139, 151.207114|-4.325, 15.322222", "volunteer" ),
					"value"			=> "-33.867139, 151.207114",
				),
				// Content
				array(
					"type"			=> "exploded_textarea",
					"heading"		=> esc_html__( "Title", "volunteer" ),
					"param_name"	=> "title",
					"value" 		=> 'First Marker Title,Second Marker Title',
					"description" 	=> esc_html__( "Enter title for each marker position here. Divide titles with linebreaks (Enter).", "volunteer" ),
					"group"			=> esc_html__( "Content", "volunteer" ),
				),
				array(
					"type"			=> 'textarea',
					"heading"		=> esc_html__( "Content", "volunteer" ),
					"param_name"	=> "info_content",
					"value" 		=> 'First Marker Content|Second Marker Content',
					"description" 	=> esc_html__( "Enter content for each marker position here. Divide content with | and divide new line with ,", "volunteer" ),
					"group"			=> esc_html__( "Content", "volunteer" ),
				),
			)
		) 
	);
}
add_action( 'vc_before_init', 'volunteer_vc_google_map_shortcode' );