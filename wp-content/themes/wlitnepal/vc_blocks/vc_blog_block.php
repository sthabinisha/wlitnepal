<?php 
/**
 * The Shortcode
 */
function volunteer_blog_posts_shortcode( $atts ) {
	
	$output = $posts = $layout = $thumbnail = $readmore = $pagination = $grid_columns = '';
	
	extract( 
		shortcode_atts( 
			array(
				'posts' 			=> '6',
				'layout' 			=> 'large',				
				'pagination' 		=> 'hide',
				'list_fullwidth' 	=> 'no',
				'grid_columns' 		=> 'two'
			), $atts 
		) 
	);
	
	global $volunteer_options, $post;
		
	if( ( is_front_page() || is_home() ) ) {
		$paged = (get_query_var('paged')) ? get_query_var('paged') : ((get_query_var('page')) ? get_query_var('page') : 1);
	} else {
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	}
	
	/**
	 * Posts Query Args
	 */
	$args = array(
				'posts_per_page' 		=> $posts,
				'orderby' 		 		=> 'date',
				'order' 		 		=> 'DESC',
				'paged' 				=> $paged,
			);
		
	$post_class = '';
	$image_size = '';
	$container_class = '';
	$container_id = '';
	$excerpt_limit = '';
	
	if( $pagination == 'infinite' ) {
		$container_id = "tpath-posts-infinite-container";
		$container_class = "tpath-posts-container ";
	} else {
		$container_id = "vc-posts-container";
		$container_class = "tpath-posts-container ";
	}
	
	if($layout == 'large') {
		$post_class = 'large-posts';
		$image_size = 'volunteer-blog-large';
		$container_class .= 'large-layout';
		$excerpt_limit = $volunteer_options['blog_excerpt_length_large'];
	} elseif($layout == 'list') {
		$post_class = 'list-posts';
		$image_size = 'volunteer-blog-list';
		$container_class .= 'list-layout';
		if( isset( $list_fullwidth ) && $list_fullwidth == 'no' ) {
			$container_class .= ' list-columns';
			$excerpt_limit = 15;
		} else {
			$container_class .= ' list-fullwidth';
			$excerpt_limit = 30;
		}
	} elseif($layout == 'grid') {
		$post_class = 'grid-posts';
		$image_size = 'volunteer-blog-list';
		$excerpt_limit = $volunteer_options['blog_excerpt_length_grid'];
		
		if($grid_columns == 'two' ) {
			$container_class .= 'grid-layout grid-col-2';
		} elseif($grid_columns == 'three') {
			$container_class .= 'grid-layout grid-col-3';
		} elseif( $grid_columns == 'four' ) {
			$container_class .= 'grid-layout grid-col-4';
		}
	}
		
	$blog_query = new WP_Query( $args );
											
	if( $blog_query->have_posts() ) {
	
		$output .= '<div id="'.$container_id.'" class="'.$container_class.' clearfix">';
					
			while($blog_query->have_posts()) : $blog_query->the_post();
			
				$post_id = get_the_ID();
				$post_format = get_post_format();
				
				$post_format_class = '';
				if( $post_format == 'image' ) {
					$post_format_class = ' image-format';
				} elseif( $post_format == 'quote' ) {
					$post_format_class = ' quote-image';
				}
				
				$output .= '<article id="post-'.$post_id.'" ';
				ob_start();
					post_class($post_class);
				$output .= ob_get_clean() .'>';
				
				$output .= '<div class="posts-inner-container clearfix">';
					$output .= '<div class="posts-content-container">';
						if ( has_post_thumbnail() && ! post_password_required() ) {
							$output .= volunteer_blog_featured_image( $image_size, $post_format, $layout );													
						}
						$output .= '<div class="post-content">';
							if( $layout != 'list' ) {
							$output .= '<div class="left-content">';
								$output .= volunteer_blog_posted_info();
							$output .= '</div>';
							}
							
							if( $layout != 'list' ) {
								$output .= '<div class="right-content">';
							}
								$output .= '<div class="entry-header">';
									$output .= volunteer_blog_title();
									if( $layout != 'list' ) {
										$output .= volunteer_blog_entry_meta( $layout );
									}
								$output .= '</div>';
								$output .= '<div class="entry-summary">';
									$output .= volunteer_blog_content( $excerpt_limit );
								$output .= '</div>';
								$output .= '<div class="entry-footer">';
									$output .= volunteer_blog_footer();
								$output .= '</div>';
							if( $layout != 'list' ) {
								$output .= '</div>';
							}
						$output .= '</div>';
					$output .= '</div>';
				$output .= '</div>';
							
				$output .= '</article>';

			endwhile;
	
		$output .= '</div>';
		
		if( $pagination != 'hide' ) {
			$output .= volunteer_pagination( $blog_query->max_num_pages, $pagination );
		}
	}
	
	wp_reset_postdata();
	
	return $output;
}
add_shortcode( 'volunteer_vc_blog', 'volunteer_blog_posts_shortcode' );

/**
 * The VC Element Config Functions
 */
function volunteer_vc_blog_shortcode() {
	vc_map( 
		array(
			"icon" 			=> 'tpath-vc-block',
			"name" 			=> esc_html__( 'Blog', 'volunteer' ),
			"base" 			=> 'volunteer_vc_blog',
			"category" 		=> esc_html__( 'Theme Addons', 'volunteer' ),			
			"params" 		=> array(
				array(
					"type" 			=> "textfield",
					"heading" 		=> esc_html__("Posts per Page", "volunteer"),
					"param_name" 	=> "posts",
					"admin_label" 	=> true,
					"value" 		=> "6",
					"description" 	=> ""
				),
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__( "Blog Layout", "volunteer" ),
					"param_name" 	=> "layout",
					"admin_label" 	=> true,
					"value" 		=> array_flip(array(
									'large' => 'Large Posts',
									'list' 	=> 'List Posts',
									'grid'  => 'Grid Posts'
									)),
				),
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__( "Pagination", "volunteer" ),
					"param_name" 	=> "pagination",
					"value" 		=> array_flip(array(
									'hide' 			=> 'Hide',
									'pagination' 	=> 'Pagination',
									'infinite'  	=> 'Infinite Scroll'
									)),
				),
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__( "Show List Layout in Fullwidth ?", "volunteer" ),
					"param_name" 	=> "list_fullwidth",
					"value" 		=> array_flip(array(
									'no' 	=> 'No',
									'yes' 	=> 'Yes'									
									)),
				),
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__( "Number of Columns for Grid Layout", "volunteer" ),
					"param_name" 	=> "grid_columns",
					"value" 		=> array_flip(array(
									'two' 		=> '2 Columns',
									'three'  	=> '3 Columns',
									'four'  	=> '4 Columns'
									)),
				),
			)
		) 
	);
	
}
add_action( 'vc_before_init', 'volunteer_vc_blog_shortcode');