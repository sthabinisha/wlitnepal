<?php
/**
 * The Shortcode
 */
function volunteer_vc_campaign_shortcode( $atts, $content = null ) {
	
	$atts = vc_map_get_attributes( 'volunteer_vc_campaign', $atts );
	extract( $atts );
	
	$default = array(
		'orderby' 	=> 'post_date',
		'number' 	=> 1, 
		'category' 	=> '',
		'creator' 	=> '', 
		'exclude' 	=> '',
		'include_inactive' => false,
		'columns' 	=> 2
	);

	$args = shortcode_atts( $default, $atts, 'volunteer_vc_campaign' );
	$query_args = Charitable_Campaigns_Shortcode::get_campaigns( $args );
	
	$output = '';
	global $post;
	
	// Classes
	$main_classes = 'tpath-campaign-wrapper tpath-vc-campaign-wrapper';
	
	if( isset( $classes ) && $classes != '' ) {
		$main_classes .= ' ' . $classes;
	}	
	
	// Heading style
	if ( isset( $content ) && $content != '' ) {
		$title_styles = '';		
		if( isset( $title_weight ) && $title_weight != '' ) {
			$title_styles = ' style="font-weight: '. $title_weight .';"';
		}
	}
	
	if( $query_args->have_posts() ) :
	
		if( isset( $campaign_timer ) && $campaign_timer == 'yes' ) {
			wp_enqueue_script( 'volunteer-countdown-plugin-js' );
			wp_enqueue_script( 'volunteer-countdown-js' );
		}
		
		$output .= '<div class="'. esc_attr( $main_classes ) .'">';
		
		if( isset( $content ) && $content != '' ) {
			$output .= '<'. $title_type .' class="campaign-widget-title"'. $title_styles .'>';
			$output .= wpb_js_remove_wpautop( $content, true );
			$output .= '</'. $title_type .'>';
		}
		
		$output .= '<div class="tpath-campaign-inner">';
		
		while( $query_args->have_posts() ) : $query_args->the_post();
		
			$campaign = charitable_get_current_campaign();
		
			$output .= '<div id="campaign-'.$post->ID.'" ';
				ob_start();
					post_class( 'campaign-post' );
				$output .= ob_get_clean() .'>';
				
				if( isset( $campaign_image ) && $campaign_image == 'yes' ) {
					$output .= '<div class="row tpath-campaign-row">';
				}
				
				if( isset( $campaign_image ) && $campaign_image == 'yes' ) {
					if ( has_post_thumbnail( $post->ID ) ) : 
						if( isset( $campaign_image_size ) && $campaign_image_size == 'large' ) {
							$image_size = 'volunteer-theme-large';
						} else {
							$image_size = 'volunteer-theme-mid';
						}
						$output .= '<div class="campaign-image-box col-sm-7">';
						$output .= get_the_post_thumbnail( $post->ID, $image_size );
						$output .= '</div>';
					endif;
				}
				
				if( isset( $campaign_image ) && $campaign_image == 'yes' ) {
					$output .= '<div class="campaign-content-box col-sm-5">';
				} else {				
					$output .= '<div class="campaign-content-box">';
				}
				
					if( isset( $campaign_title ) && $campaign_title == 'yes' ) {
						$output .= '<div class="campaign-title-box">';
						$output .= '<a href="'. get_permalink() .'" title="'. get_the_title() .'">';
						$output .= '<h3>'. get_the_title() .'</h3>';
						$output .= '</a>';
						$output .= '</div>';
					}
					
					if( isset( $campaign_description ) && $campaign_description == 'yes' ) {
						$output .= '<div class="campaign-description-box">';
						if( isset( $campaign_desc_type ) && $campaign_desc_type == 'excerpt' ) {
							$output .= $campaign->description;
						} elseif( isset( $campaign_desc_type ) && $campaign_desc_type == 'full_content' ) {
							$output .= get_the_content();
						} elseif( isset( $campaign_desc_type ) && $campaign_desc_type == 'strip_content' ) {
							if( isset( $excerpt_limit ) && $excerpt_limit == '' ) {
								$excerpt_limit = 30;
							}
							$output .= volunteer_custom_wp_trim_excerpt('', $excerpt_limit);
						}
						$output .= '</div>';
					}
					
					if( isset( $campaign_timer ) && $campaign_timer == 'yes' ) {
						$output .= '<div class="campaign-timer-box campaign-time-left">';
							$new_campaign = new Charitable_Campaign( $post );
							
							$campaign_date = $new_campaign->get_end_date( $date_format = 'd/m/Y');
							$campaign_counter  = explode( '/', $campaign_date );
							
							$data_attr = '';		
							$data_attr .= ' data-counter="down" ';
							$data_attr .= ' data-year="'. $campaign_counter[2] .'" ';
							$data_attr .= ' data-month="'. $campaign_counter[1] .'" ';
							$data_attr .= ' data-date="'. $campaign_counter[0] .'" ';
							
							$output .= '<div id="tpath-daycounter-'. $post->ID .'" class="tpath-daycounter tpath-campaign-counter clearfix"'.$data_attr.'></div>';
						$output .= '</div>';
					}
					
					if( ( isset( $campaign_progress_bar ) && $campaign_progress_bar == 'yes' ) && ( isset( $show_button ) && $show_button == 'yes' ) ) {
						$output .= '<div class="campaign-stats-button-wrapper">';
					}
					
					if( isset( $campaign_progress_bar ) && $campaign_progress_bar == 'yes' ) {
						$output .= '<div class="campaign-donation-progress-wrapper">';
							$output .= '<div class="campaign-donation-stats">';
								$output .= $campaign->get_donation_summary();
							$output .= '</div>';
							if ( $campaign->has_goal() ) {
								$output .= '<div class="campaign-progress-bar"><span class="bar" style="width: '. $campaign->get_percent_donated_raw() .'%;"></span></div>';
							}
						$output .= '</div>';
					}
					
					if( isset( $show_button ) && $show_button == 'yes' ) {
						if( ! $campaign->has_ended() ) {
							$output .= '<div class="campaign-donation-button">';
								$output .= '<a class="donate-button btn '. $button_type .' '. $button_skin .'" href="'. charitable_get_permalink( 'campaign_donation_page', array( 'campaign' => $campaign ) ) .'" title="'. esc_attr( sprintf( '%s %s', _x( 'Make a donation to', 'make a donation to campaign', 'volunteer' ), get_the_title( $campaign->ID ) ) ) .'">'. $button_text .'</a>';
							$output .= '</div>';
						}
					}
					
					if( ( isset( $campaign_progress_bar ) && $campaign_progress_bar == 'yes' ) && isset( $show_button ) && $show_button == 'yes' ) {
						$output .= '</div>';
					}
				
				$output .= '</div>';
				
				if( isset( $campaign_image ) && $campaign_image == 'yes' ) {
					$output .= '</div>';
				}
			$output .= '</div>';
		endwhile;
		$output .= '</div>';
		
		wp_reset_postdata();
		
		$output .= '</div>';
	endif;
	
	return $output;
}
add_shortcode( 'volunteer_vc_campaign', 'volunteer_vc_campaign_shortcode' );

/**
 * The VC Element Config Functions
 */
function volunteer_vc_map_campaign_shortcode() {
	vc_map( 
		array(
			"icon" 			=> 'tpath-vc-block',
			"name" 			=> esc_html__( 'Campaign', 'volunteer' ),
			"base" 			=> 'volunteer_vc_campaign',
			"category" 		=> esc_html__( 'Theme Addons', 'volunteer' ),
			"params" 		=> array(
				array(
					'type'			=> 'textfield',
					'admin_label' 	=> true,
					'heading'		=> esc_html__( 'Extra Classes', "volunteer" ),
					'param_name'	=> 'classes',
					'value' 		=> '',
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> esc_html__("Number of Posts", "volunteer"),
					"param_name" 	=> "number",
					"admin_label" 	=> true,
					"value" 		=> "1",
					"description" 	=> ""
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Campaign Orderby", "volunteer" ),
					"param_name"	=> "orderby",
					"admin_label" 	=> true,
					"value"			=> array(
						esc_html__( "Post Date", "volunteer" )	=> "post_date",
						esc_html__( "Popular", "volunteer" )	=> "popular",
						esc_html__( "Ending", "volunteer" )		=> "ending",
					),
				),
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__( "Widget Title Type", "volunteer" ),
					"param_name" 	=> "title_type",
					"value"			=> array(
							"h2"		=> "h2",
							"h3"		=> "h3",
							"h4"		=> "h4",
							"h5"		=> "h5",
							"h6"		=> "h6",
					),
				),
				array(
					"type" 			=> "textarea_html",
					"heading" 		=> esc_html__("Widget Title", "volunteer"),
					"param_name" 	=> "content",
					"value" 		=> '',
					"holder" 		=> 'div',
				),
				array(
					"type"			=> "dropdown",
					"heading"		=> esc_html__( "Widget Title Weight", 'volunteer' ),
					"param_name"	=> "title_weight",
					"value"			=> array(
						esc_html__( "Default", 'volunteer' )	=> '',
						esc_html__( "Normal", 'volunteer' )		=> '400',
						esc_html__( "Semi Bold", 'volunteer' )	=> '600',
						esc_html__( "Bold", 'volunteer' )		=> 'bold',
						esc_html__( "Extra Bold", 'volunteer' )	=> '900',
					),
				),				
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Show Campaign Title", "volunteer" ),
					"param_name"	=> "campaign_title",
					"value"			=> array(
						esc_html__( "Yes", "volunteer" )	=> "yes",
						esc_html__( "No", "volunteer" )		=> "no",
					),
					"group"			=> esc_html__( "Campaign", "volunteer" ),
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Show Campaign Image", "volunteer" ),
					"param_name"	=> "campaign_image",
					"value"			=> array(
						esc_html__( "No", "volunteer" )		=> "no",
						esc_html__( "Yes", "volunteer" )	=> "yes",
					),
					"group"			=> esc_html__( "Campaign", "volunteer" ),
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Campaign Image Size", "volunteer" ),
					"param_name"	=> "campaign_image_size",
					"value"			=> array(
						esc_html__( "Default", "volunteer" )	=> "default",
						esc_html__( "Large", "volunteer" )		=> "large",
					),
					"group"			=> esc_html__( "Campaign", "volunteer" ),
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Show Campaign Description", "volunteer" ),
					"param_name"	=> "campaign_description",
					"value"			=> array(
						esc_html__( "Yes", "volunteer" )	=> "yes",
						esc_html__( "No", "volunteer" )		=> "no",
					),
					"group"			=> esc_html__( "Campaign", "volunteer" ),
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Campaign Description", "volunteer" ),
					"param_name"	=> "campaign_desc_type",
					"value"			=> array(
						esc_html__( "Excerpt", "volunteer" )			=> "excerpt",
						esc_html__( "Stripped Content", "volunteer" )	=> "strip_content",
						esc_html__( "Full Content", "volunteer" )		=> "full_content",
					),
					"group"			=> esc_html__( "Campaign", "volunteer" ),
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> esc_html__("Excerpt Limit", 'volunteer'),
					"param_name" 	=> "excerpt_limit",
					"value" 		=> '',
					"group" 		=> esc_html__( "Campaign", "volunteer" ),
					'dependency' 	=> array(
						'element' 	=> 'campaign_desc_type',
						'value' 	=> array( 'strip_content' ),
					),
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Show Campaign Timer", "volunteer" ),
					"param_name"	=> "campaign_timer",
					"admin_label" 	=> true,
					"value"			=> array(
						esc_html__( "No", "volunteer" )		=> "no",
						esc_html__( "Yes", "volunteer" )	=> "yes",
					),
					"group"			=> esc_html__( "Campaign", "volunteer" ),
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Show Campaign Progress Bar", "volunteer" ),
					"param_name"	=> "campaign_progress_bar",
					"value"			=> array(
						esc_html__( "Yes", "volunteer" )	=> "yes",
						esc_html__( "No", "volunteer" )		=> "no",
					),
					"group"			=> esc_html__( "Campaign", "volunteer" ),
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Show Button", "volunteer" ),
					"param_name"	=> "show_button",
					"value"			=> array(
						esc_html__( "Yes", "volunteer" )	=> "yes",
						esc_html__( "No", "volunteer" )		=> "no",
					),
					"group"			=> esc_html__( "Campaign", "volunteer" ),
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> esc_html__("Button Text", 'volunteer'),
					"param_name" 	=> "button_text",
					"value" 		=> esc_html__( "Donate", "volunteer" ),
					"group" 		=> esc_html__( "Campaign", "volunteer" ),
				),
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__("Button Type", 'volunteer'),
					"param_name" 	=> "button_type",
					"value" 		=> array_flip(array(
									'btn-style-outline' 		 => 'Outline Style',
									'btn-style-color'	 		 => 'Background Style',
									)),
					'group' 		=> esc_html__( "Campaign", "volunteer" ),
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Button Skin", "volunteer" ),
					"param_name"	=> "button_skin",
					"value"			=> array(
						esc_html__( "Default", "volunteer" )		=> "btn-skin-default",
						esc_html__( "White Color", "volunteer" )	=> "btn-white",
						esc_html__( "Dark Color", "volunteer" )		=> "btn-skin-dark",
						esc_html__( "Theme Color", "volunteer" )	=> "btn-skin-theme",
					),
					'group' 		=> esc_html__( "Campaign", "volunteer" ),
				),
			)
		) 
	);
}
add_action( 'vc_before_init', 'volunteer_vc_map_campaign_shortcode' );