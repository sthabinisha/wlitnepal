<?php
/**
 * The Shortcode
 */
function volunteer_section_title_shortcode( $atts, $content = null ) {

	$atts = vc_map_get_attributes( 'volunteer_section_title', $atts );
	extract( $atts );
		
	$output = '';
	static $title_id = 1;
	
	// Heading style
	if ( isset( $title ) && $title != '' ) {
		$title_class = '';
		if( isset( $title_transform ) && $title_transform != '' ) {
			$title_class = ' text-' . strtolower( $title_transform );
		}
		if( isset( $border_options ) && $border_options != '' ) {
			$title_class .= ' title-' . strtolower( $border_options );
		}
		$title_styles = $title_stylings = '';
		if( isset( $title_size ) && $title_size != '' ) {
			$title_stylings = 'font-size: '. $title_size .'; '; 
		}
		if( isset( $title_weight ) && $title_weight != '' ) {
			$title_stylings .= 'font-weight: '. $title_weight .'; ';
		}
		if( isset( $title_color ) && $title_color != '' ) {
			$title_stylings .= 'color: '. $title_color .'; ';
		}
		if( ( isset( $border_color ) && $border_color != '' ) && $border_options == 'fullwidth_border' ) {
			$title_stylings .= 'border-color: '. $border_color .'; ';
		}
		if( isset( $title_stylings ) && $title_stylings != '' ) {
			$title_styles = ' style="'. $title_stylings .'"';
		}
	}
	
	if ( isset( $sub_title ) && $sub_title != '' ) {
		$sub_title_styles = '';		
		if( isset( $subtitle_color ) && $subtitle_color != '' ) {
			$sub_title_styles .= ' style="color: '. $subtitle_color .';"';
		}
	}
	
	$border_styles = '';		
	if( isset( $border_color ) && $border_color != '' ) {
		$border_styles .= ' style="background: '. $border_color .';"';
	}
	
	$main_title_styles = '';
	if( isset( $margin_bottom ) && $margin_bottom != '' ) {
		$main_title_styles .= ' style="margin-bottom: '. $margin_bottom .';"';
	}
	
	$extra_classes = '';
	if( isset( $classes ) && $classes != '' ) {
		$extra_classes .= ' ' . $classes;
	}
	
	$extra_classes .= volunteer_vc_animation( $css_animation );
	
	$output .= '<div class="tpath-section-title clearfix text-'. $alignment .''.$extra_classes.'"'. $main_title_styles .'>';
		if( isset( $title ) && $title != '' ) {
			$output .= '<div class="section-heading border-' . strtolower( $border_options ).'">';
				$output .= '<'.$heading_type.' class="section-title'. $title_class .'"'. $title_styles .'>';
					$output .= do_shortcode( $title );
				$output .=  '</'.$heading_type.'>';
				
				if( isset( $sub_title ) && $sub_title != '' ) {
					$output .= '<div class="section-sub-title"'. $sub_title_styles .'> '.$sub_title.'</div>';
				}

				if( isset( $border_options ) && $border_options == 'small_border' ) {
					$output .= '<span class="miniborder"'. $border_styles .'></span>';
				}
			$output .=  '</div>';
		}
	$output .= '</div>';
	
	return $output;
}
add_shortcode( 'volunteer_section_title', 'volunteer_section_title_shortcode' );

/**
 * The VC Element Config Functions
 */
function volunteer_vc_section_title_shortcode() {
	vc_map( 
		array(
			"icon" 			=> 'tpath-vc-block',
			"name" 			=> esc_html__("Section Title", "volunteer"),
			"base" 			=> "volunteer_section_title",
			"category" 		=> esc_html__("Theme Addons", "volunteer"),
			"params" 		=> array(
				array(
					'type'			=> 'textfield',
					'admin_label' 	=> true,
					'heading'		=> esc_html__( 'Extra Class', "volunteer" ),
					'param_name'	=> 'classes',
					'value' 		=> '',
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "CSS Animation", "volunteer" ),
					"param_name"	=> "css_animation",
					"value"			=> array(
						esc_html__( "No", "volunteer" )						=> '',
						esc_html__( "Top to bottom", "volunteer" )			=> "top-to-bottom",
						esc_html__( "Bottom to top", "volunteer" )			=> "bottom-to-top",
						esc_html__( "Left to right", "volunteer" )			=> "left-to-right",
						esc_html__( "Right to left", "volunteer" )			=> "right-to-left",
						esc_html__( "Appear from center", "volunteer" )		=> "appear" ),
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Heading Type", 'volunteer' ),
					"param_name"	=> "heading_type",
					"value"			=> array(
						"h2"		=> "h2",
						"h3"		=> "h3",
						"h4"		=> "h4",
						"h5"		=> "h5",
						"h6"		=> "h6",
						"div"		=> "div",
					),
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> esc_html__("Title", "volunteer"),
					"param_name" 	=> "title",
					"admin_label" 	=> true,
					"description" 	=> 'Enter text between strong tag for text Highlight',
					"value"			=> "Sample Heading",
				),
				array(
					"type"			=> "dropdown",
					"heading"		=> esc_html__( "Heading Transform", 'volunteer' ),
					"param_name"	=> "title_transform",
					"value"			=> array(
						esc_html__( "Default", 'volunteer' )	=> '',
						esc_html__( "None", 'volunteer' )		=> 'transform-none',
						esc_html__( "Capitalize", 'volunteer' )	=> 'capitalize',
						esc_html__( "Uppercase", 'volunteer' )	=> 'uppercase',
						esc_html__( "Lowercase", 'volunteer' )	=> 'lowercase',
					),					
				),
				array(
					"type"			=> "textfield",
					"heading"		=> esc_html__( "Title Font Size", "volunteer" ),
					"param_name"	=> "title_size",
					"description" 	=> esc_html__( "Enter Title Font Size. Ex: 24px", "volunteer" ),
				),
				array(
					"type"			=> "dropdown",
					"heading"		=> esc_html__( "Title Weight", 'volunteer' ),
					"param_name"	=> "title_weight",
					"value"			=> array(
						esc_html__( "Default", 'volunteer' )	=> '',
						esc_html__( "Normal", 'volunteer' )		=> '400',
						esc_html__( "Semi Bold", 'volunteer' )	=> '600',
						esc_html__( "Bold", 'volunteer' )		=> 'bold',
						esc_html__( "Extra Bold", 'volunteer' )	=> '900',
					),
				),	
				array(
					"type" 			=> "textfield",
					"heading" 		=> esc_html__("Sub Title", "volunteer"),
					"param_name" 	=> "sub_title",
					"value" 		=> '',
				),
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__("Alignment", "volunteer"),
					"param_name" 	=> "alignment",
					"value" 		=> array_flip(array(
									'left' 	 => 'Left',
									'right'  => 'Right',
									'center' => 'Center',
									)),
					"group" 		=> esc_html__( "Design", "volunteer" ),
				),
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__("Border Style", "volunteer"),
					"param_name" 	=> "border_options",
					"value" 		=> array(
						esc_html__('None', 'volunteer') 				=> 'border_none',
						esc_html__('Bottom Border', 'volunteer') 		=> 'fullwidth_border',
						esc_html__('Small Bottom Border', 'volunteer') 	=> 'small_border',
									),
					"group" 		=> esc_html__( "Design", "volunteer" ),
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> esc_html__("Margin Bottom", "volunteer"),
					"param_name" 	=> "margin_bottom",
					"value" 		=> '',
					"description" 	=> 'Example: 10px or 20px.',
					"group" 		=> esc_html__( "Design", "volunteer" ),
				),			
				array(
					"type" 			=> "colorpicker",
					"heading" 		=> esc_html__("Title Color", "volunteer"),
					"param_name" 	=> "title_color",
					"value" 		=> '',
					"group" 		=> esc_html__( "Design", "volunteer" ),
				),
				array(
					"type" 			=> "colorpicker",
					"heading" 		=> esc_html__("Sub Title Color", "volunteer"),
					"param_name" 	=> "subtitle_color",
					"value" 		=> '',
					"group" 		=> esc_html__( "Design", "volunteer" ),
				),
				array(
					"type" 			=> "colorpicker",
					"heading" 		=> esc_html__("Border Color", "volunteer"),
					"param_name" 	=> "border_color",
					"value" 		=> '',
					"group" 		=> esc_html__( "Design", "volunteer" ),
				),
			)
		) 
	);
}
add_action( 'vc_before_init', 'volunteer_vc_section_title_shortcode' );