<?php 
/**
 * Pricing Table shortcode
 */

if ( ! function_exists( 'volunteer_vc_pricing_table_shortcode' ) ) {
	function volunteer_vc_pricing_table_shortcode( $atts, $content = NULL ) {
		
		extract( 
			shortcode_atts( 
				array(
					'classes'				=> '',
					'css_animation'			=> '',
					'plan'					=> esc_html__( 'Volunteer', 'volunteer' ),
					'plan_color' 			=> '',
					'pricetext_color'		=> '',
					'cost_before' 			=> '$',
					'cost' 					=> '99.9',
					'cost_per' 				=> '/MO',
					'show_icon' 			=> '',
					'type' 					=> '',
					'icon_flaticons' 		=> '',
					'icon_fontawesome' 		=> '',
					'icon_lineicons' 		=> '',
					'icon_icomoonicons' 	=> '',
					'button_url' 			=> '#',
					'button_text' 			=> 'Sign Up'
				), $atts 
			) 
		);
		
		global $post;
	
		static $tpath_pricing_table = 1;

		$output = '';
		
		$extra_classes = '';
		if( isset( $classes ) && $classes != '' ) {
			$extra_classes .= ' ' . $classes;
		}
		
		$extra_classes .= volunteer_vc_animation( $css_animation );

		$plan_style = '';
		if( isset( $plan_color ) && $plan_color != '' ) {
			$header_bgstyle = 'background-color: '. $plan_color .';';
			$border_color = 'border-color: '. $plan_color .';';
			$text_color = 'color: '. $plan_color .';';
		}
		if (isset($pricetext_color) && ($pricetext_color != '')) {
			$price_text = 'color:'.$pricetext_color.';';
		}
		
		$styles = '';
		
		if( $pricetext_color != '' ) {
			$styles .= '<style type="text/css">';		
				$styles .= sprintf( '#tpath-pricing-table-%s .table-cost span.pricing-title {%s}', $tpath_pricing_table, $price_text ) . "\n";
			$styles .= '</style>';
		}
		if( $plan_color != '' ) {
			$styles .= '<style type="text/css">';				
			$styles .= sprintf( '#tpath-pricing-table-%s .tpath-pricing-item .table-header {%s}', $tpath_pricing_table, $header_bgstyle ) . "\n";
				$styles .= sprintf( '#tpath-pricing-table-%s .pricing-plan-list.pricing-box:hover {%s}', $tpath_pricing_table, $border_color ) . "\n";
				$styles .= sprintf( '#tpath-pricing-table-%s .table-divider .pricing-line {%s}', $tpath_pricing_table, $header_bgstyle ) . "\n";
				$styles .= sprintf( '#tpath-pricing-table-%s .table-button .btn {%s}', $tpath_pricing_table, $header_bgstyle ) . "\n";
				$styles .= sprintf( '#tpath-pricing-table-%s .tpath-pricing-item .table-cost sup {%s}', $tpath_pricing_table, $text_color ) . "\n";
			$styles .= '</style>';
		}
			
		$output .= $styles;
		 
		// Pricing Column  
		$output .= '<div id="tpath-pricing-table-'. $tpath_pricing_table .'" class="tpath-pricing-table-wrapper vc-pricing-table'.$extra_classes.'">';			
			// Pricing Item
			$output .= '<div class="tpath-pricing-item">';			
				$output .= '<div class="pricing-plan-list pricing-box">';
					$output .= '<div class="table-header">'; // Pricing Title
						if( isset( $plan ) && $plan != '' ) {
							$output .= '<h4 class="pricing-title">'. $plan .'</h4>';
						}
					$output .= '</div>';
					$output .= '<div class="table-cost">'; // Pricing Cost
						if( isset( $cost ) && $cost != '' ) {
							$output .= '<sup>'. $cost_before .'</sup>';
								$output .= '<span class="pricing-title">'. $cost .'</span>';
							$output .= '<sub>'. $cost_per .'</sub>';
						}
					$output .= '</div>';
					$output .= '<div class="table-divider">';
						$output .= '<span class="pricing-line"></span>'; // Pricing Line
					$output .= '</div>';
					$output .= '<div class="table-features">'; // Pricing Features
							$output .= wpb_js_remove_wpautop( $content, true );
					$output .= '</div>';
					$output .= '<div class="table-button tpath-input-submit">'; // Pricing Button
						// Button URL
							$btn_link = $btn_title = $btn_target = '';
							if( $button_url && $button_url != '' ) {
								$link = vc_build_link( $button_url );
								$btn_link = isset( $link['url'] ) ? $link['url'] : '';
								$btn_title = isset( $link['title'] ) ? $link['title'] : '';
								$btn_target = isset( $link['target'] ) ? $link['target'] : '';
							}
							if( isset( $btn_link ) && $btn_link != '' ) {					
								$output .= '<a href="'. esc_url( $btn_link ) .'" title="'. esc_attr( $btn_title ) .'" target="'. esc_attr( $btn_target ) .'" class="btn btn_bgcolor btn-default tpath-submit btn-pricing">';
								$output .= $button_text;
								if( isset( ${'icon_'. $type} ) && ${'icon_'. $type} != '' ) {
										$output .= '<i class="'. esc_attr( ${'icon_'. $type} ) .' pricing-icon"></i>';
								}
								$output .= '</a>';
							}
					$output .= '</div>';
					
				$output .= '</div>';
			$output .= '</div>';
		$output .= '</div>';
				
		$tpath_pricing_table++;
		
		return $output;
	}
}
add_shortcode( 'volunteer_vc_pricing_table', 'volunteer_vc_pricing_table_shortcode' );

if ( ! function_exists( 'volunteer_vc_pricing_table_shortcode_map' ) ) {
	function volunteer_vc_pricing_table_shortcode_map() {
		
		vc_map( 
			array(
				"name"					=> esc_html__( "Pricing Table", "volunteer" ),
				"base"					=> "volunteer_vc_pricing_table",
				"category"				=> esc_html__( "Theme Addons", "volunteer" ),
				"icon"					=> "vc_element-icon tpath-vc-block",
				"params"				=> array(					
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Pricing Plan', 'volunteer' ),
						'param_name'	=> 'plan',
						'admin_label' 	=> true,
						'group'			=> esc_html__( 'Plan', 'volunteer' ),
						'std'			=> esc_html__( 'Volunteer', 'volunteer' ),
					),
					array(
						'type'			=> 'colorpicker',
						'heading'		=> esc_html__( 'Pricing Table Color', 'volunteer' ),
						'param_name'	=> 'plan_color',
						'group'			=> esc_html__( 'Plan', 'volunteer' ),
					),
					array(
						'type'			=> 'colorpicker',
						'heading'		=> esc_html__( 'Price Text Color', 'volunteer' ),
						'param_name'	=> 'pricetext_color',
						'group'			=> esc_html__( 'Plan', 'volunteer' ),
					),
					// Cost
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Cost Symbol', 'volunteer' ),
						'param_name'	=> 'cost_before',
						'std'			=> '$',
						'group'			=> esc_html__( 'Plan', 'volunteer' ),
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Cost', 'volunteer' ),
						'param_name'	=> 'cost',
						'admin_label' 	=> true,
						'group'			=> esc_html__( 'Plan', 'volunteer' ),
						'std'			=> '99.9',
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Per Text', 'volunteer' ),
						'param_name'	=> 'cost_per',
						'std'			=> '/MO',
						'group'			=> esc_html__( 'Plan', 'volunteer' ),
					),
					// Features
					array(
						'type'			=> 'textarea_html',
						'heading'		=> esc_html__( 'Features', 'volunteer' ),
						'param_name'	=> 'content',
						'value'			=> '<ul>
												<li>daily food donations</li>
												<li>1x person help</li>
												<li>unlimted spending</li>
												<li>online dashboard</li>
											</ul>',
						'description'	=> esc_html__('Enter your pricing content.', 'volunteer'),
						'group'			=> esc_html__( 'Features', 'volunteer' ),
					),
					// Button
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( "Button Text", "volunteer" ),
						"param_name"	=> "button_text",
						'std'			=> 'Sign up',
						"group"			=> esc_html__( "Button", "volunteer" )
					),
					array(
						"type"			=> "vc_link",
						"heading"		=> esc_html__( "Button URL", "volunteer" ),
						"param_name"	=> "button_url",
						"value"			=> "",
						'std'			=> '#',
						"group"			=> esc_html__( "Button", "volunteer" ),
					),
					array(
						'type'			=> 'checkbox',
						'heading'		=> esc_html__( 'Show Icon?', 'volunteer' ),
						'param_name'	=> 'show_icon',
						'description'	=> esc_html__( 'Check this box to show icon for this pricing table.', 'volunteer' ),
						'value'			=> array(
							esc_html__( 'Yes, please', 'volunteer' )	=> 'yes'
						),
						'group'			=> esc_html__( 'Button', 'volunteer' ),
					),
					array(
						'type' 			=> "dropdown",
						'heading' 		=> esc_html__( "Choose from Icon library", "volunteer" ),
						'value' 		=> array(
							esc_html__( 'Flaticons', 'volunteer' ) 		=> 'flaticons',
							esc_html__( 'Font Awesome', 'volunteer' ) 	=> 'fontawesome',
							esc_html__( 'Lineicons', 'volunteer' ) 		=> 'lineicons',						
						),
						'admin_label' 	=> true,
						'param_name' 	=> 'type',
						'description' 	=> esc_html__( "Select icon library.", "volunteer" ),
						"dependency" 	=> array(
							"element" 	=> "show_icon",
							"value" 	=> "yes",
						),
						'group' 	=> 'Button',
					),
					array(
						'type' 			=> 'iconpicker',
						'heading' 		=> esc_html__( 'Icon', 'volunteer' ),
						'param_name' 	=> 'icon_flaticons',
						'value' 		=> '', // default value to backend editor admin_label
						'settings' 		=> array(
							'emptyIcon' 	=> true, // default true, display an "EMPTY" icon?
							'type' 			=> 'flaticons',
							'iconsPerPage' 	=> 4000, // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
						),
						'dependency' 	=> array(
							'element' 	=> 'type',
							'value' 	=> 'flaticons',
						),
						'description' 	=> esc_html__( 'Select icon from library.', 'volunteer' ),
						'group' 		=> 'Button',
					),
					array(
						'type' 			=> 'iconpicker',
						'heading' 		=> esc_html__( 'Icon', 'volunteer' ),
						'param_name' 	=> 'icon_fontawesome',
						'value' 		=> '', // default value to backend editor admin_label
						'settings' 		=> array(
							'emptyIcon' 	=> true, // default true, display an "EMPTY" icon?
							'iconsPerPage' 	=> 4000, // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
						),
						'dependency' 	=> array(
							'element' 	=> 'type',
							'value' 	=> 'fontawesome',
						),
						'description' 	=> esc_html__( 'Select icon from library.', 'volunteer' ),
						'group' 		=> 'Button',
					),				
					array(
						'type' 			=> 'iconpicker',
						'heading' 		=> esc_html__( 'Icon', 'volunteer' ),
						'param_name' 	=> 'icon_lineicons',
						'value' 		=> '', // default value to backend editor admin_label
						'settings' 		=> array(
							'emptyIcon' 	=> true, // default true, display an "EMPTY" icon?
							'type' 			=> 'simpleicons',
							'iconsPerPage' 	=> 4000, // default 100, how many icons per/page to display
						),
						'dependency' 	=> array(
							'element' 	=> 'type',
							'value' 	=> 'lineicons',
						),
						'description' 	=> esc_html__( 'Select icon from library.', 'volunteer' ),
						'group' 		=> 'Button',
					),
					array(
						'type'			=> 'textfield',
						'admin_label' 	=> true,
						'heading'		=> esc_html__( 'Extra Class', "volunteer" ),
						'param_name'	=> 'classes',
						'value' 		=> '',
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "CSS Animation", "volunteer" ),
						"param_name"	=> "css_animation",
						"value"			=> array(
							esc_html__( "No", "volunteer" )						=> '',
							esc_html__( "Top to bottom", "volunteer" )			=> "top-to-bottom",
							esc_html__( "Bottom to top", "volunteer" )			=> "bottom-to-top",
							esc_html__( "Left to right", "volunteer" )			=> "left-to-right",
							esc_html__( "Right to left", "volunteer" )			=> "right-to-left",
							esc_html__( "Appear from center", "volunteer" )		=> "appear" ),
					),
				)
			) 
		);
	}
}
add_action( 'vc_before_init', 'volunteer_vc_pricing_table_shortcode_map' );