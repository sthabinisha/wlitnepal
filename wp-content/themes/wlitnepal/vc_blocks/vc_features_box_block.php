<?php
/**
 * The Shortcode
 */
function volunteer_features_box_shortcode( $atts, $content = null ) {
	
	$atts = vc_map_get_attributes( 'volunteer_feature_box', $atts );
	extract( $atts );
	
	static $tpath_feature_box = 1;
	$output = '';
	
	// Classes
	$main_classes = 'tpath-feature-box feature-box-style';
	
	if( isset( $classes ) && $classes != '' ) {
		$main_classes .= ' ' . $classes;
	}
	
	if( isset( $box_style ) && $box_style != '' ) {
		$main_classes .= ' style-' . $box_style;
	}
	
	// Icon Shape Class
	$icon_extra_class = '';
	$icon_wrapper_class = '';
	if( isset( ${'icon_'. $type} ) && ${'icon_'. $type} != '' ) {
		if( isset( $icon_shape ) && $icon_shape != 'iconfb-none' ) {
			$icon_extra_class = ' icon-shape';
			$icon_wrapper_class = ' feature-icon-shape';
		}		
		$icon_wrapper_class .= ' feature-icon-'.$icon_size.' feature-'.$icon_shape.'';
	}
	
	// Heading style
	if ( isset( $title ) && $title != '' ) {
		// Heading URL
		$title_link = $link_title = $link_target = '';
		if( $title_url && $title_url != '' ) {
			$link = vc_build_link( $title_url );
			$title_link = isset( $link['url'] ) ? $link['url'] : '';
			$link_title = isset( $link['title'] ) ? $link['title'] : '';
			$link_target = !empty( $link['target'] ) ? $link['target'] : '_self';
		}
		
		$title_class = '';
		if( isset( $title_transform ) && $title_transform != '' ) {
			$title_class = ' text-' . strtolower( $title_transform );
		}
		if( isset( $border_options ) && $border_options != '' ) {
			$title_class .= ' title-' . strtolower( $border_options );
		}
		
		$title_styles = $title_stylings = '';
		$title_link_styles = $title_link_stylings = '';
		if( isset( $title_size ) && $title_size != '' ) {
			$title_link_stylings = 'font-size: '. $title_size .'; ';
		}
		if( isset( $title_weight ) && $title_weight != '' ) {
			$title_link_stylings .= 'font-weight: '. $title_weight .'; ';
		}
		if( isset( $title_color ) && $title_color != '' ) {
			$title_link_stylings .= 'color: '. $title_color .'; ';
		}
				
		if( isset( $title_link_stylings ) && $title_link_stylings != '' ) {
			$title_link_styles = ' style="'. $title_link_stylings .'"';
			$title_stylings = $title_link_stylings;
		}
		if( isset( $title_margin ) && $title_margin != '' ) {
			$title_stylings .= 'margin-bottom: '. $title_margin .'; ';
		}
		
		if( isset( $title_stylings ) && $title_stylings != '' ) {
			$title_styles = ' style="'. $title_stylings .'"';
		}
	}
	
	// Content Styles
	$content_styles = $content_stylings = '';
	if( isset( $content ) && $content != '' ) {
		if( isset( $content_size ) && $content_size != '' ) {
			$content_stylings = 'font-size: '. $content_size .'; ';
		}
		if( isset( $content_weight ) && $content_weight != '' ) {
			$content_stylings .= 'font-weight: '. $content_weight .'; ';
		}
		if( isset( $content_color ) && $content_color != '' ) {
			$content_stylings .= 'color: '. $content_color .';';
		}
		if( isset( $content_stylings ) && $content_stylings != '' ) {
			$content_styles = ' style="'. $content_stylings .'"';
		}
	}
	
	// Icon Styles
	$icon_styles = $icon_stylings = '';
	if( isset( ${'icon_'. $type} ) && ${'icon_'. $type} != '' ) {
		if( isset( $icon_color ) && $icon_color != '' ) {
			$icon_stylings = 'color: '. $icon_color .'; ';
		}
		if( isset( $icon_style ) && $icon_style == 'iconfb-bg' ) {
			if( isset( $icon_bg_color ) && $icon_bg_color != '' ) {
				$icon_stylings .= 'background-color: '. $icon_bg_color .'; ';
			}
		}
		if( isset( $icon_style ) && $icon_style == 'iconfb-bordered' ) {
			if( isset( $icon_border_color ) && $icon_border_color != '' ) {
				$icon_stylings .= 'border-color: '. $icon_border_color .';';
			}
		}
		if( isset( $icon_stylings ) && $icon_stylings != '' ) {
			$icon_styles = ' style="'. $icon_stylings .'"';
		}
	}
	
	// Button URL
	$button_link = $button_title = $button_target = '';
	if( $button_url && $button_url != '' ) {
		$link = vc_build_link( $button_url );
		$button_link = isset( $link['url'] ) ? $link['url'] : '';
		$button_title = isset( $link['title'] ) ? $link['title'] : '';
		$button_target = !empty( $link['target'] ) ? $link['target'] : '_self';
	}
		
	$css_classes = array( vc_shortcode_custom_css_class( $css ) );
	$css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( $css_classes ) ), 'tpath_feature_box', $atts ) );
	
	$main_classes .= ' ' . esc_attr( trim( $css_class ) );
	
	$main_classes .= volunteer_vc_animation( $css_animation );
	
	$output .= '<div class="'. esc_attr( $main_classes ) .'">';
		$output .= '<div class="feature-box-item">';
			if( isset( $box_style ) && ( $box_style != 'title-with-icon' ) ) {
				$output .= '<div class="feature-box-inner text-'.$alignment.''.$icon_wrapper_class.'">';
				
					// Icon
					if( isset( ${'icon_'. $type} ) && ${'icon_'. $type} != '' ) {
						$output .= '<div class="feature-icon-wrapper shape-'.$icon_shape.'">';
							$output .= '<i class="'. esc_attr( ${'icon_'. $type} ) .' feature-icon'.$icon_extra_class.' '.$icon_shape.' '.$icon_skin_color.' '.$icon_style.' icon-'.$icon_size.'"'. $icon_styles .'></i>';
						$output .= '</div>';
					}
					
					if( isset( $alignment ) && ( $alignment == "left" || $alignment == "right" ) ) {
						$output .= '<div class="feature-content-wrapper">';
					}
						
					// Title
					if( isset( $title ) && $title != '' ) {
						$output .= '<'. $title_type .' class="feature-box-title'. $title_class .'"'. $title_styles .'>';
						if( isset( $title_link ) && $title_link != '' ) {
							$output .= '<a href="'. esc_url( $title_link ) .'" title="'. esc_attr( $link_title ) .'" target="'. esc_attr( $link_target ) .'"'. $title_link_styles .'>';
						}
						$output .= $title;
						if( isset( $title_link ) && $title_link != '' ) {
							$output .= '</a>';
						}
						$output .= '</'. $title_type .'>';
					}
					
					// Sub Title
					if( isset( $sub_title ) && $sub_title != '' ) {
						$output .= '<h6 class="feature-sub-title">'. $sub_title .'</h6>';
					}
					
					// Content
					if( isset( $content ) && $content != '' ) {
						$output .= '<div class="feature-box-desc"'. $content_styles .'>';
							$output .= wpb_js_remove_wpautop( $content, true );
						$output .= '</div>';
					}
					
					// Button
					if( isset( $show_button ) && $show_button == 'yes' ) {
						$output .= '<div class="feature-box-button text-'.$btn_alignment.'">';
							$output .= '<a href="'.esc_url( $button_link ).'" class="btn btn-fbox '. $button_type .' '. $button_skin .'" title="'. esc_attr( $button_title ).'" target="'. esc_attr( $button_target ).'">'.$button_text.'</a>';
						$output .= '</div>';
					}
					
					if( isset( $alignment ) && ( $alignment == "left" || $alignment == "right" ) ) {
						$output .= '</div>';
					}
				
				$output .= '</div>';
			} else {
				$output .= '<div class="feature-icon-box-inner text-'.$alignment.''.$icon_wrapper_class.'">';
					if( isset( $title ) && $title != '' ) {
						$output .= '<div class="feature-icon-box-title">';
					}
					
					if( $alignment == "left" ) {
						// Title
						if( isset( $title ) && $title != '' ) {
							$output .= '<'. $title_type .' class="feature-box-title'. $title_class .'"'. $title_styles .'>';
							if( isset( $title_link ) && $title_link != '' ) {
								$output .= '<a href="'. esc_url( $title_link ) .'" title="'. esc_attr( $link_title ) .'" target="'. esc_attr( $link_target ) .'"'. $title_link_styles .'>';
							}
							$output .= $title;
							if( isset( $title_link ) && $title_link != '' ) {
								$output .= '</a>';
							}
							$output .= '</'. $title_type .'>';
						}
					}
					
					// Icon
					if( isset( ${'icon_'. $type} ) && ${'icon_'. $type} != '' ) {
						$output .= '<div class="feature-icon-wrapper shape-'.$icon_shape.'">';
							$output .= '<i class="'. esc_attr( ${'icon_'. $type} ) .' feature-icon'.$icon_extra_class.' '.$icon_shape.' '.$icon_skin_color.' '.$icon_style.' icon-'.$icon_size.'"'. $icon_styles .'></i>';
						$output .= '</div>';
					}
					
					if( $alignment == "right" || $alignment == "center" ) {
						// Title
						if( isset( $title ) && $title != '' ) {
							$output .= '<'. $title_type .' class="feature-box-title'. $title_class .'"'. $title_styles .'>';
							if( isset( $title_link ) && $title_link != '' ) {
								$output .= '<a href="'. esc_url( $title_link ) .'" title="'. esc_attr( $link_title ) .'" target="'. esc_attr( $link_target ) .'"'. $title_link_styles .'>';
							}
							$output .= $title;
							if( isset( $title_link ) && $title_link != '' ) {
								$output .= '</a>';
							}
							$output .= '</'. $title_type .'>';
						}
					}
					
					if( isset( $title ) && $title != '' ) {
						$output .= '</div>';
					}
					
					$output .= '<div class="feature-icon-box-content">';
						// Sub Title
						if( isset( $sub_title ) && $sub_title != '' ) {
							$output .= '<h6 class="feature-sub-title">'. $sub_title .'</h6>';
						}
					
						// Content						
						if( isset( $content ) && $content != '' ) {
							$output .= '<div class="feature-box-desc"'. $content_styles .'>';
								$output .= wpb_js_remove_wpautop( $content, true );
							$output .= '</div>';
						}
						
						// Button
						if( isset( $show_button ) && $show_button == 'yes' ) {
							$output .= '<div class="feature-box-button text-'.$btn_alignment.'">';
								$output .= '<a href="'.esc_url( $button_link ).'" class="btn btn-fbox '. $button_type .' '. $button_skin .'" title="'. esc_attr( $button_title ).'" target="'. esc_attr( $button_target ).'">'.$button_text.'</a>';
							$output .= '</div>';
						}
					$output .= '</div>';
				
				$output .= '</div>';
			}
		$output .= '</div>';
	$output .= '</div>';
	
	$tpath_feature_box++;
		
	return $output;
}
add_shortcode( 'volunteer_feature_box', 'volunteer_features_box_shortcode' );

/**
 * The VC Element Config Functions
 */
function volunteer_vc_features_box_shortcode() {
	vc_map( 
		array(
			"icon" 			=> 'tpath-vc-block',
			"name" 			=> esc_html__( 'Feature Box', 'volunteer' ),
			"base" 			=> 'volunteer_feature_box',
			"category" 		=> esc_html__( 'Theme Addons', 'volunteer' ),
			"params" 		=> array(
				array(
					'type'			=> 'textfield',
					'admin_label' 	=> true,
					'heading'		=> esc_html__( 'Extra Class', "volunteer" ),
					'param_name'	=> 'classes',
					'value' 		=> '',
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> __( "CSS Animation", "volunteer" ),
					"param_name"	=> "css_animation",
					"value"			=> array(
						esc_html__( "No", "volunteer" )						=> '',
						esc_html__( "Top to bottom", "volunteer" )			=> "top-to-bottom",
						esc_html__( "Bottom to top", "volunteer" )			=> "bottom-to-top",
						esc_html__( "Left to right", "volunteer" )			=> "left-to-right",
						esc_html__( "Right to left", "volunteer" )			=> "right-to-left",
						esc_html__( "Appear from center", "volunteer" )		=> "appear" ),
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Box Style", "volunteer" ),
					"param_name"	=> "box_style",
					'admin_label' 	=> true,
					"value"			=> array(
						esc_html__( "Default Style", "volunteer" )		=> "default-box",
						esc_html__( "Title with Icon", "volunteer" )	=> "title-with-icon",
					),
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Alignment", "volunteer" ),
					"param_name"	=> "alignment",
					'admin_label' 	=> true,
					"value"			=> array(
						esc_html__( "Left", "volunteer" )		=> "left",
						esc_html__( "Right", "volunteer" )		=> "right",
						esc_html__( "Center", "volunteer" )		=> "center",
					),
				),
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__( "Title Type", "volunteer" ),
					"param_name" 	=> "title_type",
					"value"			=> array(
							"h2"		=> "h2",
							"h3"		=> "h3",
							"h4"		=> "h4",
							"h5"		=> "h5",
							"h6"		=> "h6",
					),
					"group" 		=> esc_html__( "Title", "volunteer" ),
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> esc_html__("Title", "volunteer"),
					"param_name" 	=> "title",
					"value" 		=> '',
					"group" 		=> esc_html__( "Title", "volunteer" ),
				),
				array(
					"type"			=> "vc_link",
					"heading"		=> esc_html__( "Title URL", 'volunteer' ),
					"param_name"	=> "title_url",
					"value"			=> "",
					"group" 		=> esc_html__( "Title", "volunteer" ),
				),
				array(
					"type"			=> "dropdown",
					"heading"		=> esc_html__( "Title Transform", 'volunteer' ),
					"param_name"	=> "title_transform",
					"value"			=> array(
						esc_html__( "Default", 'volunteer' )	=> '',
						esc_html__( "None", 'volunteer' )		=> 'transform-none',
						esc_html__( "Capitalize", 'volunteer' )	=> 'capitalize',
						esc_html__( "Uppercase", 'volunteer' )	=> 'uppercase',
						esc_html__( "Lowercase", 'volunteer' )	=> 'lowercase',
					),
					"group" 		=> esc_html__( "Title", "volunteer" ),
				),
				array(
					"type"			=> "textfield",
					"heading"		=> esc_html__( "Title Font Size", "volunteer" ),
					"param_name"	=> "title_size",
					"description" 	=> esc_html__( "Enter Title Font Size. Ex: 24px", "volunteer" ),
					"group"			=> esc_html__( "Title", "volunteer" ),
				),
				array(
					"type"			=> "dropdown",
					"heading"		=> esc_html__( "Title Weight", 'volunteer' ),
					"param_name"	=> "title_weight",
					"value"			=> array(
						esc_html__( "Default", 'volunteer' )	=> '',
						esc_html__( "Normal", 'volunteer' )		=> '400',
						esc_html__( "Semi Bold", 'volunteer' )	=> '600',
						esc_html__( "Bold", 'volunteer' )		=> 'bold',
						esc_html__( "Extra Bold", 'volunteer' )	=> '900',
					),
					"group" 		=> esc_html__( "Title", "volunteer" ),
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> esc_html__("Title Margin Bottom", 'volunteer'),
					"description" 	=> esc_html__( "Enter Values in Number. Ex: 15px", "volunteer" ),
					"param_name" 	=> "title_margin",
					"value" 		=> '',
					"group" 		=> esc_html__( "Title", "volunteer" ),
				),
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__("Title Border Style", "volunteer"),
					"param_name" 	=> "border_options",
					"value" 		=> array(
						esc_html__('None', 'volunteer') 				=> 'border_none',
						esc_html__('Bottom Border', 'volunteer') 		=> 'fullwidth_border',
						esc_html__('Small Bottom Border', 'volunteer') 	=> 'small_border',
									),
					"group" 		=> esc_html__( "Title", "volunteer" ),
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> esc_html__("Sub Title", "volunteer"),
					"param_name" 	=> "sub_title",
					"value" 		=> '',
					"group" 		=> esc_html__( "Title", "volunteer" ),
				),
 				array(
					'type' 			=> "dropdown",
					'heading' 		=> esc_html__( "Choose from Icon library", "volunteer" ),
					'value' 		=> array(
						esc_html__( 'None', 'volunteer' ) 			=> 'none',
						esc_html__( 'Flaticons', 'volunteer' ) 		=> 'flaticons',
						esc_html__( 'Font Awesome', 'volunteer' ) 	=> 'fontawesome',
						esc_html__( 'Lineicons', 'volunteer' ) 		=> 'lineicons',						
					),
					'admin_label' 	=> true,
					'param_name' 	=> 'type',
					'description' 	=> esc_html__( "Select icon library.", "volunteer" ),
					"group" 		=> esc_html__( "Icon", "volunteer" ),
				),
				array(
					'type' 			=> 'iconpicker',
					'heading' 		=> esc_html__( 'Icon', 'volunteer' ),
					'param_name' 	=> 'icon_flaticons',
					'value' 		=> '', // default value to backend editor admin_label
					'settings' 		=> array(
						'emptyIcon' 	=> true, // default true, display an "EMPTY" icon?
						'type' 			=> 'flaticons',
						'iconsPerPage' 	=> 4000, // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
					),
					'dependency' 	=> array(
						'element' 	=> 'type',
						'value' 	=> 'flaticons',
					),
					'description' 	=> esc_html__( 'Select icon from library.', 'volunteer' ),
					"group" 		=> esc_html__( "Icon", "volunteer" ),
				),
				array(
					'type' 			=> 'iconpicker',
					'heading' 		=> esc_html__( 'Icon', 'volunteer' ),
					'param_name' 	=> 'icon_fontawesome',
					'value' 		=> '', // default value to backend editor admin_label
					'settings' 		=> array(
						'emptyIcon' 	=> true, // default true, display an "EMPTY" icon?
						'iconsPerPage' 	=> 4000, // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
					),
					'dependency' 	=> array(
						'element' 	=> 'type',
						'value' 	=> 'fontawesome',
					),
					'description' 	=> esc_html__( 'Select icon from library.', 'volunteer' ),
					"group" 		=> esc_html__( "Icon", "volunteer" ),
				),				
				array(
					'type' 			=> 'iconpicker',
					'heading' 		=> esc_html__( 'Icon', 'volunteer' ),
					'param_name' 	=> 'icon_lineicons',
					'value' 		=> '', // default value to backend editor admin_label
					'settings' 		=> array(
						'emptyIcon' 	=> true, // default true, display an "EMPTY" icon?
						'type' 			=> 'simpleicons',
						'iconsPerPage' 	=> 4000, // default 100, how many icons per/page to display
					),
					'dependency' 	=> array(
						'element' 	=> 'type',
						'value' 	=> 'lineicons',
					),
					'description' 	=> esc_html__( 'Select icon from library.', 'volunteer' ),
					"group" 		=> esc_html__( "Icon", "volunteer" ),
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Icon Size", "volunteer" ),
					"param_name"	=> "icon_size",
					"value"			=> array(
						esc_html__( "Normal", "volunteer" )		=> "normal",
						esc_html__( "Small", "volunteer" )		=> "small",
						esc_html__( "Medium", "volunteer" )		=> "medium",
						esc_html__( "Large", "volunteer" )		=> "large",
					),
					"group"			=> esc_html__( "Icon", "volunteer" ),
				),			
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Icon Shape", "volunteer" ),
					"param_name"	=> "icon_shape",
					"value"			=> array(
						esc_html__( "None", "volunteer" )		=> "iconfb-none",
						esc_html__( "Circle", "volunteer" )		=> "iconfb-circle",
						esc_html__( "Rounded", "volunteer" )	=> "iconfb-rounded",
						esc_html__( "Square", "volunteer" )		=> "iconfb-square",
					),
					"group"			=> esc_html__( "Icon", "volunteer" ),
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Icon Background Style", "volunteer" ),
					"param_name"	=> "icon_style",
					"value"			=> array(
						esc_html__( "None", "volunteer" )			=> "iconfb-bg-none",
						esc_html__( "Background", "volunteer" )		=> "iconfb-bg",
						esc_html__( "Bordered", "volunteer" )		=> "iconfb-bordered",
					),
					"group"			=> esc_html__( "Icon", "volunteer" ),
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Icon Color", "volunteer" ),
					"param_name"	=> "icon_skin_color",
					"value"			=> array(
						esc_html__( "Default", "volunteer" )	=> "iconfb-skin-default",
						esc_html__( "Light", "volunteer" )		=> "iconfb-skin-light",
						esc_html__( "Dark", "volunteer" )		=> "iconfb-skin-dark",
					),
					"group"			=> esc_html__( "Icon", "volunteer" ),
				),
				array(
					"type" 			=> "textarea_html",
					"heading" 		=> esc_html__("Content", "volunteer"),
					"param_name" 	=> "content",
					"value" 		=> '',
					"holder" 		=> 'div',
					"group" 		=> esc_html__( "Content", "volunteer" ),
				),
				array(
					"type"			=> "textfield",
					"heading"		=> esc_html__( "Content Font Size", "volunteer" ),
					"param_name"	=> "content_size",
					"description" 	=> esc_html__( "Enter Content Font Size. Ex: 14px", "volunteer" ),
					"group"			=> esc_html__( "Content", "volunteer" ),
				),
				array(
					"type"			=> "dropdown",
					"heading"		=> esc_html__( "Content Weight", 'volunteer' ),
					"param_name"	=> "content_weight",
					"value"			=> array(
						esc_html__( "Default", 'volunteer' )	=> '',
						esc_html__( "Normal", 'volunteer' )		=> '400',
						esc_html__( "Semi Bold", 'volunteer' )	=> '600',
						esc_html__( "Bold", 'volunteer' )		=> 'bold',
						esc_html__( "Extra Bold", 'volunteer' )	=> '900',
					),
					"group" 		=> esc_html__( "Content", "volunteer" ),
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Show Button", "volunteer" ),
					"param_name"	=> "show_button",
					"value"			=> array(
						esc_html__( "No", "volunteer" )	=> "no",
						esc_html__( "Yes", "volunteer" )	=> "yes",							
					),
					"group"			=> esc_html__( "Content", "volunteer" ),
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> esc_html__("Button Text", 'volunteer'),
					"param_name" 	=> "button_text",
					"value" 		=> '',
					"group" 		=> esc_html__( "Content", "volunteer" ),
				),
				array(
					"type"			=> "vc_link",
					"heading"		=> esc_html__( "Button URL", "volunteer" ),
					"param_name"	=> "button_url",
					"value"			=> "",
					"group"			=> esc_html__( "Content", "volunteer" ),
				),
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__("Button Type", 'volunteer'),
					"param_name" 	=> "button_type",
					"value" 		=> array_flip(array(
									'btn-style-outline' 		 => 'Outline Style',
									'btn-style-color'	 		 => 'Background Style',
									'btn-simple-text' 			 => 'Simple Text',
									)),
					'group' 		=> esc_html__( "Content", "volunteer" ),
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Button Skin", "volunteer" ),
					"param_name"	=> "button_skin",
					"value"			=> array(
						esc_html__( "Default", "volunteer" )		=> "btn-skin-default",
						esc_html__( "White Color", "volunteer" )	=> "btn-white",
						esc_html__( "Dark Color", "volunteer" )		=> "btn-skin-dark",
						esc_html__( "Theme Color", "volunteer" )	=> "btn-skin-theme",
					),
					'group' 		=> esc_html__( "Content", "volunteer" ),
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Button Alignment", "volunteer" ),
					"param_name"	=> "btn_alignment",
					'admin_label' 	=> true,
					"value"			=> array(
						esc_html__( "Right", "volunteer" )		=> "right",
						esc_html__( "Left", "volunteer" )		=> "left",
						esc_html__( "Center", "volunteer" )		=> "center",
					),
					'group' 		=> esc_html__( "Content", "volunteer" ),
				),
				array(            
					"type" 			=> "colorpicker",
					"heading" 		=> esc_html__( "Title color", "volunteer" ),
					"param_name" 	=> "title_color",
					"value" 		=> '',
					"description" 	=> '',
					'group' 		=> esc_html__( "Style", "volunteer" ),
				),
				array(            
					"type" 			=> "colorpicker",
					"heading" 		=> esc_html__( "Content color", "volunteer" ),
					"param_name" 	=> "content_color",
					"value" 		=> '',
					"description" 	=> '',
					'group' 		=> esc_html__( "Style", "volunteer" ),
				),
				array(            
					"type" 			=> "colorpicker",
					"heading" 		=> esc_html__( "Icon color", "volunteer" ),
					"param_name" 	=> "icon_color",
					"value" 		=> '',
					"description" 	=> '',
					"group" 		=> esc_html__( "Style", "volunteer" ),
				),
				array(
					"type"			=> 'colorpicker',
					"heading"		=> esc_html__( "Icon Background Color", "volunteer" ),
					"param_name"	=> "icon_bg_color",
					"group"			=> esc_html__( "Style", "volunteer" ),
					'dependency'	=> array(
						'element'	=> 'icon_style',
						'value'		=> array( 'iconfb-bg' ),
					),
				),
				array(
					"type"			=> 'colorpicker',
					"heading"		=> esc_html__( "Icon Border Color", "volunteer" ),
					"param_name"	=> "icon_border_color",
					"group"			=> esc_html__( "Style", "volunteer" ),
					'dependency'	=> array(
						'element'	=> 'icon_style',
						'value'		=> array( 'iconfb-bordered' ),
					),
				),
				array(
					'type' 			=> 'css_editor',
					'heading' 		=> esc_html__( 'CSS box', 'volunteer' ),
					'param_name' 	=> 'css',
					'group' 		=> esc_html__( 'Design Options', 'volunteer' )
				),
			)
		) 
	);
}
add_action( 'vc_before_init', 'volunteer_vc_features_box_shortcode' );