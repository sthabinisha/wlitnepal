<?php 
/**
 * The Events Calendar List shortcode 
 */

if ( ! function_exists( 'volunteer_vc_events_list_shortcode' ) ) {
	function volunteer_vc_events_list_shortcode( $atts, $content = NULL ) {
	
		/**
		 * Check if events calendar plugin method exists
		 */
		if ( ! function_exists( 'tribe_get_events' ) ) {
			return;
		}
		
		$atts = vc_map_get_attributes( 'volunteer_vc_events_list', $atts );
		extract( $atts );

		$output = '';
		$extra_class = '';
		static $events_id = 1;
		
		global $post;
		
		if( isset( $events_listtype ) && $events_listtype == 'categories' ) {
			// Include categories
			$include_categories = ( '' != $include_categories ) ? $include_categories : '';
			$include_categories = ( 'all' == $include_categories ) ? '' : $include_categories;
			$include_filter_cats = '';
			if( $include_categories ) {
				$include_categories = explode( ',', $include_categories );
				$include_filter_cats = array();
				foreach( $include_categories as $key ) {
					$key = get_term_by( 'slug', $key, 'tribe_events_cat' );
					$include_filter_cats[] = $key->term_id;
				}
			}
			
			if ( ! empty( $include_categories ) && is_array( $include_categories ) ) {
				$include_categories = array(
					'taxonomy'	=> 'tribe_events_cat',
					'field'		=> 'slug',
					'terms'		=> $include_categories,
					'operator'	=> 'IN',
				);
			} else {
				$include_categories = '';
			}
		
			// Exclude categories
			$exclude_filter_cats = '';
			if( $exclude_categories ) {
				$exclude_categories = explode( ',', $exclude_categories );
				if ( ! empty( $exclude_categories ) && is_array( $exclude_categories ) ) {
					$exclude_filter_cats = array();
					foreach ( $exclude_categories as $key ) {
						$key = get_term_by( 'slug', $key, 'tribe_events_cat' );
						$exclude_filter_cats[] = $key->term_id;
					}
					$exclude_categories = array(
							'taxonomy'	=> 'tribe_events_cat',
							'field'		=> 'slug',
							'terms'		=> $exclude_categories,
							'operator'	=> 'NOT IN',
						);
				} else {
					$exclude_categories = '';
				}
			}
		} else if( isset( $events_listtype ) && $events_listtype == 'ids' ) {
			// Include Events
			$include_postids = ( '' != $include_events ) ? $include_events : '';
			$include_filter_ids = '';
			if( $include_postids ) {
				$include_postids = explode( ',', $include_events );
				$include_filter_ids = array();
				foreach( $include_postids as $key ) {
					$include_filter_ids[] = $key;
				}
			}
			
			// Exclude Events
			$exclude_postids = ( '' != $exclude_events ) ? $exclude_events : '';
			$exclude_filter_ids = '';
			if( $exclude_postids ) {
				$exclude_postids = explode( ',', $exclude_events );
				$exclude_filter_ids = array();
				foreach( $exclude_postids as $key ) {
					$exclude_filter_ids[] = $key;
				}
			}
		}
				
		// Past Event
		$meta_date_compare = '>=';
		$meta_date_date = date( 'Y-m-d' );

		if( isset( $past ) && $past == 'yes' ) {
			$meta_date_compare = '<';
		}
		
		// Key
		$key = '';
		if( $orderkey == 'startdate' ) {
			$key = '_EventStartDate';
		} else {
			$key = '_EventEndDate';
		}
		
		// Date
		$meta_date = '';
		$meta_date = array(
			array(
				'key' 		=> $key,
				'value' 	=> $meta_date_date,
				'compare' 	=> $meta_date_compare,
				'type' 		=> 'DATETIME'
			)
		);
		
		// Specific Month
		if( $month == 'current' ) {
			$month = date( 'Y-m' );
		}
		
		if( $month ) {
			$month_array = explode("-", $month);

			$month_yearstr = $month_array[0];
			$month_monthstr = $month_array[1];

			$month_startdate = date($month_yearstr . "-" . $month_monthstr . "-1");
			$month_enddate = date($month_yearstr . "-" . $month_monthstr . "-t");

			$meta_date = array(
				array(
					'key' 		=> $key,
					'value' 	=> array($month_startdate, $month_enddate),
					'compare' 	=> 'BETWEEN',
					'type' 		=> 'DATETIME'
				)
			);
		}
		
		if( isset( $message ) && $message == "" ) {
			$message = 'There are no upcoming events at this time.';
		}
		
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		
		$query_args = array(
						'post_type' 		=> 'tribe_events',
						'posts_per_page'	=> $posts,
						'paged' 			=> $paged,
						'orderby' 		 	=> 'meta_value',
						'order' 		 	=> $orderby,
						'meta_key' 			=> $key,
						'meta_query' 		=> array( $meta_date )
					  );
		
		if( isset( $events_listtype ) && $events_listtype == 'categories' ) {			  		
			$query_args['tax_query'] 	= array(
											'relation'	=> 'AND',
											$include_categories,
											$exclude_categories );
		} 
		else if( isset( $events_listtype ) && $events_listtype == 'ids' ) {
			if( $include_filter_ids != '' ) {
				$query_args['include'] 	= $include_filter_ids;
			}
			
			if( $exclude_filter_ids != '' ) {
				$query_args['exclude'] 	= $exclude_filter_ids;
			}
		}
		
		$events_query = get_posts( $query_args );
		
		// Classes
		$main_classes = 'tpath-events-list-wrapper';
		
		if( isset( $classes ) && $classes != '' ) {
			$main_classes .= ' ' . $classes;
		}
		$main_classes .= ' style-'. $events_style;
		
		$row_class = '';
		$column_class = '';
		if( isset( $events_style ) && $events_style == 'grid' ) {
			$row_class = ' row';
			if( isset( $columns ) && $columns != '' ) {
				if( isset( $columns ) && $columns == '1' ) {
					$column_class = ' col-md-12 col-xs-12';
				} else if( isset( $columns ) && $columns == '2' ) {
					$column_class = ' col-md-6 col-sm-6 col-xs-12';
				} else if( isset( $columns ) && $columns == '3' ) {
					$column_class = ' col-md-4 col-sm-6 col-xs-12';
				} else if( isset( $columns ) && $columns == '4' ) {
					$column_class = ' col-md-3 col-sm-6 col-xs-12';
				}
			} else {
				$column_class = ' col-md-12 col-xs-12';
			}
		}
		
		$contentorder = explode( ',', $contentorder );
		
		// Stylings
		$main_styles = '';
		
		
		if( isset( $bg_color ) && $bg_color != '' ) {
			$overlay_color = volunteer_hex2rgb( $bg_color );
			$main_stylings .= '#tpath_events_'.$events_id.' .ecs-event-overlay::after { background-color: rgba(' . $overlay_color[0] . ',' . $overlay_color[1] . ',' . $overlay_color[2] . ', 0.7' . $tpath_overlay_color_opacity . ');}';
		}
		if( isset( $main_stylings ) && $main_stylings != '' ) {
			$main_styles = ' style="'. $main_stylings .'"';
		}
		
		$title_styles = '';
		if( isset( $title_color ) && $title_color != '' ) {
			$title_styles = 'color: '. $title_color .';';
		}
		if( $title_styles ) {
			$title_styles = ' style="'. $title_styles  .'"';
		}
		
		$content_styles = '';
		if( isset( $content_color ) && $content_color != '' ) {
			$content_styles = ' style="color: '. $content_color .';"';
		}
		if( isset( $main_stylings ) && $main_stylings != '' ) {
			$output .= '<style>';
				$output .= $main_stylings;
			$output .= '</style>';			
		}
			
		if( $events_query ) {
			$output .= '<div id="tpath_events_'.$events_id.'" class="'. esc_attr( $main_classes ) .'">';
				$output .= '<ul class="ecs-event-list'. $row_class .' type-'.$events_style.'">';
					
					$count = 1;
					
					foreach( $events_query as $post ) :
						setup_postdata( $post );
						
						if( ( isset( $columns ) && $columns == '2' && $count == '3' ) || ( isset( $columns ) && $columns == '3' && $count == '4' ) || ( isset( $columns ) && $columns == '4' && $count == '5' ) ) {
							$output .= '<li class="ecs-clearfix clearfix"></li>';
							$count = 1;
						}
											
						$output .= '<li class="ecs-event'. $column_class .'">';
							$output .= '<div class="ecs-event-inner clearfix">';
							
							// Put Values into $output
							foreach ( $contentorder as $content_order ) {
								switch ( trim( $content_order ) ) {
								
									case 'title' :
										$output .= '<div class="ecs-event-overlay" '.$main_styles.'>';
										$output .= '<'. $title_type .' class="event-title summary'.$extra_class.'"><a href="' . tribe_get_event_link() . '" rel="bookmark"'.$title_styles.'>' . apply_filters( 'ecs_event_list_title', get_the_title(), $atts ) . '</a></'. $title_type .'>';
										break;
			
									case 'thumbnail' :
										if( isset( $thumb_image ) && $thumb_image == "true" ) {
											$thumbWidth = is_numeric( $thumb_width ) ? $thumb_width : '';
											$thumbHeight = is_numeric( $thumb_height ) ? $thumb_height : '';
											
											if( ! empty($thumbWidth) && ! empty($thumbHeight) ) {
												$output .= get_the_post_thumbnail(get_the_ID(), array($thumbWidth, $thumbHeight) );
											} else {
												$size = ( !empty($thumbWidth) && !empty($thumbHeight) ) ? array( $thumbWidth, $thumbHeight ) : 'volunteer-blog-list';
			
												if ( $thumb = get_the_post_thumbnail( get_the_ID(), $size ) ) {
												
													$output .= '<a href="' . tribe_get_event_link() . '">';
													$output .= '<div class="ecs-img-overlay">';
													$output .= $thumb;
													$output .= '<span class="icon-arrow flaticon-right11"></span>';
													$output .= '</div>';
													$output .= '</a>';
													$output .= '</div>';
												}
											}
										}
										break;
			
									case 'excerpt' :
										if( isset( $excerpt ) && $excerpt == "true" ) {
											$excerptLength = is_numeric( $excerpt_length ) ? $excerpt_length : 100;
											$output .= '<div class="ecs-excerpt"'. $content_styles .'>' . volunteer_custom_wp_trim_excerpt( '', $excerptLength ) . '</div>';
										}
										break;
			
									case 'date' :
										if( isset( $eventdetails ) && $eventdetails == "true" ) {
											$output .= '<span class="duration time"'. $content_styles .'>' . apply_filters( 'ecs_event_list_details', tribe_events_event_schedule_details(), $atts ) . '</span>';
										}
										break;
			
									case 'venue' :
										if( isset( $venue ) && $venue == "true" ) {
											$output .= '<span class="duration venue"'. $content_styles .'><em> at </em>' . apply_filters( 'ecs_event_list_venue', tribe_get_venue(), $atts ) . '</span>';
										}
										break;
								}
							}
							$output .= '</div>';
							
						$output .= '</li>';
						
						$count++;
						
						wp_reset_postdata();
					
					endforeach;
		
				$output .= '</ul>';
				
				if( isset( $viewall ) && $viewall == "yes" ) {
					$output .= '<span class="ecs-all-events"><a href="' . apply_filters( 'ecs_event_list_viewall_link', tribe_get_events_link(), $atts ) .'" class="btn btn-default" rel="bookmark">' . translate( 'View All Events', 'volunteer' ) . '</a></span>';
				}
			$output .= '</div>';
		} else { //No Events were Found
			$output .= translate( $message, 'volunteer' );
		}
		
		$events_id++;
		return $output;
	}
}
add_shortcode( 'volunteer_vc_events_list', 'volunteer_vc_events_list_shortcode' );

if ( ! function_exists( 'volunteer_vc_events_list_shortcode_map' ) ) {
	function volunteer_vc_events_list_shortcode_map() {
		
		vc_map( 
			array(
				"name"					=> esc_html__( "Events List", "volunteer" ),
				"base"					=> "volunteer_vc_events_list",
				"category"				=> esc_html__( "Theme Addons", "volunteer" ),
				"icon" 					=> 'tpath-vc-block',
				"params"				=> array(					
					array(
						'type'			=> 'textfield',
						'admin_label' 	=> true,
						'heading'		=> esc_html__( 'Extra Class', "volunteer" ),
						'param_name'	=> 'classes',
						'value' 		=> '',
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Style", "volunteer" ),
						"param_name"	=> "events_style",						
						"value"			=> array(
							esc_html__( "List", "volunteer" )		=> "list",
							esc_html__( "Grid", "volunteer" )		=> "grid" ),
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Columns", "volunteer" ),
						"param_name"	=> "columns",
						"value"			=> array(
							esc_html__( "One Column", "volunteer" )			=> "1",
							esc_html__( "Two Columns", "volunteer" )		=> "2",
							esc_html__( "Three Columns", "volunteer" )		=> "3",
							esc_html__( "Four Columns", "volunteer" )		=> "4" ),
						'dependency' 	=> array(
							'element' 	=> 'events_style',
							'value' 	=> array( 'grid' ),
						),
					),
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( "Posts per Page", "volunteer" ),
						"admin_label" 	=> true,
						"param_name"	=> "posts",						
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "List Events", "volunteer" ),
						"param_name"	=> "events_listtype",
						"admin_label" 	=> true,
						"value"			=> array(
							esc_html__( "Categories", "volunteer" )		=> "categories",
							esc_html__( "List of IDs", "volunteer" )	=> "ids" ),
					),
					array(
						'type' 			=> 'autocomplete',
						'heading' 		=> esc_html__( 'Include Events', 'volunteer' ),
						'param_name' 	=> 'include_events',
						"admin_label" 	=> true,
						'description' 	=> esc_html__( 'Add events by title.', 'volunteer' ),
						'settings' 		=> array(
							'multiple' 	=> true,
							'sortable' 	=> true,
						),
						'dependency' 	=> array(
							'element' 	=> 'events_listtype',
							'value' 	=> array( 'ids' ),
						),
					),
					array(
						'type' 			=> 'autocomplete',
						'heading' 		=> esc_html__( 'Exclude Events', 'volunteer' ),
						'param_name' 	=> 'exclude_events',
						"admin_label" 	=> true,
						'description' 	=> esc_html__( 'Remove events by title.', 'volunteer' ),
						'settings' 		=> array(
							'multiple' 	=> true,
							'sortable' 	=> true,
						),
						'dependency' 	=> array(
							'element' 	=> 'events_listtype',
							'value' 	=> array( 'ids' ),
						),
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Include Categories', 'volunteer' ),
						'param_name'	=> 'include_categories',
						'admin_label'	=> true,
						'description'	=> esc_html__('Enter the slugs of a categories (comma seperated) to pull posts from or enter "all" to pull recent posts from all categories. Example: category-1, category-2.','volunteer'),
						'dependency' 	=> array(
							'element' 	=> 'events_listtype',
							'value' 	=> array( 'categories' ),
						),
					),
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Exclude Categories', 'volunteer' ),
						'param_name'	=> 'exclude_categories',
						'admin_label'	=> true,
						'description'	=> esc_html__('Enter the slugs of a categories (comma seperated) to exclude. Example: category-1, category-2.', 'volunteer'),
						'dependency' 	=> array(
							'element' 	=> 'events_listtype',
							'value' 	=> array( 'categories' ),
						),
					),				
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Order By", "volunteer" ),
						"param_name"	=> "orderby",
						"value"			=> array(
							esc_html__( "ASC", "volunteer" )		=> "ASC",
							esc_html__( "DESC", "volunteer" )		=> "DESC",							
						),
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Order by Key", "volunteer" ),
						"param_name"	=> "orderkey",
						"value"			=> array(
							esc_html__( "Start Date", "volunteer" )		=> "startdate",
							esc_html__( "End Date", "volunteer" )		=> "enddate" ),
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Event Title Type", "volunteer" ),
						"param_name"	=> "title_type",
						"value"			=> array(
							esc_html__( "h2", "volunteer" )	=> "h2",
							esc_html__( "h3", "volunteer" )	=> "h3",
							esc_html__( "h4", "volunteer" )	=> "h4",
							esc_html__( "h5", "volunteer" )	=> "h5",
							esc_html__( "h6", "volunteer" )	=> "h6",
						),
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Show Event Details ?", "volunteer" ),
						"param_name"	=> "eventdetails",	
						"value"			=> array(
							esc_html__( "Yes", "volunteer" )	=> "true",
							esc_html__( "No", "volunteer" )		=> "false" ),
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Show Event Venue ?", "volunteer" ),
						"param_name"	=> "venue",	
						"value"			=> array(
							esc_html__( "Yes", "volunteer" )	=> "true",
							esc_html__( "No", "volunteer" )		=> "false" ),
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Show Excerpt ?", "volunteer" ),
						"param_name"	=> "excerpt",	
						"value"			=> array(
							esc_html__( "Yes", "volunteer" )	=> "true",
							esc_html__( "No", "volunteer" )		=> "false" ),
					),
					array(
						"type"			=> 'textfield',
						"heading"		=> esc_html__( "Excerpt Length", "volunteer" ),
						"param_name"	=> "excerpt_length",	
						"value"			=> "",
						'dependency'	=> array(
							'element'	=> 'excerpt',
							'value'		=> array( 'true' ),
						),
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Show Thumbnail Image ?", "volunteer" ),
						"param_name"	=> "thumb_image",	
						"value"			=> array(
							esc_html__( "Yes", "volunteer" )	=> "true",
							esc_html__( "No", "volunteer" )		=> "false" ),
					),
					array(
						"type"			=> 'textfield',
						"heading"		=> esc_html__( "Thumbnail Width", "volunteer" ),
						"param_name"	=> "thumb_width",	
						"value"			=> "",
						'dependency'	=> array(
							'element'	=> 'thumb_image',
							'value'		=> array( 'true' ),
						),
					),
					array(
						"type"			=> 'textfield',
						"heading"		=> esc_html__( "Thumbnail Height", "volunteer" ),
						"param_name"	=> "thumb_height",	
						"value"			=> "",
						'dependency'	=> array(
							'element'	=> 'thumb_image',
							'value'		=> array( 'true' ),
						),
					),
					array(
						"type"			=> 'textfield',
						"heading"		=> esc_html__( "Message", "volunteer" ),
						"param_name"	=> "message",	
						"value"			=> "",
						'description'	=> esc_html__('Message to show when there are no events.','volunteer'),
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "View All Events", "volunteer" ),
						"param_name"	=> "viewall",	
						"value"			=> array(
							esc_html__( "Yes", "volunteer" )		=> "yes",
							esc_html__( "No", "volunteer" )		=> "no" ),
						'description'	=> esc_html__('Choose to show "View all events" or not.','volunteer'),
					),
					array(
						"type"			=> 'textfield',
						"heading"		=> esc_html__( "Content Order", "volunteer" ),
						"param_name"	=> "contentorder",	
						"value"			=> "title, thumbnail, excerpt, date, venue",
						'description'	=> esc_html__('Manage the order of content with commas.','volunteer'),
					),
					array(
						"type"			=> 'textfield',
						"heading"		=> esc_html__( "Events from Specific Month", "volunteer" ),
						"param_name"	=> "month",	
						"value"			=> "",
						'description'	=> esc_html__('Type "current" for displaying current month only. Ex: 2015-06 or current','volunteer'),
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Show Outdated Events", "volunteer" ),
						"param_name"	=> "past",
						"value"			=> array(
							esc_html__( "No", "volunteer" )		=> "no",
							esc_html__( "Yes", "volunteer" )		=> "yes" ),
					),
					array(
						"type"			=> 'colorpicker',
						"heading"		=> esc_html__( "Background Color", "volunteer" ),
						"param_name"	=> "bg_color",
						"group"			=> esc_html__( "Design", "volunteer" ),
					),
					array(
						"type"			=> 'colorpicker',
						"heading"		=> esc_html__( "Event Title Color", "volunteer" ),
						"param_name"	=> "title_color",
						"group"			=> esc_html__( "Design", "volunteer" ),
					),
					array(
						"type"			=> 'colorpicker',
						"heading"		=> esc_html__( "Content Color", "volunteer" ),
						"param_name"	=> "content_color",
						"group"			=> esc_html__( "Design", "volunteer" ),
					),
				)
			) 
		);
		
		//Filters For autocomplete param:
		//For suggestion: vc_autocomplete_[shortcode_name]_[param_name]_callback
		add_filter( 'vc_autocomplete_volunteer_vc_events_list_include_events_callback', 'volunteer_vc_events_include_field_search', 10, 1 ); // Get suggestion(find). Must return an array
		add_filter( 'vc_autocomplete_volunteer_vc_events_list_include_events_render', 'volunteer_vc_events_include_field_render', 10, 1 ); // Render exact post. Must return an array (label,value)
		add_filter( 'vc_autocomplete_volunteer_vc_events_list_exclude_events_callback', 'volunteer_vc_events_exclude_field_search', 10, 1 ); // Get suggestion(find). Must return an array
		add_filter( 'vc_autocomplete_volunteer_vc_events_list_exclude_events_render', 'volunteer_vc_events_exclude_field_render', 10, 1 ); // Render exact post. Must return an array (label,value)
		
		/**
		 * @param $search_string
		 *
		 * @return array
		 */
		function volunteer_vc_events_include_field_search( $search_string ) {
			$query = $search_string;
			$data = array();
			$args = array( 's' => $query, 'post_type' => 'tribe_events' );
			$args['vc_search_by_title_only'] = true;
			$args['numberposts'] = - 1;
			if ( strlen( $args['s'] ) == 0 ) {
				unset( $args['s'] );
			}
			add_filter( 'posts_search', 'vc_search_by_title_only', 500, 2 );
			$posts = get_posts( $args );
			if ( is_array( $posts ) && ! empty( $posts ) ) {
				foreach ( $posts as $post ) {
					$data[] = array(
						'value' => $post->ID,
						'label' => $post->post_title,
						'group' => $post->post_type,
					);
				}
			}
		
			return $data;
		}
		
		/**
		 * @param $value
		 *
		 * @return array|bool
		 */
		function volunteer_vc_events_include_field_render( $value ) {
			$post = get_post( $value['value'] );
		
			return is_null( $post ) ? false : array(
				'label' => $post->post_title,
				'value' => $post->ID,
				'group' => $post->post_type
			);
		}
		
		/**
		 * @param $data_arr
		 *
		 * @return array
		 */
		function volunteer_vc_events_exclude_field_search( $data_arr ) {
			if( is_array( $data_arr ) ) {
				$query = isset( $data_arr['query'] ) ? $data_arr['query'] : null;
				$term = isset( $data_arr['term'] ) ? $data_arr['term'] : "";
			} else {
				$query = $data_arr;
			}
			$data = array();
			
			$args = ! empty( $query ) ? array( 's' => $query, 'post_type' => 'tribe_events' ) : array( 's' => $term, 'post_type' => 'tribe_events' );
			
			$args['vc_search_by_title_only'] = true;
			$args['numberposts'] = - 1;
			if ( strlen( $args['s'] ) == 0 ) {
				unset( $args['s'] );
			}
			add_filter( 'posts_search', 'vc_search_by_title_only', 500, 2 );
			$posts = get_posts( $args );
			if ( is_array( $posts ) && ! empty( $posts ) ) {
				foreach ( $posts as $post ) {
					$data[] = array(
						'value' => $post->ID,
						'label' => $post->post_title,
						'group' => $post->post_type,
					);
				}
			}
		
			return $data;
		}
		
		/**
		 * @param $value
		 *
		 * @return array|bool
		 */
		function volunteer_vc_events_exclude_field_render( $value ) {
			$post = get_post( $value['value'] );
		
			return is_null( $post ) ? false : array(
				'label' => $post->post_title,
				'value' => $post->ID,
				'group' => $post->post_type
			);
		}
		
	}
}
add_action( 'vc_before_init', 'volunteer_vc_events_list_shortcode_map' );