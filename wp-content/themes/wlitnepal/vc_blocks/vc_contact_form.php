<?php
/**
 * Contact Form shortcode 
 */

if ( ! function_exists( 'volunteer_vc_contact_form_shortcode' ) ) {
	function volunteer_vc_contact_form_shortcode( $atts, $content = NULL ) {
		
		extract(
			shortcode_atts( 
				array(
					'classes'				=> '',
					'css_animation'			=> '',
					'form_style'			=> 'default',
					'form_bgstyle'			=> 'default',
					'show_name'				=> 'yes',
					'name_label'			=> esc_html__('User Name', 'volunteer'),
					'email_label'			=> esc_html__('Email', 'volunteer'),
					'show_phone'			=> 'yes',
					'phone_label'			=> esc_html__('Phone Number', 'volunteer'),
					'show_message' 			=> 'yes',
					'message_label' 		=> esc_html__('Message', 'volunteer'),					
					'button_text' 			=> esc_html__('Send Now', 'volunteer'),
					'button_align' 			=> 'left',
					'button_size' 			=> 'default',
					'button_block' 			=> '',
				), $atts 
			) 
		);

		$output = '';
		$button_class = '';
		$button_html = '';
		static $tpath_contactform_id = 1;
		
		// Button
		$button_html = $button_text;
		if ( 'yes' == $button_block ) {
			$button_class .= ' btn-block';
		}
	
		// Classes
		$main_classes = '';
		if( isset( $classes ) && $classes != '' ) {
			$main_classes .= ' ' . $classes;
		}
		if( isset( $form_style ) && $form_style != '' ) {
			$main_classes .= ' form-style-' . $form_style;
		}
		if( isset( $form_bgstyle ) && $form_bgstyle != '' ) {
			$main_classes .= ' form-bg-' . $form_bgstyle;
		}
		
		$main_classes .= volunteer_vc_animation( $css_animation );
		
		wp_enqueue_script( 'volunteer-bootstrap-validator-js' );
		
		$output .= '<div class="contact-form-wrapper'. esc_attr( $main_classes ) .'">';
			$output .= '<p class="bg-success tpath-form-success"></p>'; 
			$output .= '<p class="bg-danger tpath-form-error"></p>';
				
				$output .= '<div class="tpath-contact-form-container">';
					$output .= '<form name="contactform" class="tpath-contact-form" id="contactform'.$tpath_contactform_id.'" method="post" action="#">';
					
					// Name Field
					if( isset( $show_name ) && $show_name == 'yes' ) {						
						$output .= '<div class="tpath-input-text form-group">';
							$output .= '<label class="sr-only" for="contact_name">'.$name_label.'</label>';
							$output .= '<input type="text" name="contact_name" id="contact_name" class="input-name form-control" placeholder="'.esc_attr($name_label).'">';		
						$output .= '</div>';
					}
					
					// Email Field
					$output .= '<div class="tpath-input-email form-group">';
						$output .= '<label class="sr-only" for="contact_email">'.$email_label.'</label>';							
						$output .= '<input type="email" name="contact_email" id="contact_email" class="input-email form-control" placeholder="'.esc_attr($email_label).'">';
					$output .= '</div>';
					
					// Phone Field
					if( isset( $show_phone ) && $show_phone == 'yes' ) {
						$output .= '<div class="tpath-input-phone form-group">';
							$output .= '<label class="sr-only" for="contact_phone">'.$phone_label.'</label>';
							$output .= '<input type="text" id="contact_phone" name="contact_phone" class="input-phone form-control" placeholder="'.esc_attr($phone_label).'">';
						$output .= '</div>';
					}
					
					if( isset( $form_layout ) && $form_layout == 'two-column' ) {
						$output .= '</div>';
						$output .= '<div class="col-md-6 col-xs-12">';
					}
					
					// Message Field
					if( isset( $show_message ) && $show_message == 'yes' ) {
						$output .= '<div class="tpath-textarea-message form-group">';								
							$output .= '<label class="sr-only" for="contact_message">'.$message_label.'</label>';
							$output .= '<textarea name="contact_message" id="contact_message" class="textarea-message form-control" rows="6" cols="25" placeholder="'.esc_attr($message_label).'"></textarea>';
						$output .= '</div>';
					}
					
					// Button
					$output .= '<div class="tpath-input-submit form-group text-'.$button_align.'">';
						$output .= '<button type="submit" class="btn btn_bgcolor btn-'. esc_attr( $button_size ) .' tpath-submit'. esc_attr( $button_class ) .'">'.$button_html.'<span class="btn-icon fa fa-envelope-o"></span></button>';
					$output .= '</div>';
					
					$output .= '</form>';
				$output .= '</div>';
				
		$output .= '</div>';
		
		$tpath_contactform_id++;
		
		return $output;
	}
}
add_shortcode( 'volunteer_vc_contact_form', 'volunteer_vc_contact_form_shortcode' );

if ( ! function_exists( 'volunteer_vc_contact_form_shortcode_map' ) ) {
	function volunteer_vc_contact_form_shortcode_map() {
	
		vc_map( 
			array(
				"name"					=> esc_html__( "Contact Form", "volunteer" ),
				"base"					=> "volunteer_vc_contact_form",
				"category"				=> esc_html__( "Theme Addons", "volunteer" ),
				"icon" 					=> 'tpath-vc-block',
				"params"				=> array(					
					array(
						'type'			=> 'textfield',
						'admin_label' 	=> true,
						'heading'		=> esc_html__( 'Extra Class', "volunteer" ),
						'param_name'	=> 'classes',
						'value' 		=> '',
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "CSS Animation", "volunteer" ),
						"param_name"	=> "css_animation",
						"value"			=> array(
							esc_html__( "No", "volunteer" )						=> '',
							esc_html__( "Top to bottom", "volunteer" )			=> "top-to-bottom",
							esc_html__( "Bottom to top", "volunteer" )			=> "bottom-to-top",
							esc_html__( "Left to right", "volunteer" )			=> "left-to-right",
							esc_html__( "Right to left", "volunteer" )			=> "right-to-left",
							esc_html__( "Appear from center", "volunteer" )		=> "appear" ),
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Form Style", "volunteer" ),
						"param_name"	=> "form_style",
						"value"			=> array(
							esc_html__( "Default", "volunteer" )		=> "default",
							esc_html__( "Style 1", "volunteer" )		=> "style1",
							esc_html__( "Fullwidth", "volunteer" )		=> "fullwidth" ),
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Form Background", "volunteer" ),
						"param_name"	=> "form_bgstyle",
						"value"			=> array(
							esc_html__( "Default", "volunteer" )		=> "default",
							esc_html__( "Transparent", "volunteer" )	=> "transparent" ),
					),
					// Fields
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Show Name Field", "volunteer" ),
						"param_name"	=> "show_name",
						"value"			=> array(
							esc_html__( "Yes", "volunteer" )	=> "yes",
							esc_html__( "No", "volunteer" )		=> "no",
						),
						"group"			=> esc_html__( "Fields", "volunteer" ),
					),
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( "Name Field Label", "volunteer" ),
						"param_name"	=> "name_label",
						"value"			=> "User Name",
						"group"			=> esc_html__( "Fields", "volunteer" ),
					),
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( "Email Field Label", "volunteer" ),
						"param_name"	=> "email_label",
						"value"			=> "Email",
						"group"			=> esc_html__( "Fields", "volunteer" ),
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Show Phone Field", "volunteer" ),
						"param_name"	=> "show_phone",
						"value"			=> array(
							esc_html__( "Yes", "volunteer" )	=> "yes",
							esc_html__( "No", "volunteer" )		=> "no",
						),
						"group"			=> esc_html__( "Fields", "volunteer" ),
					),
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( "Phone Field Label", "volunteer" ),
						"param_name"	=> "phone_label",
						"value"			=> "Phone Number",
						"group"			=> esc_html__( "Fields", "volunteer" ),
					),					
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Show Message Field", "volunteer" ),
						"param_name"	=> "show_message",
						"value"			=> array(
							esc_html__( "Yes", "volunteer" )	=> "yes",
							esc_html__( "No", "volunteer" )		=> "no",
						),
						"group"			=> esc_html__( "Fields", "volunteer" ),
					),
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( "Message Field Label", "volunteer" ),
						"param_name"	=> "message_label",
						"value"			=> "Message",
						"group"			=> esc_html__( "Fields", "volunteer" ),
					),
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( "Button Text", "volunteer" ),
						"param_name"	=> "button_text",
						'admin_label' 	=> true,
						"value"			=> esc_html__( 'Send Message', 'volunteer' ),
						"group"			=> esc_html__( "Button", "volunteer" ),
					),
					array(
						'type' 			=> 'dropdown',
						'heading' 		=> esc_html__( 'Button Alignment', 'volunteer' ),
						'param_name' 	=> 'button_align',
						'description' 	=> esc_html__( 'Select button alignment.', 'volunteer' ),
						'value' 		=> array(
							esc_html__( 'Left', 'volunteer' ) 	=> 'left',
							esc_html__( 'Right', 'volunteer' ) 	=> 'right',
							esc_html__( 'Center', 'volunteer' ) => 'center'
						),
						"group"			=> esc_html__( "Button", "volunteer" ),
					),
					array(
						'type' 			=> 'dropdown',
						'heading' 		=> esc_html__( 'Button Size', 'volunteer' ),
						'param_name' 	=> 'button_size',
						'description' 	=> esc_html__( 'Select button size.', 'volunteer' ),
						'value' 		=> array(
							esc_html__( 'Default', 'volunteer' ) 	=> 'default',
							esc_html__( 'Large', 'volunteer' ) 		=> 'large',
						),
						"group"			=> esc_html__( "Button", "volunteer" ),
					),
					array(
						'type' 			=> 'checkbox',
						'heading' 		=> esc_html__( 'Set full width button?', 'volunteer' ),
						'param_name' 	=> 'button_block',
						"value"			=> array(
							esc_html__( "Yes", "volunteer" )	=> "yes"
						),
						"group"			=> esc_html__( "Button", "volunteer" ),
					),
				)
			) 
		);
	}
}
add_action( 'vc_before_init', 'volunteer_vc_contact_form_shortcode_map' );