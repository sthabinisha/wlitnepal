<?php
/**
 * The Shortcode 
 */

function volunteer_portfolio_gallery_shortcode( $atts ) {	

	$output = '';
	
	$atts = vc_map_get_attributes( 'volunteer_vc_portfolio', $atts );
	extract( $atts );

	global $post;
	
	/**
	 * Portfolio Query Args
	 */
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	

	$args = array(
		'post_type' 		=> 'tpath_portfolio',
		'posts_per_page' 	=> $posts,
		'paged' 			=> $paged,
	);
	
	if( isset( $show_filter  ) && $show_filter  != 'hide' ) {
		$filter_type = $show_filter;
	} else {
		$filter_type= $hide_filter;
	}
	
	if( isset( $portfolio_style  ) && $portfolio_style  == 'style_three' ) {
		$column_type = 1;
	} else {
		$column_type = $columns;
	}
	
	$column_image = $column_content = $content_type = '';
	if( isset( $portfolio_style  ) && $portfolio_style  == 'style_three' ) {
		$column_image = "col-lg-8 col-md-8 col-sm-12";
		$column_content = "col-lg-4 col-md-4 col-sm-12";
	}
	else {
		$content_type = "overlay-box";
	}
	

	if( ! ( $categories == 'all' ) ) {		

		$category_id = (int)$categories;	

		$args['tax_query'] = array( array(
								'taxonomy' 	=> 'portfolio_categories',
								'field' 	=> 'id',
								'terms' 	=> $category_id
							) );
	}

	$portfolio_query = new WP_Query( $args );

	if ( $portfolio_query->have_posts() ) {

		$portfolio_tags = get_terms('portfolio_categories');

		$output = '<div class="portfolio-gallery-wrapper '.$portfolio_style.'">';	
			$output .= '<div class="tpath-portfolio portfolio-columns-'.$columns.'" data-columns="'.$column_type.'" data-gutter="'.$gutter.'">';	
			
			if( is_array($portfolio_tags) && ! empty($portfolio_tags) && $show_filter != 'hide' && $categories == 'all' ) {
				$output .= '<div class="container">';
					$output .= '<div class="portfolio-top-wrapper">';
						$output .= '<ul class="portfolio-tabs list-inline">';
						$output .= '<li><a class="active" data-filter="*" href="#">'. esc_html__('All', 'volunteer') .'</a></li>';
							foreach( $portfolio_tags as $tags ) {
								$output .= '<li><a data-filter=".'.$tags->slug.'" href="#">'. $tags->name .'</a></li>';
							}
						$output .= '</ul>';
					$output .= '</div>';
				$output .= '</div>';
			}

			$output .= '<div class="portfolio-container">';	

				while($portfolio_query->have_posts()) : $portfolio_query->the_post();
				$portfolio_client = get_post_meta( $post->ID, 'volunteer_client_name', true );
				$portfolio_img = $portfolio_img_large = '';
				$portfolio_img_large = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$portfolio_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'volunteer-blog-list' );		

				$item_classes = '';
				$item_tags = get_the_terms($post->ID, 'portfolio_categories');
				if( $item_tags ) {
					foreach( $item_tags as $item_tag ) {
						$item_classes .=  ' ' . urldecode($item_tag->slug);
					}
				}
				$output .= '<div id="portfolio-'.get_the_ID().'" class="portfolio-item'.$item_classes.'">';
					$output .= '<div class="portfolio-single-item">';
						$output .= '<div class="portfolio-image '.$column_image.'">';
							$output .= '<a href="'.esc_url( $portfolio_img_large[0] ).'" data-rel="prettyPhoto[portfolio]" title="'.get_the_title().'">';
							$output .= '<img class="img-responsive" src="'. esc_url( $portfolio_img[0] ).'" alt="'.get_the_title().'" />';
							$output .= '</a>';
						$output .= '</div>';
							
						$output .= '<div class="portfolio-content '.$column_content.' '.$content_type.'">';
							$output .= '<div class="portfolio-box-border">';
								$output .= '<div class="content">';
									$output .= '<h4 class="portfolio-title"><a href="'. get_permalink() .'" title="'.get_the_title().'">'.get_the_title().'</a>';
									$output .= '</h4>';
									$output .= '<h5 class="portfolio-caption">';
										if( isset( $portfolio_client ) && $portfolio_client != '' ) {
											$output .= $portfolio_client;
										}
									$output .= '</h5>';
								$output .= '</div>';
							$output .= '</div>';
							
							if( isset( $portfolio_style  ) && $portfolio_style  == 'style_three' ) {
								$output .= '<div class="portfolio-miniborder"></div>';
								$output .= '<div class="portfolio-excerpt">';
									$output .= volunteer_custom_wp_trim_excerpt('', 60);
								$output .= '</div>';
								if( isset( $button_text  ) && $button_text  != '' ) {
									$output .= '<div class="portfolio-button">';
										$output .= '<a class="btn but-default portfolio-btn" href="'. get_permalink() .'" title="'.get_the_title().'">'.$button_text.'</a>';
									$output .= '</div>';
								}
							}
							
						$output .= '</div>';
					$output .= '</div>';
				$output .= '</div>';

				endwhile;

			$output .= '</div>';	

			$output .= '</div>';
		$output .= '</div>';

		if( isset( $show_pagination ) && $show_pagination == 'Yes' ) {
			$output .= volunteer_pagination( $portfolio_query->max_num_pages, '' );
		}
	}

	wp_reset_postdata();

	return $output;
}

add_shortcode( 'volunteer_vc_portfolio', 'volunteer_portfolio_gallery_shortcode' );

/**
 * The VC Element Config Functions
 */

function volunteer_vc_portfolio_shortcode() {

	vc_map( 
		array(
			"icon" 			=> 'tpath-vc-block',
			"name" 			=> esc_html__( 'Portfolio', 'volunteer' ),
			"base" 			=> 'volunteer_vc_portfolio',
			"category" 		=> esc_html__( 'Theme Addons', 'volunteer' ),
			"params" 		=> array(
				array(
					"type" 			=> "textfield",
					"heading" 		=> esc_html__("How Many Posts to Show?", "volunteer"),
					"param_name" 	=> "posts",
					"value" 		=> '8',
					"description" 	=> ''
				),
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__("Portfolio Style", "volunteer"),
					"param_name" 	=> "portfolio_style",
					"value" 		=> array_flip(array(
									'style_one' 			 => 'Style 1',
									'style_two' 			 => 'Style 2',
									'style_three' 			 => 'Style 3',
									)),
					"description" => ''
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> esc_html__("Button Text for Single page link", "volunteer"),
					"param_name" 	=> "button_text",
					"value" 		=> 'More Info',
					"description" 	=> 'Enter Link text for Single Portfolio Page - Only for "STYLE 3"',
				),
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__("Portfolio Columns", "volunteer"),
					"param_name" 	=> "columns",
					"admin_label" 	=> true,
					"value" 		=> array_flip(array(
									'2' 	=> '2 Columns',
									'3'  	=> '3 Columns',
									'4' 	=> '4 Columns',
									)),
					"description" 	=> 'Enter Link text for Single Portfolio Page - Only for "STYLE 1 and 2"',
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> esc_html__("Items Spacing (without px)", "volunteer"),
					"param_name" 	=> "gutter",
					"value" 		=> '30',
					"description" 	=> ''
				),
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__("Display Filter", "volunteer"),
					"param_name" 	=> "show_filter",
					"value" 		=> array_flip(array(
									'yes' 			 => 'Yes',
									'hide' 			 => 'Hide',
									)),
					"description" => ''
				),
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__("Show Pagination", "volunteer"),
					"param_name" 	=> "show_pagination",
					"value" 		=> array(
										'Yes',
										'No'
									),
					"description" => ''
				),
			)
		) 
	);	

}
add_action( 'vc_before_init', 'volunteer_vc_portfolio_shortcode');