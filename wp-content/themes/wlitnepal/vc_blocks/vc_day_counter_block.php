<?php 
/**
 * Day Counter shortcode
 */

if ( ! function_exists( 'volunteer_day_counter_shortcode' ) ) {
	function volunteer_day_counter_shortcode( $atts, $content = NULL ) {
		
		extract( 
			shortcode_atts( 
				array(
					'classes'				=> '',
					'counter_type'			=> 'down',
					'year' 					=> '2016',
					'month'					=> '1',
					'date' 					=> '1',
					'counter_color'			=> '',
					'countertext_color'		=> ''
				), $atts 
			) 
		);

		$output = '';
		static $counter_id = 1;
		
		$data_attr = '';		
		$data_attr .= ' data-counter="'.$counter_type.'" ';
		$data_attr .= ' data-year="'.$year.'" ';
		$data_attr .= ' data-month="'.$month.'" ';
		$data_attr .= ' data-date="'.$date.'" ';
		
		// Classes
		$main_classes = 'tpath-daycounter-container';
		
		if( isset( $classes ) && $classes != '' ) {
			$main_classes .= ' ' . $classes;
		}
		if(isset($counter_color) && $counter_color != '' ) {
			$counter_style = 'color: '.$counter_color.'; text-align:center;';
			$styles .= '<style type="text/css">';
			$styles .= sprintf( '#tpath-daycounter-%s .countdown-section .countdown-amount {%s}', $counter_id, $counter_style ) . "\n";
			$styles .= '</style>';
		}
		if(isset($countertext_color) && $countertext_color != '' ) {
			$countertext_style = 'color: '.$countertext_color.'; text-align:center; width:100%;';
			$styles .= '<style type="text/css">';
			$styles .= sprintf( '#tpath-daycounter-%s .countdown-section .countdown-period {%s}', $counter_id, $countertext_style ) . "\n";
			$styles .= '</style>';
		}
		$output .= $styles;		
		
		wp_enqueue_script( 'volunteer-countdown-plugin-js' );
		wp_enqueue_script( 'volunteer-countdown-js' );
	
		$output .= '<div class="'. esc_attr( $main_classes ) .'">';
			$output .= '<div id="tpath-daycounter-'.$counter_id.'" class="tpath-daycounter tpath-daycounter-wrapper clearfix"'.$data_attr.'>';
			$output .= '</div>';
		$output .= '</div>';
		
		$counter_id++;
		
		return $output;
	}
}
add_shortcode( 'volunteer_day_counter', 'volunteer_day_counter_shortcode' );

if ( ! function_exists( 'volunteer_day_counter_shortcode_map' ) ) {
	function volunteer_day_counter_shortcode_map() {
		
		vc_map( 
			array(
				"name"					=> esc_html__( "Day Counter", "volunteer" ),
				"base"					=> "volunteer_day_counter",
				"category"				=> esc_html__( "Theme Addons", "volunteer" ),
				"icon"					=> "tpath-vc-block",
				"params"				=> array(					
					array(
						'type'			=> 'textfield',
						'heading'		=> esc_html__( 'Extra Class', "volunteer" ),
						'param_name'	=> 'classes',
						'value' 		=> '',
					),
					array(
						"type"			=> 'dropdown',
						"heading"		=> esc_html__( "Counter Type", "volunteer" ),
						"param_name"	=> "counter_type",
						"admin_label" 	=> true,
						"value"			=> array(
							esc_html__( "Counter Down", "volunteer" )	=> "down",
							esc_html__( "Counter Up", "volunteer" )		=> "up" ),
					),
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( "Year", "volunteer" ),
						"admin_label" 	=> true,
						"param_name"	=> "year",						
					),
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( "Month", "volunteer" ),
						"admin_label" 	=> true,
						"param_name"	=> "month",						
					),
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( "Date", "volunteer" ),
						"admin_label" 	=> true,
						"param_name"	=> "date",
					),
					array(
						"type"			=> "colorpicker",
						"heading"		=> esc_html__( "Counter Numbers Color", "volunteer" ),
						"param_name"	=> "counter_color",
						"value"			=> "",
					),
					array(
						"type"			=> "colorpicker",
						"heading"		=> esc_html__( "Text Color", "volunteer" ),
						"param_name"	=> "countertext_color",
						"value"			=> "",
					),
				)
			) 
		);
	}
}
add_action( 'vc_before_init', 'volunteer_day_counter_shortcode_map' );