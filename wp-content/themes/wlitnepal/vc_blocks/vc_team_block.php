<?php 
/**
 * The Shortcode
 */
function volunteer_vc_team_shortcode( $atts ) {
	
	$output = '';
	
	extract( 
		shortcode_atts( 
			array(
				'team_type'					=> 'grid',
				'columns'					=> '4',
				'posts' 					=> '4',
				'show_pagination' 			=> 'no',
				'btn_txt' 					=> '',
				'categories' 				=> 'all',
				'items'						=> '2',
				'items_scroll' 				=> '1',
				'auto_play' 				=> 'true',					
				'timeout_duration' 			=> '5000',
				'infinite_loop' 			=> '',
				'margin' 					=> '30',
				'items_tablet'				=> '2',
				'items_mobile_landscape'	=> '1',
				'items_mobile_portrait'		=> '1',
				'navigation' 				=> 'true',
				'pagination' 				=> 'true',	
			), $atts 
		) 
	);
	
	global $post;
	static $team_id = 1;
	$extra_class = '';
	
	// Slider Configuration
	$data_attr = '';

	if( isset( $items ) && $items != '' ) {
		$data_attr .= ' data-items="' . $items . '" ';
	}
	
	if( isset( $items_scroll ) && $items_scroll != '' ) {
		$data_attr .= ' data-slideby="' . $items_scroll . '" ';
	}
	
	if( isset( $items_tablet ) && $items_tablet != '' ) {
		$data_attr .= ' data-items-tablet="' . $items_tablet . '" ';
	}
	
	if( isset( $items_mobile_landscape ) && $items_mobile_landscape != '' ) {
		$data_attr .= ' data-items-mobile-landscape="' . $items_mobile_landscape . '" ';
	}
	
	if( isset( $items_mobile_portrait ) && $items_mobile_portrait != '' ) {
		$data_attr .= ' data-items-mobile-portrait="' . $items_mobile_portrait . '" ';
	}
	
	if( isset( $auto_play ) && $auto_play != '' ) {
		$data_attr .= ' data-autoplay="' . $auto_play . '" ';
	}
	if( isset( $timeout_duration ) && $timeout_duration != '' ) {
		$data_attr .= ' data-autoplay-timeout="' . $timeout_duration . '" ';
	}
	
	if( isset( $infinite_loop ) && $infinite_loop != '' ) {
		$data_attr .= ' data-loop="' . $infinite_loop . '" ';
	}
	
	if( isset( $margin ) && $margin != '' ) {
		$data_attr .= ' data-margin="' . $margin . '" ';
	}
	
	if( isset( $pagination ) && $pagination != '' ) {
		$data_attr .= ' data-pagination="' . $pagination . '" ';
	}
	if( isset( $navigation ) && $navigation != '' ) {
		$data_attr .= ' data-navigation="' . $navigation . '" ';
	}	
	
	/**
	 * Team Query Args
	 */
	 
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	 
	$args = array(
		'post_type' 		=> 'tpath_team_member',
		'posts_per_page' 	=> $posts,
		'paged' 			=> $paged,
		'orderby' 		 	=> 'date',
		'order' 		 	=> 'DESC',
	);
		
	if( ! ( $categories == 'all' ) ) {
		
		$category_id = (int)$categories;
		
		$args['tax_query'] = array( array(
								'taxonomy' 	=> 'team_member_categories',
								'field' 	=> 'id',
								'terms' 	=> $category_id
							) );
	}	
		
	$team_query = new WP_Query( $args );
	
	// Classes
	$main_classes = '';
	if( isset( $team_style ) && $team_style != '' ) {
		$main_classes .= ' team-' . $team_style;
	}
	if( isset( $team_type ) && $team_type != '' ) {
		$main_classes .= ' team-type-' . $team_type;
	}		
												
	if( $team_query->have_posts() ) {
	
		if( isset( $team_type ) && $team_type == 'slider' ) {
			$output = '<div class="tpath-team-slider-wrapper'.$main_classes.'">';
			$output .= '<div id="tpath-team-slider'.$team_id.'" class="tpath-owl-carousel owl-carousel team-carousel-slider"'.$data_attr.'>';
		} else {
			$output = '<div class="tpath-team-grid-wrapper'.$main_classes.'">';
			$output .= '<div id="tpath-team-grid'.$team_id.'" class="row tpath-grid-inner team-columns-'.$columns.'">';
			
			if( $columns == "2" ) {
				$extra_class = " col-md-6 col-xs-12";
			} elseif( $columns == "3" ) {
				$extra_class = " col-md-4 col-xs-12";
			} elseif( $columns == "4" ) {
				$extra_class = " col-md-3 col-xs-12";
			}
		}	
		
		while ($team_query->have_posts()) : $team_query->the_post();
		
			$team_img = '';
			$team_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'volunteer-team' );
			$member_phone 		= get_post_meta( $post->ID, 'volunteer_member_phone', true );
			$member_email 		= get_post_meta( $post->ID, 'volunteer_member_email', true );
			$volunteer_label 	= get_post_meta( $post->ID, 'volunteer_volunteer_label', true );
			
			// Social Links
			$member_facebook 	= get_post_meta( $post->ID, 'volunteer_member_facebook', true );
			$member_twitter 	= get_post_meta( $post->ID, 'volunteer_member_twitter', true );
			$member_instagram 	= get_post_meta( $post->ID, 'volunteer_member_instagram', true );
			$member_googleplus 	= get_post_meta( $post->ID, 'volunteer_member_googleplus', true );
			$member_linkedin 	= get_post_meta( $post->ID, 'volunteer_member_linkedin', true );
			$member_pinterest 	= get_post_meta( $post->ID, 'volunteer_member_pinterest', true );
			$member_dribbble 	= get_post_meta( $post->ID, 'volunteer_member_dribbble', true );
			$member_flickr 		= get_post_meta( $post->ID, 'volunteer_member_flickr', true );
			$member_yahoo 		= get_post_meta( $post->ID, 'volunteer_member_yahoo', true );
			$member_blog 		= get_post_meta( $post->ID, 'volunteer_member_blog', true );
			
			$output .= '<div class="team-item'.$extra_class.'">';
			$output .= '<div class="team-item-wrapper">';
			
				$output .= '<div class="team-image-wrapper">';
				if( isset( $team_img ) && $team_img != '' ) {
					$output .= '<div class="team-item-img">';
						$output .= '<img src="'.esc_url($team_img[0]).'" width="'.esc_attr($team_img[1]).'" height="'.esc_attr($team_img[2]).'" alt="'.get_the_title().'" class="img-responsive" />';
					$output .= '</div>';
					
					if( isset( $volunteer_label ) && $volunteer_label != '' ) {
						$output .= '<div class="team-label">'.do_shortcode( $volunteer_label ).'</div>';
					}
				}
				$output .= '</div>';
				
				$output .= '<div class="team-content-wrapper">';

					$output .= '<h5 class="team-member-name">';
					$output .= '<a href="'. get_permalink() .'" title="'. get_the_title() .'">'. get_the_title() .'</a></h5>';
						
					if( isset( $member_phone ) && $member_phone != '' ) {
						$output .= '<div class="team-member_phone"><i class="flaticon flaticon-telephone114"></i> <span>Mobile</span> :'.do_shortcode( $member_phone ).'</div>';
					}
					if( isset( $member_email ) && $member_email != '' ) {
						$output .= '<div class="team-member_email"><i class="fa fa-envelope-o"></i> <span>Email</span> : <a href="mailto:'.do_shortcode( $member_email ).'">'.do_shortcode( $member_email ).'</a></div>';
					}
						
					$output .= '<div class="member-social">';
					$output .= '<ul class="tpath-member-social-icons list-inline">';
						
					if( isset( $member_facebook ) && $member_facebook != '' ) {
						$output .= '<li class="facebook"><a target="_blank" href="'.esc_url($member_facebook).'"><i class="fa fa-facebook"></i></a></li>';
					}	
					if( isset( $member_twitter ) && $member_twitter != '' ) {
						$output .= '<li class="twitter"><a target="_blank" href="'.esc_url($member_twitter).'"><i class="fa fa-twitter"></i></a></li>';
					}
					if( isset( $member_instagram ) && $member_instagram != '' ) {
						$output .= '<li class="instagram"><a target="_blank" href="'.esc_url($member_instagram).'"><i class="fa fa-instagram"></i></a></li>';
					}
					if( isset( $member_googleplus ) && $member_googleplus != '' ) {
						$output .= '<li class="googleplus"><a target="_blank" href="'.esc_url($member_googleplus).'"><i class="fa fa-google-plus"></i></a></li>';
					}	
					if( isset( $member_pinterest ) && $member_pinterest != '' ) {
						$output .= '<li class="pinterest"><a target="_blank" href="'.esc_url($member_pinterest).'"><i class="fa fa-pinterest"></i></a></li>';
					}
					if( isset( $member_blog ) && $member_blog != '' ) {
						$output .= '<li class="blogger"><a target="_blank" href="'.esc_url($member_blog).'"><i class="fa icon-blogger"></i></a></li>';
					}
					if( isset( $member_dribbble ) && $member_dribbble != '' ) {
						$output .= '<li class="dribbble"><a target="_blank" href="'.esc_url($member_dribbble).'"><i class="fa fa-dribbble"></i></a></li>';
					}
					if( isset( $member_linkedin ) && $member_linkedin != '' ) {
						$output .= '<li class="linkedin"><a target="_blank" href="'.esc_url($member_linkedin).'"><i class="fa fa-linkedin"></i></a></li>';
					}
					if( isset( $member_flickr ) && $member_flickr != '' ) {
						$output .= '<li class="flickr"><a target="_blank" href="'.esc_url($member_flickr).'"><i class="fa fa-flickr"></i></a></li>';
					}
					if( isset( $member_yahoo ) && $member_yahoo != '' ) {
						$output .= '<li class="yahoo"><a target="_blank" href="'.esc_url($member_yahoo).'"><i class="fa fa-yahoo"></i></a></li>';
					}
						
					$output .= '</ul>';
					$output .= '</div>';
					if( isset( $btn_txt ) && $btn_txt != '' ) { 							
						$output .= '<div class="team-member-btn"><a class="btn btn_trans_themecolor" href="'. get_permalink() .'" title="'. get_the_title() .'">'. $btn_txt .'</a></div>';
					}
				$output .= '</div>';
				$output .= '</div>';
			$output .= '</div>';

		endwhile;
	
		$output .= '</div>';
		
		if( isset( $team_type ) && $team_type == 'grid' ) {
			if( isset( $show_pagination ) && $show_pagination == 'yes' ) {
				$output .= volunteer_pagination( $team_query->max_num_pages, '' );
			}
		}
			
		$output .= '</div>';
		
	}
	
	$team_id++;
	wp_reset_postdata();
	
	return $output;
}
add_shortcode( 'volunteer_vc_team', 'volunteer_vc_team_shortcode' );

/**
 * The VC Element Config Functions
 */
function volunteer_vc_team_map() {
	vc_map( 
		array(
			"icon" 			=> 'tpath-vc-block',
			"name" 			=> esc_html__( 'Team', 'volunteer' ),
			"base" 			=> 'volunteer_vc_team',
			"category" 		=> esc_html__( 'Theme Addons', 'volunteer' ),		
			"params" 		=> array(
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__( "Team Type", "volunteer" ),
					"param_name" 	=> "team_type",
					"admin_label" 	=> true,
					"value" 		=> array_flip(array(
									'grid' 		 => 'Grid',
									'slider' 	 => 'Slider',									
									)),
				),
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__("Team Columns", "volunteer"),
					"param_name" 	=> "columns",
					'dependency'	=> array(
						'element'	=> "team_type",
						'value'		=> 'grid'
					),
					"value" 		=> array_flip(array(
									'2' 	=> '2 Columns',
									'3'  	=> '3 Columns',
									'4' 	=> '4 Columns',
									)),
					"description" 	=> ''
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> esc_html__("How Many Posts to Show?", "volunteer"),
					"param_name" 	=> "posts",
					"admin_label" 	=> true,
					"value" 		=> "4",
					"description" 	=> ""
				),
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__( "Show Pagination", "volunteer" ),
					"param_name" 	=> "show_pagination",
					'dependency'	=> array(
						'element'	=> "team_type",
						'value'		=> 'grid'
					),
					"value" 		=> array_flip(array(
									'no' 		=> 'No',
									'yes' 	 	=> 'Yes',
									)),
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> esc_html__("Button Text", "volunteer"),
					"param_name" 	=> "btn_txt",
					"value" 		=> "",
				),
				// Slider
				array(
					"type"			=> "textfield",
					"heading"		=> esc_html__( "Items to Display", "volunteer" ),
					"param_name"	=> "items",
					'dependency'	=> array(
						'element'	=> "team_type",
						'value'		=> 'slider'
					),
					"group"			=> esc_html__( "Slider", "volunteer" ),
				),
				array(
					"type"			=> "textfield",
					"heading"		=> esc_html__( "Items to Scrollby", "volunteer" ),
					"param_name"	=> "items_scroll",
					'dependency'	=> array(
						'element'	=> "team_type",
						'value'		=> 'slider'
					),
					"group"			=> esc_html__( "Slider", "volunteer" ),
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> esc_html__( "Auto Play", 'volunteer' ),
					'param_name'	=> "auto_play",
					'dependency'	=> array(
						'element'	=> "team_type",
						'value'		=> 'slider'
					),		
					'value'			=> array(
						esc_html__( 'True', 'volunteer' )	=> 'true',
						esc_html__( 'False', 'volunteer' )	=> 'false',
					),
					"group"			=> esc_html__( "Slider", "volunteer" ),
				),
				array(
					'type'			=> 'textfield',
					'heading'		=> esc_html__( 'Timeout Duration (in milliseconds)', 'volunteer' ),
					'param_name'	=> "timeout_duration",
					'value'			=> "5000",
					'dependency'	=> array(
						'element'	=> "auto_play",
						'value'		=> 'true'
					),
					"group"			=> esc_html__( "Slider", "volunteer" ),
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> esc_html__( "Infinite Loop", 'volunteer' ),
					'param_name'	=> "infinite_loop",
					'dependency'	=> array(
						'element'	=> "team_type",
						'value'		=> 'slider'
					),
					'value'			=> array(
						esc_html__( 'False', 'volunteer' )	=> 'false',
						esc_html__( 'True', 'volunteer' )	=> 'true',
					),
					"group"			=> esc_html__( "Slider", "volunteer" ),
				),
				array(
					"type"			=> "textfield",
					"heading"		=> esc_html__( "Margin ( Items Spacing )", "volunteer" ),
					"param_name"	=> "margin",
					'dependency'	=> array(
						'element'	=> "team_type",
						'value'		=> 'slider'
					),
					"group"			=> esc_html__( "Slider", "volunteer" ),
				),
				array(
					"type"			=> "textfield",
					"heading"		=> esc_html__( "Items To Display in Tablet", "volunteer" ),
					"param_name"	=> "items_tablet",
					'dependency'	=> array(
						'element'	=> "team_type",
						'value'		=> 'slider'
					),
					"group"			=> esc_html__( "Slider", "volunteer" ),
				),
				array(
					"type"			=> "textfield",
					"heading"		=> esc_html__( "Items To Display In Mobile Landscape", "volunteer" ),
					"param_name"	=> "items_mobile_landscape",
					'dependency'	=> array(
						'element'	=> "team_type",
						'value'		=> 'slider'
					),
					"group"			=> esc_html__( "Slider", "volunteer" ),
				),
				array(
					"type"			=> "textfield",
					"heading"		=> esc_html__( "Items To Display In Mobile Portrait", "volunteer" ),
					"param_name"	=> "items_mobile_portrait",
					'dependency'	=> array(
						'element'	=> "team_type",
						'value'		=> 'slider'
					),
					"group"			=> esc_html__( "Slider", "volunteer" ),
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Navigation", "volunteer" ),
					"param_name"	=> "navigation",
					'dependency'	=> array(
						'element'	=> "team_type",
						'value'		=> 'slider'
					),
					"value"			=> array(
						esc_html__( "Yes", "volunteer" )	=> "true",
						esc_html__( "No", "volunteer" )		=> "false" ),
					"group"			=> esc_html__( "Slider", "volunteer" ),
				),
				array(
					"type"			=> 'dropdown',
					"heading"		=> esc_html__( "Pagination", "volunteer" ),
					"param_name"	=> "pagination",
					'dependency'	=> array(
						'element'	=> "team_type",
						'value'		=> 'slider'
					),
					"value"			=> array(
						esc_html__( "Yes", "volunteer" )	=> "true",
						esc_html__( "No", "volunteer" )		=> "false" ),
					"group"			=> esc_html__( "Slider", "volunteer" ),
				),
			)
		) 
	);
	
}
add_action( 'vc_before_init', 'volunteer_vc_team_map');