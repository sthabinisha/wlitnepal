<?php 
/**
 * The Shortcode   
 */  
function volunteer_counter_block_shortcode( $atts, $content = null ) {  
	
	$output = '';
	
	extract(  
		shortcode_atts( 
			array(
				'alignment' 		=> 'center',
				'counter_title' 	=> '',
				'counter_value' 	=> '',
				'counter_font_size' => 'normal',
				'type' 				=> 'none',
				'icon_flaticons' 	=> '',
				'icon_fontawesome' 	=> '',
				'icon_lineicons' 	=> '',
				'icon_color'		=> '',
				'icon_bgcolor'		=> '',
				'counter_color'		=> '',
				'title_color' 		=> '',
				), $atts 
		) 
	);

	$text_alignment = ( isset( $alignment ) && $alignment != '' ) ? ' text-'. strtolower( $alignment ) : '';
	
	$icon_style = '';
	if(isset( $icon_color ) && $icon_color != '') {
		$icon_style = ' style="color: '.$icon_color.';"';
	}
	
	$counter_style = '';
	if(isset( $counter_color ) && $counter_color != '') {
		$counter_style = ' style="color: '.$counter_color.';"';
	}
	
	$title_style = '';
	if(isset( $title_color ) && $title_color != '') {
		$title_style = ' style="color: '.$title_color.';"';
	}
	
	wp_enqueue_script( 'volunteer-countto-js' );
	
	$output .= '<div class="tpath-counter-section size-'.$counter_font_size.''.$text_alignment.'">';
		$output .= '<div class="tpath-count-number" data-count="'.$counter_value.'">';
			$output .= '<div class="counter-value">';
				if( isset( $type ) && $type != 'none' ) {
					if( isset( ${'icon_'. $type} ) && ${'icon_'. $type} != '' ) {
						$output .= '<span class="tpath-counter-icon" '.$icon_style.'><i class="'. esc_attr( ${'icon_'. $type} ) . ' counter-icon"'.$icon_style.'></i></span>';
					}
				}
				$output .= '<span class="counter"'.$counter_style.'></span>';
			$output .= '</div>';
		$output .= '</div>';
		$output .= '<div class="counter-info">';
			$output .= '<div class="counter-title"'.$title_style.'>';
				$output .= $counter_title;
			$output .= '</div>';
		$output .= '</div>';
	$output .= '</div>';
	
	return $output;
}
add_shortcode( 'volunteer_counter_block', 'volunteer_counter_block_shortcode' );

/**
 * The VC Element Config Functions
 */
function volunteer_vc_counters_section_shortcode() {
	vc_map( 
		array(
			"icon" 			=> 'tpath-vc-block',
			"name" 			=> esc_html__( 'Counters Block', 'volunteer' ),
			"base" 			=> 'volunteer_counter_block',
			"category" 		=> esc_html__( 'Theme Addons', 'volunteer' ),
			"params" 		=> array(
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__( "Alignment", "volunteer" ),
					"param_name" 	=> "alignment",
					"value" 		=> array_flip(array(
								'none' 	 => 'None',
								'left' 	 => 'Left',
								'right'  => 'Right',
								'center' => 'Center',
								)),					
				),
				array(
					"type" 			=> "textfield",
					"heading" 		=> esc_html__("Counter Title", "volunteer"),
					"param_name" 	=> "counter_title",
					"value" 		=> '',
				),	
				array(
					"type" 			=> "textfield",
					"heading" 		=> esc_html__("Counter Value", "volunteer"),
					"param_name" 	=> "counter_value",
					"value" 		=> '',
				),
				array(
					"type" 			=> "dropdown",
					"heading" 		=> esc_html__( "Counter Number Size", "volunteer" ),
					"param_name" 	=> "counter_font_size",
					"std" 			=> "normal",
					"value" 		=> array_flip(array(
									'normal' 	 => 'Normal',
									'small' 	 => 'Small',
									)),
				),
				array(
					'type' 			=> "dropdown",
					'heading' 		=> esc_html__( "Choose from Icon library", "volunteer" ),
					'value' 		=> array(
						esc_html__( 'None', 'volunteer' ) 			=> 'none',
						esc_html__( 'Flaticons', 'volunteer' ) 		=> 'flaticons',
						esc_html__( 'Font Awesome', 'volunteer' ) 	=> 'fontawesome',
						esc_html__( 'Lineicons', 'volunteer' ) 		=> 'lineicons',						
					),
					'admin_label' 	=> true,
					'param_name' 	=> 'type',
					'description' 	=> esc_html__( "Select icon library.", "volunteer" ),
					'group' 		=> esc_html__( "Icon", "volunteer" ),
				),
				array(
					'type' 			=> 'iconpicker',
					'heading' 		=> esc_html__( 'Icon', 'volunteer' ),
					'param_name' 	=> 'icon_flaticons',
					'value' 		=> '', // default value to backend editor admin_label
					'settings' 		=> array(
						'emptyIcon' 	=> true, // default true, display an "EMPTY" icon?
						'type' 			=> 'flaticons',
						'iconsPerPage' 	=> 4000, // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
					),
					'dependency' 	=> array(
						'element' 	=> 'type',
						'value' 	=> 'flaticons',
					),
					'description' 	=> esc_html__( 'Select icon from library.', 'volunteer' ),
					'group' 		=> esc_html__( "Icon", "volunteer" ),
				),
				array(
					'type' 			=> 'iconpicker',
					'heading' 		=> esc_html__( 'Icon', 'volunteer' ),
					'param_name' 	=> 'icon_fontawesome',
					'value' 		=> '', // default value to backend editor admin_label
					'settings' 		=> array(
						'emptyIcon' 	=> true, // default true, display an "EMPTY" icon?
						'iconsPerPage' 	=> 4000, // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
					),
					'dependency' 	=> array(
						'element' 	=> 'type',
						'value' 	=> 'fontawesome',
					),
					'description' 	=> esc_html__( 'Select icon from library.', 'volunteer' ),
					'group' 		=> esc_html__( "Icon", "volunteer" ),
				),				
				array(
					'type' 			=> 'iconpicker',
					'heading' 		=> esc_html__( 'Icon', 'volunteer' ),
					'param_name' 	=> 'icon_lineicons',
					'value' 		=> '', // default value to backend editor admin_label
					'settings' 		=> array(
						'emptyIcon' 	=> true, // default true, display an "EMPTY" icon?
						'type' 			=> 'simpleicons',
						'iconsPerPage' 	=> 4000, // default 100, how many icons per/page to display
					),
					'dependency' 	=> array(
						'element' 	=> 'type',
						'value' 	=> 'lineicons',
					),
					'description' 	=> esc_html__( 'Select icon from library.', 'volunteer' ),
					'group' 		=> esc_html__( "Icon", "volunteer" ),
				),
				array(            
					"type" 			=> "colorpicker",
					"heading" 		=> esc_html__( "Counter Number Color", "volunteer" ),
					"param_name" 	=> "counter_color",
					"value" 		=> '',
					'group' 		=> esc_html__( "Design", "volunteer" ),
				),
				array(            
					"type" 			=> "colorpicker",
					"heading" 		=> esc_html__( "Title color", "volunteer" ),
					"param_name" 	=> "title_color",
					"value" 		=> '',
					'group' 		=> esc_html__( "Design", "volunteer" ),
				),
				array(            
					"type" 			=> "colorpicker",
					"heading" 		=> esc_html__( "Icon color", "volunteer" ),
					"param_name" 	=> "icon_color",
					"value" 		=> '',					
					'group' 		=> esc_html__( "Design", "volunteer" ),
				),
			)
		) 
	);
}
add_action( 'vc_before_init', 'volunteer_vc_counters_section_shortcode' );