<?php
/**
 * Theme Functions and Definitions
 *
 * @package TemplatePath
 */
 
// Set path to theme specific functions
$library_path = get_template_directory() . '/lib/';
$includes_path = get_template_directory() . '/includes/';
$admin_path = get_template_directory() . '/admin/';

// Define path to theme specific functions
define( 'VOLUNTEER_LIBRARY', $library_path );
define( 'VOLUNTEER_INCLUDES', $includes_path );
define( 'VOLUNTEER_ADMIN', $admin_path );
define( 'VOLUNTEER_ADMIN_URL', get_template_directory_uri() . '/admin/' );
define( 'VOLUNTEER_THEME_URL', get_template_directory_uri() );
define( 'VOLUNTEER_THEME_DIR', get_template_directory() );
define( 'VOLUNTEER_THEME_COLOR_SCHEMES', get_template_directory() . '/color-schemes/' );
 
/**
 * Theme Options Panel Admin
 */
require VOLUNTEER_ADMIN . 'index.php';

class Volunteer_Init {

	public function __construct() {
	
		/**
		 * Register Sidebar
		 */
		include_once VOLUNTEER_INCLUDES . 'sidebar-register.php';
		
		/**
		 * Theme Actions and Filters
		 */
		require_once VOLUNTEER_INCLUDES . 'theme-filters.php';		
		
		/**
		 * Theme Functions
		 */
		require_once VOLUNTEER_INCLUDES . 'theme-functions.php';
		
		/**
		 * Blog Functions
		 */
		require_once VOLUNTEER_THEME_DIR . '/partials/blog-functions.php';
		
		/**
		 * Admin Custom Meta Boxes
		 */
		include_once VOLUNTEER_INCLUDES . 'metaboxes.php';
		
		/**
		 * Admin Custom Meta Box Fields
		 */
		include_once VOLUNTEER_INCLUDES . 'register-metabox-types.php';
		
		/**
		 * Bootstrap Library Files
		 */
		require_once VOLUNTEER_LIBRARY . 'wp_bootstrap_navwalker.php';
		require_once VOLUNTEER_LIBRARY . 'wp_bootstrap_mobile_navwalker.php';
		
		/**
		 * Mega Menu Framework
		 */
		require_once VOLUNTEER_INCLUDES . 'class-megamenu-framework.php';
		
		/**
		 * Woocommerce Config
		 */
		if( class_exists('Woocommerce') ) {
			include_once( VOLUNTEER_INCLUDES . 'woo-config.php' );
		}
		
		/**
		 * Breadcrumbs
		 */
		require_once VOLUNTEER_INCLUDES . 'class-tpath-breadcrumbs.php';
		
		/**
		 * Demo Importer
		 */
		include_once VOLUNTEER_INCLUDES . 'plugins/tpath-importer/demo-importer.php';
		
		/**
		 * TGM Plugin Activation
		 */
		include_once VOLUNTEER_THEME_DIR . '/includes/class-tgm-plugin-activation.php';
		
		/**
		 * Visual Composer ( Page Builder Elements )
		 */
		if( function_exists('vc_set_as_theme') ) {
			require_once VOLUNTEER_THEME_DIR . '/vc_includes/vc_init.php';
		}
		
	}
	
}
$volunteer = new Volunteer_Init();

/*
* Check Registered Post types
*/
function volunteer_check_registered_post_types() {
    // types will be a list of the post type names
    $types = get_post_types( array('_builtin' => false) );
	
	if( in_array( 'tpath_portfolio', $types ) ) {
		define( 'VOLUNTEER_PORTFOLIO_ACTIVE', true );
	} else {
		define( 'VOLUNTEER_PORTFOLIO_ACTIVE', false );
	}
	
	if( in_array( 'tpath_testimonial', $types ) ) {
		define( 'VOLUNTEER_TESTIMONIAL_ACTIVE', true );
	} else {
		define( 'VOLUNTEER_TESTIMONIAL_ACTIVE', false );
	}
	
	if( in_array( 'tpath_team_member', $types ) ) {
		define( 'VOLUNTEER_TEAM_ACTIVE', true );
	} else {
		define( 'VOLUNTEER_TEAM_ACTIVE', false );
	}
	
	if( in_array( 'tpath_clients', $types ) ) {
		define( 'VOLUNTEER_CLIENTS_ACTIVE', true );
	} else {
		define( 'VOLUNTEER_CLIENTS_ACTIVE', false );
	}
	
} 

add_action( 'init', 'volunteer_check_registered_post_types' );

/*
* Post Star Ratings
*/
require_once VOLUNTEER_THEME_DIR . '/includes/class-tpath-ratings.php';

$config_options = array();	
$config_options[] = array(
	'meta_key'			=>	'volunteer_post_item_rating',
	'name'				=>	'Rating',
	'disable_on_update'	=>	FALSE,
	'max_rating_size'	=> 	5,
	'min_rating_size'	=> 	0,
	'active_post_types'	=>	array( 'tpath_testimonial' )
);

//Instatiate plugin class and pass config options array
new VolunteerPostRatings( $config_options );