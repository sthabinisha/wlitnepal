/*
 * Megamenu Framework
 * 
 */

( function( $ ) {

	"use strict";
	
	var volunteer_megamenu = {

		menu_item_move: function() {
			$(document).on( 'mouseup', '.menu-item-bar', function( event, ui ) {
				if( ! $(event.target).is('a') ) {
					setTimeout( volunteer_megamenu.update_megamenu_fields, 200 );
				}
			});
		},

		update_megamenu_status: function() {

			$(document).on( 'click', '.edit-menu-item-volunteer-megamenu-status', function() {
				var parent_menu_item = $( this ).parents( '.menu-item:eq(0)' );

				if( $( this ).is( ':checked' ) ) {
					parent_menu_item.addClass( 'tpath-megamenu-active' );
				} else 	{
					parent_menu_item.removeClass( 'tpath-megamenu-active' );
				}

				volunteer_megamenu.update_megamenu_fields();
			});
		},

		update_megamenu_fields: function() {
			var menu_items = $( '.menu-item');

			menu_items.each( function( i ) 	{

				var volunteer_megamenu_status = $( '.edit-menu-item-volunteer-megamenu-status', this );

				if( ! $(this).is( '.menu-item-depth-0' ) ) {
					var check_against = menu_items.filter( ':eq('+(i-1)+')' );

					if( check_against.is( '.tpath-megamenu-active' ) ) {
						volunteer_megamenu_status.attr( 'checked', 'checked' );
						$(this).addClass( 'tpath-megamenu-active' );
					} else {
						volunteer_megamenu_status.attr( 'checked', '' );
						$(this).removeClass( 'tpath-megamenu-active' );
					}
				} else {
					if( volunteer_megamenu_status.attr( 'checked' ) ) {
						$(this).addClass( 'tpath-megamenu-active' );
					}
				}
			});
		}

	};
	
	$(document).ready(function(){
	
		volunteer_megamenu.menu_item_move();
		volunteer_megamenu.update_megamenu_status();
		volunteer_megamenu.update_megamenu_fields();
		
	});
	
})( jQuery );