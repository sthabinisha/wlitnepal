<?php
/**
 * Single Post Template
 *
 * @package TemplatePath
 */
 
global $volunteer_options;
get_header();
?>
<div class="container">
	<div id="main-wrapper" class="tpath-row row">
		<div id="single-sidebar-container" class="single-sidebar-container main-col-full">
			<div class="tpath-row row">
				<div id="primary" class="content-area <?php volunteer_primary_content_classes(); ?>">
					<div id="content" class="site-content">	
						<?php if ( have_posts() ):
								while ( have_posts() ): the_post(); 
								
								$post_content_block 	= get_post_meta( $post->ID, 'volunteer_additional_content_block', true ); 
								$post_format = get_post_format(); ?>
								
								<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
									<div class="posts-inner-container clearfix">
										<div class="posts-content-container">
											<?php if ( has_post_thumbnail() && ! post_password_required() ) {
												echo volunteer_blog_featured_image( 'volunteer-blog-large', $post_format, 'large', 'single' );													
											} ?>
											<div class="post-content">				
												<div class="left-content">
													<?php echo volunteer_blog_posted_info(); ?>
												</div>
												
												<div class="right-content">
													<div class="entry-header">
														<?php echo volunteer_blog_entry_meta( 'large' ); ?>
													</div>
													<div class="entry-summary">
														<?php echo volunteer_blog_content( '', 'full_content' ); ?>
													</div>
													<?php if( $volunteer_options['blog_social_sharing'] || has_tag() ) { ?>
													<div class="tag-share-wrapper">
														<?php if( has_tag() ) { ?>
															<div class="tags-share-section">					
																<?php the_tags('<div class="post-tags"><h6 class="inline-block">'.esc_html__('Tags:', 'volunteer').'</h6>', ',', '</div>'); ?>
															</div>			
														<?php } ?>
														<?php if( $volunteer_options['blog_social_sharing'] ) {
															echo '<div class="share-options">';
															volunteer_display_social_sharing_icons();
															echo '</div>';
														} ?>
													</div>
													<?php } ?>
												</div>
											</div>
										</div>
									</div>
								</article>
								
								<?php if( $volunteer_options['blog_author_info'] ) {
									volunteer_author_info();
								}
								
								if( ! $volunteer_options['blog_prev_next'] ) { 
									volunteer_postnavigation();
								}
								
								if( $volunteer_options['blog_comments'] ) { 
									comments_template();
								}					
								endwhile;
								
								else : ?>
								<?php get_template_part( 'content', 'none' ); ?>
						<?php endif; ?>				
					</div><!-- #content -->
				</div><!-- #primary -->
			
				<?php get_sidebar(); ?>
			</div>
		</div><!-- #single-sidebar-container -->
		
	</div><!-- #main-wrapper -->
</div><!-- .container -->	

<?php if( isset( $post_content_block ) && ( $post_content_block != '' && $post_content_block != 0 ) ) {
	echo volunteer_block( $post_content_block );
} ?>
<?php get_footer(); ?>