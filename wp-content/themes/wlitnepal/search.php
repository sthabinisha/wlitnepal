<?php
/**
 * Search Page Template
 *
 * @package TemplatePath
 */
 
global $volunteer_options;
get_header(); ?>
<div class="container">
	<div id="main-wrapper" class="tpath-row row">
		<div id="single-sidebar-container" class="single-sidebar-container main-col-full">
			<div class="tpath-row row">
				<div id="primary" class="content-area <?php volunteer_primary_content_classes(); ?>">
					<div id="content" class="site-content">
						<?php if ( have_posts() && strlen( trim( get_search_query() ) ) != 0 ): ?>
							<div class="tpath-search-page search-page-form">
								<h4><?php esc_html_e( 'New Search', 'volunteer' ); ?></h4>
																	
								<p><?php esc_html_e('If you didn\'t find what you were looking for, try another search', 'volunteer'); ?></p>
								<form role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" class="search-form">
									<div class="input-group">
										<input type="text" value="" name="s" id="s" class="form-control" placeholder="<?php esc_html_e('Search', 'volunteer'); ?>" />
										<span class="input-group-btn">
											<button class="btn btn-search" type="submit"><i class="fa fa-search"></i></button>
										</span>
									</div>
								</form>	
							</div>
							
							<div id="archive-posts-container" class="tpath-posts-container tpath-search-results large-layout scroll-pagination clearfix">
								<?php while ( have_posts() ): the_post();
										$post_id = get_the_ID();
										$post_format = get_post_format();
										
										$post_format_class = '';
										if( $post_format == 'image' ) {
											$post_format_class = ' image-format';
										} elseif( $post_format == 'quote' ) {
											$post_format_class = ' quote-image';
										} ?>
										
										<article id="post-<?php echo esc_attr( $post_id ); ?>" <?php post_class( 'large-posts' ); ?>>
											<div class="posts-inner-container clearfix">
												<div class="posts-content-container">
													<?php if ( has_post_thumbnail() && ! post_password_required() ) {
														echo volunteer_blog_featured_image( 'volunteer-blog-large', $post_format, 'large' );													
													} ?>
													<div class="post-content">				
														<div class="left-content">
															<?php echo volunteer_blog_posted_info(); ?>
														</div>
														
														<div class="right-content">
															<div class="entry-header">
																<?php echo volunteer_blog_title();			
																echo volunteer_blog_entry_meta( 'large' ); ?>
															</div>
															<div class="entry-summary">
																<?php echo volunteer_blog_content( $volunteer_options['blog_excerpt_length_large'] ); ?>
															</div>
															<div class="entry-footer">
																<?php echo volunteer_blog_footer(); ?>
															</div>
														</div>
													</div>
												</div>
											</div>
										</article>
								<?php endwhile; ?>
							</div>							
								
						<?php else : ?>
							<div class="tpath-search-no-results">
								<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
									<div class="posts-inner-container">
										<div class="posts-content-container">
											<div class="entry-header">			   
												<h1 class="entry-title"><?php esc_html_e('Nothing Found', 'volunteer'); ?></h1>			
											</div><!-- .entry-header -->
											<div class="entry-content">
												<p><?php esc_html_e('Sorry, but no posts matched your search terms.', 'volunteer'); ?></p>
											</div><!-- .entry-content -->
										</div><!-- .posts-content-container -->		
									</div><!-- .posts-inner-container -->
								</article><!-- #post -->
							</div>
						<?php endif; ?>
													
						<?php echo volunteer_pagination( $pages = '', 'pagination' );	?>
					</div><!-- #content -->
				</div><!-- #primary -->
			
				<?php get_sidebar(); ?>
			</div>
		</div><!-- #single-sidebar-container -->
	
	</div><!-- #main-wrapper -->
</div><!-- .container -->
<?php get_footer(); ?>