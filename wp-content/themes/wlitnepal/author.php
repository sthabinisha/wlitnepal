<?php
/**
 * Author Template
 *
 * @package TemplatePath
 */
 
global $volunteer_options;
get_header();
 
$container_class = $scroll_type = '';
if( $volunteer_options['archive_blog_type'] == 'grid' ) {	
	if( $volunteer_options['blog_grid_columns'] != '' ) {
		if( $volunteer_options['blog_grid_columns'] == 'two' ) {
			$container_class = 'grid-layout grid-col-2';
		} elseif ( $volunteer_options['blog_grid_columns'] == 'three' ) {
			$container_class = 'grid-layout grid-col-3';
		} elseif ( $volunteer_options['blog_grid_columns'] == 'four' ) {
			$container_class = 'grid-layout grid-col-4';
		}
	}
	$post_class = 'grid-posts';
	$page_type_layout = 'grid';
	$image_size = 'volunteer-blog-list';
	$excerpt_limit = $volunteer_options['blog_excerpt_length_grid'];
	
} elseif( $volunteer_options['archive_blog_type'] == 'large' ) {
	$container_class = 'large-layout';
	$post_class = 'large-posts';
	$image_size = 'volunteer-blog-large';
	$page_type_layout = 'large';
	$excerpt_limit = $volunteer_options['blog_excerpt_length_large'];
	
} elseif( $volunteer_options['archive_blog_type'] == 'list' ) {
	$container_class = 'list-layout';
	$post_class = 'list-posts';
	$image_size = 'volunteer-blog-list';
	$page_type_layout = 'list';
	$list_fullwidth = $volunteer_options['blog_list_fullwidth'];
	if( isset( $list_fullwidth ) && $list_fullwidth == 'no' ) {
		$excerpt_limit = 15;
	} else {
		$excerpt_limit = 30;
	}
	$list_fullwidth = $volunteer_options['blog_list_fullwidth'];
	if( isset( $list_fullwidth ) && $list_fullwidth == 'no' ) {
		$container_class .= ' list-columns';
	} else {
		$container_class .= ' list-fullwidth';
	}
}

if( $volunteer_options['disable_blog_pagination'] ) {
	$scroll_type = "infinite";
	$scroll_type_class = " scroll-infinite";
} else {
	$scroll_type = "pagination";
	$scroll_type_class = " scroll-pagination";
}
?>
<div class="container">
	<div id="main-wrapper" class="tpath-row row">
		<div id="single-sidebar-container" class="single-sidebar-container main-col-full">
			<div class="tpath-row row">	
				<div id="primary" class="content-area <?php volunteer_primary_content_classes(); ?>">
					<div id="content" class="site-content">
						<?php echo volunteer_author_info(); ?>
						<div id="archive-posts-container" class="tpath-posts-container <?php echo esc_attr( $container_class ); ?><?php echo esc_attr( $scroll_type_class ); ?> clearfix">
							<?php if ( have_posts() ):
								while ( have_posts() ): the_post();
								
									$post_id = get_the_ID();
									$post_format = get_post_format();
									
									$post_format_class = '';
									if( $post_format == 'image' ) {
										$post_format_class = ' image-format';
									} elseif( $post_format == 'quote' ) {
										$post_format_class = ' quote-image';
									} ?>
									
									<article id="post-<?php echo esc_attr( $post_id ); ?>" <?php post_class($post_class); ?>>
										<div class="posts-inner-container clearfix">
											<div class="posts-content-container">
												<?php if ( has_post_thumbnail() && ! post_password_required() ) {
													echo volunteer_blog_featured_image( $image_size, $post_format, $page_type_layout );													
												} ?>
												<div class="post-content">
													<?php if( $page_type_layout != 'list' ) { ?>
													<div class="left-content">
														<?php echo volunteer_blog_posted_info(); ?>
													</div>	
													<?php } ?>
													
													<?php if( $page_type_layout != 'list' ) { ?>
													<div class="right-content">
													<?php } ?>
														<div class="entry-header">
															<?php echo volunteer_blog_title();
															if( $page_type_layout != 'list' ) {
																echo volunteer_blog_entry_meta( $page_type_layout );
															} ?>
														</div>
														<div class="entry-summary">
															<?php echo volunteer_blog_content( $excerpt_limit ); ?>
														</div>
														<div class="entry-footer">
															<?php echo volunteer_blog_footer(); ?>
														</div>
													<?php if( $page_type_layout != 'list' ) { ?>
													</div>
													<?php } ?>
												</div>
											</div>
										</div>
									</article>
									
								<?php endwhile;
								
								else :
									get_template_part( 'content', 'none' );
							endif; ?>
							
						</div>
						
						<?php echo volunteer_pagination( $pages = '', $scroll_type );
						
						wp_reset_postdata(); ?>
						
					</div><!-- #content -->
				</div><!-- #primary -->
			
				<?php get_sidebar(); ?>
			</div>
		</div><!-- #single-sidebar-container -->

	</div><!-- #main-wrapper -->
</div><!-- .container -->
<?php get_footer(); ?>