<?php
/**
 * Thankyou page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<div class="woo-thankyou-wrapper">	
	
<?php if ( $order ) : ?>

	<div class="tpath-woocommerce-thank-you-page">
		
	<?php if ( $order->has_status( 'failed' ) ) : ?>

		<h6><?php _e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction.', 'volunteer' ); ?></h6>

		<h6><?php
			if ( is_user_logged_in() )
				_e( 'Please attempt your purchase again or go to your account page.', 'volunteer' );
			else
				_e( 'Please attempt your purchase again.', 'volunteer' );
		?></h6>

		<p>
			<a href="<?php echo esc_url( $order->get_checkout_payment_url() ); ?>" class="button pay"><?php _e( 'Pay', 'volunteer' ) ?></a>
			<?php if ( is_user_logged_in() ) : ?>
			<a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php _e( 'My Account', 'volunteer' ); ?></a>
			<?php endif; ?>
		</p>

	<?php else : ?>

		<h4 class="thank-you-title"><?php echo apply_filters( 'woocommerce_thankyou_order_received_title', __( 'Order Received', 'volunteer' ), $order ); ?></h4>
		<h6 class="thank-you-text"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'volunteer' ), $order ); ?></h6>

		<ul class="order_details order_info">
			<li class="order">
				<?php _e( 'Order Number:', 'volunteer' ); ?>
				<strong><?php echo wp_kses_post( $order->get_order_number() ); ?></strong>
			</li>
			<li class="date">
				<?php _e( 'Date:', 'volunteer' ); ?>
				<strong><?php echo date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ); ?></strong>
			</li>
			<li class="total">
				<?php _e( 'Total:', 'volunteer' ); ?>
				<strong><?php echo wp_kses_post( $order->get_formatted_order_total() ); ?></strong>
			</li>
			<?php if ( $order->payment_method_title ) : ?>
			<li class="method">
				<?php _e( 'Payment Method:', 'volunteer' ); ?>
				<strong><?php echo wp_kses_post( $order->payment_method_title ); ?></strong>
			</li>
			<?php endif; ?>
		</ul>
		<div class="clear"></div>

	<?php endif; ?>

	<?php do_action( 'woocommerce_thankyou_' . $order->payment_method, $order->id ); ?>
	
	</div>
	
	<div class="tpath-woocommerce-order-details">
	
	<?php do_action( 'woocommerce_thankyou', $order->id ); ?>

<?php else : ?>

	<h5><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', __( 'Thank you. Your order has been received.', 'volunteer' ), null ); ?></h5>
	
	</div>

<?php endif; ?>

</div>