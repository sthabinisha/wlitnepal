<?php  

/**
 * VC Functions
 */
require_once VOLUNTEER_THEME_DIR . '/vc_includes/admin_vc_functions.php';

/**
 * VC Base layouts
 */
require_once VOLUNTEER_THEME_DIR . '/vc_includes/vc_layouts.php';

/**
 * Page builder Additional blocks
 */

// Section Title Shortcode
if( ! function_exists('volunteer_section_title_shortcode') ) {
	require_once VOLUNTEER_THEME_DIR. '/vc_blocks/vc_section_title_block.php';
}

// Features Shortcode
if( ! function_exists('volunteer_features_box_shortcode') ) {
	require_once VOLUNTEER_THEME_DIR. '/vc_blocks/vc_features_box_block.php';
}

// Counters Shortcode
if( ! function_exists('volunteer_counter_block_shortcode') ) {
	require_once VOLUNTEER_THEME_DIR. '/vc_blocks/vc_counters_block.php';
}

// Contact Form Shortcode
if( ! function_exists('volunteer_vc_contact_form_shortcode') ) {
	require_once VOLUNTEER_THEME_DIR. '/vc_blocks/vc_contact_form.php';
}

// Portfolio Shortcode
if( ! function_exists('volunteer_portfolio_gallery_shortcode') ) {
	require_once VOLUNTEER_THEME_DIR. '/vc_blocks/vc_portfolio_block.php';
}

// Team Shortcode
if( ! function_exists('volunteer_team_shortcode') ) {
	require_once VOLUNTEER_THEME_DIR. '/vc_blocks/vc_team_block.php';
}

// Testimonial Shortcode
if( ! function_exists('volunteer_testimonial_slider_shortcode') ) {
	require_once VOLUNTEER_THEME_DIR. '/vc_blocks/vc_testimonial_slider_block.php';
}

// Clients Shortcode
if( ! function_exists('volunteer_clients_slider_shortcode') ) {
	require_once VOLUNTEER_THEME_DIR. '/vc_blocks/vc_clients_slider_block.php';
}

// Google Map Shortcode
if( ! function_exists('volunteer_google_map_shortcode') ) {
	require_once VOLUNTEER_THEME_DIR. '/vc_blocks/vc_google_map_block.php';
}

// Blog Shortcode
if( ! function_exists('volunteer_blog_posts_shortcode') ) {
	require_once VOLUNTEER_THEME_DIR. '/vc_blocks/vc_blog_block.php';
}

// Latest Posts Shortcode
if( ! function_exists('volunteer_latest_posts_shortcode') ) {
	require_once VOLUNTEER_THEME_DIR. '/vc_blocks/vc_latest_posts.php';
}

// Pricing Table Shortcode
if( ! function_exists('volunteer_vc_pricing_table_shortcode') ) {
	require_once VOLUNTEER_THEME_DIR. '/vc_blocks/vc_pricing_table.php';
}

// Day Counter Shortcode
if( ! function_exists('volunteer_day_counter_shortcode') ) {
	require_once VOLUNTEER_THEME_DIR. '/vc_blocks/vc_day_counter_block.php';
}
 
// Content Carosuel Shortcode
if( ! function_exists('volunteer_vc_content_carousel') ) {
	require_once VOLUNTEER_THEME_DIR. '/vc_blocks/vc_content_carousel_block.php';
}
if( class_exists( 'Charitable' ) ) {
	// Charitable Campaigns Shortcode
	if( ! function_exists('volunteer_vc_campaign_shortcode') ) {
		require_once VOLUNTEER_THEME_DIR. '/vc_blocks/vc_charitable_campaign_block.php';
	}
}

// Events List And Grid Shortcode
if( ! function_exists('volunteer_vc_events_list_shortcode') ) {
	require_once VOLUNTEER_THEME_DIR. '/vc_blocks/vc_events_list.php';
}