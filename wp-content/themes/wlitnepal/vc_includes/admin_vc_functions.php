<?php 
/**
 * Force Visual Composer to initialize as "built into the theme".
 * This will hide certain tabs under the Settings -> Visual Composer
 */
if( function_exists('vc_set_as_theme') ) {
	function Volunteer_vcSetAsTheme() {
		vc_set_as_theme( $disable_updater = true );
	}
	add_action( 'vc_before_init', 'Volunteer_vcSetAsTheme' );
}

// Get VC CSS Animation
function volunteer_vc_animation( $css_animation ) {
	$output = '';
	if ( $css_animation != '' ) {
		wp_enqueue_script( 'waypoints' );
		$output = ' wpb_animate_when_almost_visible wpb_' . $css_animation;
	}

	return $output;
}

function volunteer_vc_add_extra_attr() {

	/**
	 * Add background style to VC Row
	 */
	 
	vc_add_param( 'vc_row', array(
		'type'			=> 'dropdown',
		'heading'		=> esc_html__( 'Background Style', 'volunteer' ),
		'param_name'	=> 'background_style',
		'value'			=> array(
			esc_html__( 'Standard Settings', 'volunteer' )				=> 'tpath-standard',
			esc_html__( 'Primary Background Color', 'volunteer' )		=> 'primary-color',
			esc_html__( 'Primary Dark Background Color', 'volunteer' )	=> 'primary-dark-color',
			esc_html__( 'Background Overlay', 'volunteer' )				=> 'overlay-wrapper',
			esc_html__( 'Dark Background', 'volunteer' )				=> 'dark-wrapper',
			esc_html__( 'Grey Background', 'volunteer' )				=> 'grey-wrapper',
			esc_html__( 'White Background', 'volunteer' )				=> 'white-wrapper',
		),
	) );
	
	vc_add_param( 'vc_row', array(
		'type'			=> 'dropdown',
		'heading'		=> esc_html__( 'Overlay Color', 'volunteer' ),
		'param_name'	=> 'bg_overlay_style',
		'value'			=> array(
			esc_html__( 'Theme Color', 'volunteer' )			=> 'theme-overlay-color',
			esc_html__( 'Theme Dark Color', 'volunteer' )		=> 'theme-dark-overlay-color',
			esc_html__( 'Dark Color', 'volunteer' )				=> 'dark-overlay-color',
			esc_html__( 'Black Color', 'volunteer' )			=> 'black-overlay-color',
			esc_html__( 'White Color', 'volunteer' )			=> 'white-overlay-color',
		),
		'dependency'	=> array(
			'element'	=> 'background_style',
			'value'		=> array( 'overlay-wrapper' ),
		),
	) );
	
	vc_add_param( 'vc_row', array(
		'type'			=> 'dropdown',
		'heading'		=> esc_html__( 'Overlay Opacity', 'volunteer' ),
		'param_name'	=> 'bg_overlay_opacity',
		'value'			=> array(
			esc_html__( 'Light Opacity', 'volunteer' )		=> 'light-opacity',
			esc_html__( 'Dark Opacity', 'volunteer' )		=> 'dark-opacity',
		),
		'dependency'	=> array(
			'element'	=> 'background_style',
			'value'		=> array( 'overlay-wrapper' ),
		),
	) );
	
	vc_add_param( 'vc_row', array(
		'type'			=> 'dropdown',
		'heading'		=> esc_html__( 'Center Row Content', 'volunteer' ),
		'param_name'	=> 'center_row',
		'value'			=> array(
			esc_html__( 'Yes', 'volunteer' )	=> 'yes',
			esc_html__( 'No', 'volunteer' )		=> 'no',
		),
		'description'	=> esc_html__( 'Use this option to add container and center the inner content. Useful when using full-width pages.', 'volunteer' ),
	) );
	
	vc_add_param( 'vc_row', array(
		'type'			=> 'dropdown',
		'heading'		=> esc_html__( 'Equal Height', 'volunteer' ),
		'param_name'	=> 'match_height',
		'value'			=> array(
			esc_html__( 'No', 'volunteer' )		=> 'no',
			esc_html__( 'Yes', 'volunteer' )	=> 'yes',
		),
		'description'	=> esc_html__( 'Use this option to make all column in equal height.', 'volunteer' ),
	) );
	
	vc_add_param( 'vc_row', vc_map_add_css_animation( $label = false ) );
	
	vc_remove_param( 'vc_row', 'equal_height' );
	
	/**
	 * Add options to VC Column
	 */
	
	vc_add_param( 'vc_column', array(
		'type'			=> 'dropdown',
		'heading'		=> esc_html__( 'Background Style', 'volunteer' ),
		'param_name'	=> 'column_bg',
		'value'			=> array(
			esc_html__( 'Default', 'volunteer' )						=> 'column-bg-default',
			esc_html__( 'Primary Background Color', 'volunteer' )		=> 'primary-color',
			esc_html__( 'Primary Dark Background Color', 'volunteer' )	=> 'primary-dark-color',
			esc_html__( 'Background Overlay', 'volunteer' )				=> 'overlay-wrapper',
			esc_html__( 'Dark Background', 'volunteer' )				=> 'dark-wrapper',
			esc_html__( 'Grey Background', 'volunteer' )				=> 'grey-wrapper',
			esc_html__( 'White Background', 'volunteer' )				=> 'white-wrapper',
		),
	) );
	
	vc_add_param( 'vc_column', array(
		'type'			=> 'dropdown',
		'heading'		=> esc_html__( 'Overlay Color', 'volunteer' ),
		'param_name'	=> 'bg_overlay_style',
		'value'			=> array(
			esc_html__( 'Theme Color', 'volunteer' )			=> 'theme-overlay-color',
			esc_html__( 'Theme Dark Color', 'volunteer' )		=> 'theme-dark-overlay-color',
			esc_html__( 'Dark Color', 'volunteer' )				=> 'dark-overlay-color',
			esc_html__( 'Black Color', 'volunteer' )			=> 'black-overlay-color',
			esc_html__( 'White Color', 'volunteer' )			=> 'white-overlay-color',
		),
		'dependency'	=> array(
			'element'	=> 'column_bg',
			'value'		=> array( 'overlay-wrapper' ),
		),
	) );
	
	vc_add_param( 'vc_column', array(
		'type'			=> 'dropdown',
		'heading'		=> esc_html__( 'Overlay Opacity', 'volunteer' ),
		'param_name'	=> 'bg_overlay_opacity',
		'value'			=> array(
			esc_html__( 'Light Opacity', 'volunteer' )		=> 'light-opacity',
			esc_html__( 'Dark Opacity', 'volunteer' )		=> 'dark-opacity',
		),
		'dependency'	=> array(
			'element'	=> 'column_bg',
			'value'		=> array( 'overlay-wrapper' ),
		),
	) );
		
	vc_add_param( 'vc_column', array(
		'type'			=> 'dropdown',
		'heading'		=> esc_html__( 'Typography Style', 'volunteer' ),
		'param_name'	=> 'typo_style',
		'value'			=> array(
			esc_html__( 'Default', 'volunteer' )			=> 'default',
			esc_html__( 'Dark Color', 'volunteer' )			=> 'dark',
			esc_html__( 'Grey Color', 'volunteer' )			=> 'grey',
			esc_html__( 'White Color', 'volunteer' )		=> 'white',
		),
	) );
	
	vc_add_param( 'vc_column', vc_map_add_css_animation( $label = false) );
		
	/**
	 * Section
	 */
	
	vc_remove_param( 'vc_tta_section', 'el_class' );

	vc_add_param( 'vc_tta_section', array(
		"type" 			=> "dropdown",
		"heading" 		=> esc_html__( "Icon library", "volunteer" ),
		"value" 		=> array(
			esc_html__( "Font Awesome", "volunteer" ) 	=> "fontawesome",
			esc_html__( 'Open Iconic', 'volunteer' ) 	=> 'openiconic',
			esc_html__( 'Typicons', 'volunteer' ) 		=> 'typicons',
			esc_html__( 'Entypo', 'volunteer' ) 		=> 'entypo',
			esc_html__( "Lineicons", "volunteer" ) 		=> "lineicons",
			esc_html__( "Flaticons", "volunteer" ) 		=> "flaticons",
		),
		"admin_label" 	=> true,
		"param_name" 	=> "i_type",
		"dependency" 	=> array(
							"element" 	=> "add_icon",
							"value" 	=> "true",
						),
		"description" 	=> esc_html__( "Select icon library.", "volunteer" ),
	) );
	
	vc_add_param( 'vc_tta_section', array(
		"type" 			=> 'iconpicker',
		"heading" 		=> esc_html__( "Icon", "volunteer" ),
		"param_name" 	=> "i_icon_lineicons",
		"value" 		=> "",
		"settings" 		=> array(
			"emptyIcon" 	=> true,
			"type" 			=> 'simpleicons',
			"iconsPerPage" 	=> 4000,
		),
		"dependency" 	=> array(
			"element" 	=> "i_type",
			"value" 	=> "lineicons",
		),
		"description" 	=> esc_html__( "Select icon from library.", "volunteer" ),
	) );
	
	vc_add_param( 'vc_tta_section', array(
		"type" 			=> 'iconpicker',
		"heading" 		=> esc_html__( "Icon", "volunteer" ),
		"param_name" 	=> "i_icon_flaticons",
		"value" 		=> "",
		"settings" 		=> array(
			"emptyIcon" 	=> true,
			"type" 			=> 'flaticons',
			"iconsPerPage" 	=> 4000,
		),
		"dependency" 	=> array(
			"element" 	=> "i_type",
			"value" 	=> "flaticons",
		),
		"description" 	=> esc_html__( "Select icon from library.", "volunteer" ),
	) );
	
	vc_add_param( 'vc_tta_section', array(
		'type' 			=> 'textfield',
		'heading' 		=> esc_html__( 'Extra class name', 'volunteer' ),
		'param_name' 	=> 'el_class',
		'description' 	=> esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'volunteer' )
	) );

	/**
	 * Add Style to VC Tour
	 */
	vc_add_param( 'vc_tta_tour', array(
			'type' 			=> 'dropdown',
			'param_name' 	=> 'style',
			'value' 		=> array(
				esc_html__( 'Custom Theme Style', 'volunteer' )		 => 'tpath_tour_design',
				esc_html__( 'Classic', 'volunteer' )				 => 'classic',
				esc_html__( 'Modern', 'volunteer' )					 => 'modern',
				esc_html__( 'Flat', 'volunteer' )					 => 'flat',
				esc_html__( 'Outline', 'volunteer' )				 => 'outline',
			),
			'heading' 		=> esc_html__( 'Style', 'volunteer' ),
			'description' 	=> esc_html__( 'Select tour display style.', 'volunteer' ),
	) );
	
	/**
	 * Button
	 */
	 vc_add_param( 'vc_btn', array(
		"type" 			=> "dropdown",
		'heading' 		=> esc_html__( 'Style', 'volunteer' ),
		'description' 	=> esc_html__( 'Select button display style.', 'volunteer' ),
		'value' 		=> array(
			esc_html__( 'Default Border', 'volunteer' ) 	=> 'border',
			esc_html__( 'Background', 'volunteer' ) 		=> 'background',
			esc_html__( 'Modern', 'volunteer' ) 			=> 'modern',
			esc_html__( 'Classic', 'volunteer' ) 			=> 'classic',
			esc_html__( 'Flat', 'volunteer' ) 				=> 'flat',
			esc_html__( 'Outline', 'volunteer' ) 			=> 'outline',
			esc_html__( '3d', 'volunteer' ) 				=> '3d',
			esc_html__( 'Outline custom', 'volunteer' ) 	=> 'outline-custom',
		),
		"param_name" 	=> "style",
	) );
	
	vc_add_param( 'vc_btn', array(
		'type' 					=> 'dropdown',
		'heading' 				=> esc_html__( 'Color', 'volunteer' ),
		'param_name' 			=> 'color',
		'description' 			=> esc_html__( 'Select button color.', 'volunteer' ),
		// compatible with btn2, need to be converted from btn1
		'param_holder_class' 	=> 'vc_colored-dropdown vc_btn3-colored-dropdown',
		'value' 				=> array(
					// Theme Colors
					esc_html__( 'Theme Light', 'volunteer' ) 	=> 'theme_light',
					esc_html__( 'Theme Dark', 'volunteer' ) 	=> 'theme_dark',
					esc_html__( 'White', 'volunteer' ) 			=> 'white',
					esc_html__( 'Black', 'volunteer' ) 			=> 'black',
				   // Btn1 Colors
				   esc_html__( 'Classic Grey', 'volunteer' ) 		=> 'default',
				   esc_html__( 'Classic Blue', 'volunteer' ) 		=> 'primary',
				   esc_html__( 'Classic Turquoise', 'volunteer' ) 	=> 'info',
				   esc_html__( 'Classic Green', 'volunteer' ) 		=> 'success',
				   esc_html__( 'Classic Orange', 'volunteer' )		=> 'warning',
				   esc_html__( 'Classic Red', 'volunteer' ) 		=> 'danger',
				   esc_html__( 'Classic Black', 'volunteer' ) 		=> "inverse"
				   // + Btn2 Colors (default color set)
			   ) + getVcShared( 'colors-dashed' ),
		'std' 					=> 'primary-bg',
		// must have default color grey
		'dependency' => array(
			'element' => 'style',
			'value_not_equal_to' => array( 'custom', 'outline-custom' )
		),
	) );
		
	/**
	 * Call To Action
	 */
	
	vc_add_param( 'vc_cta', array(
		'type' 			=> 'dropdown',
		'heading' 		=> esc_html__( 'Style', 'volunteer' ),
		'param_name' 	=> 'style',
		'value' 		=> array(
			esc_html__( 'Default', 'volunteer' ) 	=> 'default',
			esc_html__( 'Classic', 'volunteer' ) 	=> 'classic',
			esc_html__( 'Flat', 'volunteer' ) 		=> 'flat',
			esc_html__( 'Outline', 'volunteer' ) 	=> 'outline',
			esc_html__( '3d', 'volunteer' ) 		=> '3d',
			esc_html__( 'Custom', 'volunteer' ) 	=> 'custom',
		),
		'std' 			=> 'default',
		'description' 	=> esc_html__( 'Select call to action display style.', 'volunteer' ),
	) );
	
	vc_add_param( 'vc_cta', array(
		"type" 			=> "dropdown",
		'heading' 		=> esc_html__( 'Style', 'volunteer' ),
		'description' 	=> esc_html__( 'Select button display style.', 'volunteer' ),
		'value' 		=> array(
			esc_html__( 'Default', 'volunteer' ) 		=> 'default',
			esc_html__( 'Transparent', 'volunteer' ) 	=> 'transparent',
			esc_html__( 'Modern', 'volunteer' ) 		=> 'modern',
			esc_html__( 'Classic', 'volunteer' ) 		=> 'classic',
			esc_html__( 'Flat', 'volunteer' ) 			=> 'flat',
			esc_html__( 'Outline', 'volunteer' ) 		=> 'outline',
			esc_html__( '3d', 'volunteer' ) 			=> '3d',
			esc_html__( 'Custom', 'volunteer' ) 		=> 'custom',
			esc_html__( 'Outline custom', 'volunteer' ) => 'outline-custom',
		),
		'dependency' 			=> array(
			'element' 		=> 'add_button',
			'not_empty' 	=> true,
		),
		"integrated_shortcode" 			=> "vc_btn",
		"integrated_shortcode_field" 	=> "btn_",
		"param_name" 					=> "btn_style",
		"group"							=> esc_html__( 'Button', 'volunteer' ),
	) );
	
	/**
	 * Portfolio Categories
	 */
	if( VOLUNTEER_PORTFOLIO_ACTIVE ) {
		$portfolio_args = array(
			'post_type' 		=> 'tpath_portfolio',
			'orderby' 			=> 'name',
			'hide_empty' 		=> 0,
			'hierarchical' 		=> 1,
			'taxonomy' 			=> 'portfolio_categories'
		);
		
		$portfolio_cats = get_categories( $portfolio_args );
		$portfolio_cats_list = array( 'Show All Categories' => 'all' );
		
		foreach( $portfolio_cats as $cat ) {
			$portfolio_cats_list[$cat->name] = $cat->term_id;
		}
		
		$attributes = array(
						"type" 			=> "dropdown",
						"heading" 		=> esc_html__("Choose Portfolio Category", "volunteer"),
						"param_name" 	=> "categories",
						"value" 		=> $portfolio_cats_list,
						"description" 	=> ''
					);
		
		vc_add_param('volunteer_vc_portfolio', $attributes);
	}
	
	/**
	 * Team Categories
	 */
	if( VOLUNTEER_TEAM_ACTIVE ) {
		$team_args = array(
			'post_type' 		=> 'tpath_team_member',
			'orderby' 			=> 'name',
			'hide_empty' 		=> 0,
			'hierarchical' 		=> 1,
			'taxonomy' 			=> 'team_member_categories'
		);
		
		$team_cats = get_categories( $team_args );
		$team_cats_list = array( 'Show All Categories' => 'all' );
		
		foreach( $team_cats as $team_cat ){
			$team_cats_list[$team_cat->name] = $team_cat->term_id;
		}
		
		$attributes = array(
						"type" 			=> "dropdown",
						"heading" 		=> esc_html__("Choose Team Category", "volunteer"),
						"param_name" 	=> "categories",
						"value" 		=> $team_cats_list,
						"description" 	=> ''
					);
		
		vc_add_param('volunteer_vc_team', $attributes);
	}
	
	/**
	 * Testimonial Categories
	 */
	if( VOLUNTEER_TESTIMONIAL_ACTIVE ) {
		$testimonial_args = array(
			'post_type' 		=> 'tpath_testimonial',
			'orderby' 			=> 'name',
			'hide_empty' 		=> 0,
			'hierarchical' 		=> 1,
			'taxonomy' 			=> 'testimonial_categories'
		);
		
		$testimonial_cats = get_categories( $testimonial_args );
		$testimonial_cats_list = array( 'Show All Categories' => 'all' );
		
		foreach( $testimonial_cats as $testimonial_cat ){
			$testimonial_cats_list[$testimonial_cat->name] = $testimonial_cat->term_id;
		}
		
		$attributes = array(
						"type" 			=> "dropdown",
						"heading" 		=> esc_html__("Choose Testimonial Category", "volunteer"),
						"param_name" 	=> "categories",
						"value" 		=> $testimonial_cats_list,
						"description" 	=> ''
					);
		
		vc_add_param('volunteer_vc_testimonial', $attributes);
	}
	
	/**
	 * Client Categories
	 */
	if( VOLUNTEER_CLIENTS_ACTIVE ) {
		$client_args = array(
			'post_type' 		=> 'tpath_clients',
			'orderby' 			=> 'name',
			'hide_empty' 		=> 0,
			'hierarchical' 		=> 1,
			'taxonomy' 			=> 'client_categories'
		);
		
		$client_cats = get_categories( $client_args );
		$client_cats_list = array( 'Show All Categories' => 'all' );
		
		foreach( $client_cats as $client_cat ){
			$client_cats_list[$client_cat->name] = $client_cat->term_id;
		}
		
		$attributes = array(
						"type" 			=> "dropdown",
						"heading" 		=> esc_html__("Choose Client Category", "volunteer"),
						"param_name" 	=> "categories",
						"value" 		=> $client_cats_list,
						"description" 	=> ''
					);
		
		vc_add_param('volunteer_vc_clients', $attributes);
	}
	
}
add_action('init', 'volunteer_vc_add_extra_attr', 999);

/* =============================================================
 *	Add Custom Flaticons to VC
 * ============================================================= */
if( ! function_exists('volunteer_vc_custom_flaticons') ) {
	function volunteer_vc_custom_flaticons( $icons ) {
		
		$pattern = '/\.(flaticon-(?:\w+(?:-)?)+):before\s+{\s*content:\s*"(.+)";\s+}/';
		$flaticons_path = VOLUNTEER_THEME_URL . '/css/flaticon.css';
		
		$response = wp_remote_get( $flaticons_path );
		if( is_array($response) ) {
			$subject = $response['body']; // use the content
		}
		
		preg_match_all($pattern, $subject, $matches, PREG_SET_ORDER);
		
		$all_flaticons = array();
		$all_new_flaticons = array();
		
		foreach($matches as $match){
			$all_flaticons['flaticon ' . $match[1]] = $match[1];
		}
		
		foreach($all_flaticons as $key => $value ){
			$all_new_flaticons[] = array( $key => $value );
		}
	
		return array_merge( $icons, $all_new_flaticons );
	
	}
}
add_filter('vc_iconpicker-type-flaticons', 'volunteer_vc_custom_flaticons', 10, 1);

/* =============================================================
 *	Add Simple Line Icons to VC
 * ============================================================= */
if( ! function_exists('volunteer_vc_custom_simpleicons') ) {
	function volunteer_vc_custom_simpleicons( $icons ) {
	
		$pattern = '/\.(icon-(?:\w+(?:-)?)+):before\s+{\s*content:\s*"(.+)";\s+}/';
		$simple_icons_path = VOLUNTEER_THEME_URL . '/css/simple-line-icons.css';
		
		$response = wp_remote_get( $simple_icons_path );
		if( is_array($response) ) {
			$subject = $response['body']; // use the content
		}
		
		preg_match_all($pattern, $subject, $matches, PREG_SET_ORDER);
		
		$all_simple_icons = array();
		$simple_icons = array();
		
		foreach($matches as $match){
			$all_simple_icons['simple-icon ' . $match[1]] = $match[1];
		}
		
		foreach($all_simple_icons as $key => $value ){
			$simple_icons[] = array( $key => $value );
		}
	
		return array_merge( $icons, $simple_icons );
	
	}
}
add_filter('vc_iconpicker-type-simpleicons', 'volunteer_vc_custom_simpleicons', 10, 1);