<?php
/**
 * Register widgetized areas
 */
if ( ! function_exists( 'volunteer_widgets_init' ) ) {
	function volunteer_widgets_init() {
	
	global $volunteer_options;
	
	// Primary Sidebar
	    
	register_sidebar( array(
		'name'          => esc_html__( 'Main Sidebar', 'volunteer' ),
		'id'            => 'primary',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
		'description' 	=> esc_html__( 'The default sidebar used in two or three-column layouts.', 'volunteer' ),
	) );
		
	// Header Right Sidebar
		
	register_sidebar( array(
		'name'          => esc_html__( 'Header Right Sidebar', 'volunteer' ),
		'id'            => 'header-right',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
		'description' 	=> esc_html__( 'A header right sidebar used in header only if enabled in theme options.', 'volunteer' ),
	) );
	
	// Footer Widgets Sidebar
	$is_footer_widgets = '';
	$is_footer_widgets = isset($volunteer_options['footer_widgets_enable']) ? $volunteer_options['footer_widgets_enable'] : 0;
	
	if( $is_footer_widgets ) {
	
		$columns = $volunteer_options['footer_widget_layout'];
		if ( ! $columns ) $columns = 4;
		for ( $i = 1; $i <= intval( $columns ); $i++ ) {
		
			register_sidebar( array(
				'name'          => sprintf( esc_html__( 'Footer %d', 'volunteer' ), $i ),
				'id'            => sprintf( 'footer-widget-%d', $i ),
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3 class="widget-title">',
				'after_title'   => '</h3>',
				'description'	=> sprintf( esc_html__( 'Footer widget Area %d.', 'volunteer' ), $i ),
			) );
				
		}
		
	}
	
	/**
	 * Woocommerce Sidebar
	 */
	if( class_exists('Woocommerce') ) {
		register_sidebar( array(
			'name'          => esc_html__( 'Shop Sidebar', 'volunteer' ),
			'id'            => 'shop-sidebar',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
			'description' 	=> esc_html__( 'Shop Sidebar to show your widgets on Shop Pages.', 'volunteer' ),
		) );
	}
		
	} // End volunteer_widgets_init()
}

add_action( 'widgets_init', 'volunteer_widgets_init' );  
?>