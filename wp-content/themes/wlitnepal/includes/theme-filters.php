<?php
/**
* Filters and Actions
*/

// Theme Setup
add_action( 'after_setup_theme', 'volunteer_setup' );
// Admin Theme Activation Hook
add_action( 'admin_init', 'volunteer_theme_activation' );
// Add layout extra classes to body_class output
add_filter( 'body_class', 'volunteer_layout_body_class', 10 );
// Add custom meta tags to the <head>
add_action( 'wp_head', 'volunteer_meta_tags', 10 );
// Custom Css from Theme Option
add_action( 'wp_head', 'volunteer_enqueue_custom_styling' );
// Add Custom Visual Composer CSS for Block
add_action( 'wp_head', 'volunteer_vc_addfrontcss', 1000 );
// Load Theme Stylesheet and Jquery only on Frontend
if ( ! is_admin() ) {
	add_action( 'wp_enqueue_scripts', 'volunteer_load_theme_scripts', 20 );
}
// Store Ajax URL
add_action( 'wp_head', 'volunteer_ajaxurl' );
// Dynamic JS from Theme Option
add_action( 'wp_head', 'volunteer_enqueue_dynamic_js' );
// Custom Excerpt Length and Custom More Excerpt
add_filter( 'excerpt_length', 'volunteer_custom_excerpt_length', 999 );
add_filter( 'excerpt_more', 'volunteer_custom_excerpt_more' );

/* ======================================
 * Theme Setup
 * ====================================== */
 
/* Set the content width based on the theme's design and stylesheet. */
if ( ! isset( $content_width ) )
	$content_width = 640; /* pixels */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */

if ( ! function_exists( 'volunteer_setup' ) ) {
	function volunteer_setup () {
	
		// load textdomain
    	load_theme_textdomain('volunteer', get_template_directory() . '/languages');
		
		// This theme styles the visual editor to resemble the theme style.
		add_editor_style( 'css/editor-style.css' );
			
		// Enable support for Post Thumbnails
		add_theme_support( 'post-thumbnails' );
		add_image_size('volunteer-blog-large', 1000, 377, true);
		add_image_size('volunteer-blog-list', 580, 345, true);
		add_image_size('volunteer-team', 450, 400, true);
		add_image_size('volunteer-theme-mid', 550, 375, true);
		add_image_size('volunteer-theme-large', 710, 445, true);
		
		// Theme Support for Title Tag
		add_theme_support( 'title-tag' );
	
		// Add default posts and comments RSS feed links to head
		add_theme_support( 'automatic-feed-links' );
		
		// Add Woocommerce Support
		add_theme_support( 'woocommerce' );
		
		// Register Nav Menus
		if ( function_exists('wp_nav_menu') ) {
			add_theme_support( 'nav-menus' );
			register_nav_menus( array(
				'top-menu' 			=> esc_html__( 'Top Menu' , 'volunteer' ),
				'primary-menu' 		=> esc_html__( 'Primary Menu' , 'volunteer' ),
			) );
		}
		
		/*
		 * Switches default core markup for galleries to output valid HTML5.
		 */
		add_theme_support( 'html5', array( 'gallery', 'caption'	) );
		
		// Add Posts Format Support
		add_theme_support( 'post-formats', array( 'gallery', 'link', 'image', 'quote', 'video', 'audio' ) );
		
		// This theme uses its own gallery styles.
		add_filter( 'use_default_gallery_style', '__return_false' );
		
	} // End volunteer_setup()
}

/* ======================================
 * Admin Theme Activation Hook
 * ====================================== */
if ( ! function_exists( 'volunteer_theme_activation' ) ) {
	function volunteer_theme_activation() {
	
		global $pagenow;
		
		if( is_admin() && 'themes.php' == $pagenow && isset( $_GET['activated'] ) ) {
			update_option( 'shop_catalog_image_size', array( 'width' => 550, 'height' => 375, 1 ) );
		}
		
	}
}

/* ======================================
 * Add layout to body_class output
 * ====================================== */
if ( ! function_exists( 'volunteer_layout_body_class' ) ) {

	function volunteer_layout_body_class( $classes ) {
	
		global $post, $wp_query, $volunteer_options;

		$layout = $blog_type = $theme_class = $footer_layout = $logo_align = '';
		
		// Set Column Layout from singular posts
		if( is_singular() ) {
			$layout = get_post_meta( $post->ID, 'volunteer_layout', true );			
		}
		
		if( class_exists('Woocommerce') ) {
			if( is_shop() ) {
				$shop_page_id = get_option('woocommerce_shop_page_id');
				if( ! empty( $shop_page_id ) ) {
					$layout = get_post_meta( $shop_page_id, 'volunteer_layout', true );
				}
			} 
			
			else if( is_product_category() || is_product_tag() ) {
				$layout = 'two-col-right';				
			}
		}
		
		else if( is_archive() ) {
			$layout = $volunteer_options['blog_archive_layout'];
			$blog_type = 'blog-' . $volunteer_options['archive_blog_type'];
		}
		
		if( is_home() ) {
			$home_id = get_option( 'page_for_posts' );
			$layout = get_post_meta( $home_id, 'volunteer_layout', true );
			if( !$layout ) {
				$layout = $volunteer_options['blog_layout'];
			}
			$blog_type = 'blog-' . $volunteer_options['blog_type'];
		}
		
		if( class_exists('Woocommerce') ) {
			if( is_product() ) {				
				$layout = 'two-col-right';				
			}
			if( is_singular('tpath_portfolio') ) {
				$layout = $volunteer_options['layout'];
			}
			if( is_singular() && is_single() ) {
				if( is_singular( 'post' ) ) {
					$layout = get_post_meta( $post->ID, 'volunteer_layout', true );
					if( !$layout ) {
						$layout = $volunteer_options['single_post_layout'];
					}
				} else if( !$layout ) {
					$layout = get_post_meta( $post->ID, 'volunteer_layout', true );
				}			
			}
		}
		else if( is_singular('tpath_portfolio') ) {
			$layout = $volunteer_options['layout'];
		}
		else if( is_singular() && is_single() ) {
			if( is_singular( 'post' ) ) {
				$layout = get_post_meta( $post->ID, 'volunteer_layout', true );
				if( !$layout ) {
					$layout = $volunteer_options['single_post_layout'];
				}
			} else if( !$layout ) {
				$layout = get_post_meta( $post->ID, 'volunteer_layout', true );
			}			
		}
				
		// If Singular posts value empty set theme option value		
		if( !$layout ) {			
			if( $volunteer_options['layout'] != '' ) {		
				$layout = $volunteer_options['layout'];
			}
			else {
				$layout = 'one-col';
			}
		}
				
		// Theme Layout
		if( is_singular() ) {
			$theme_class = get_post_meta( $post->ID, 'volunteer_theme_layout', true );			
		}
		
		if( $theme_class == '' || $theme_class == 'default' ) {		
			if( $volunteer_options['theme_layout'] != '' ) {
				$theme_class = $volunteer_options['theme_layout'];
			} else {
				$theme_class = 'boxed';
			}
		}
		
		$classes[] = $theme_class;
		
		// Sticky Class
		if( isset( $volunteer_options['sticky_header'] ) && $volunteer_options['sticky_header'] == 1 ) {
			$classes[] = 'header-sticky-enabled';
		}
		
		// Blog Type
		$classes[] = $blog_type;
								
		// Add classes to body_class() output 
		$classes[] = $layout;
		return $classes;
		
	} // End volunteer_layout_body_class()
	
}

/* ======================================
 * Print custom meta tags
 * ====================================== */
if ( ! function_exists( 'volunteer_meta_tags' ) ) {

	function volunteer_meta_tags() {
	
		global $volunteer_options;
		
		if( ! ( function_exists( 'has_site_icon' ) && has_site_icon() ) ) {
				
			if( isset($volunteer_options['enable_responsive']) ) {
				echo '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />' . "\n";
			}
					
			if( isset( $volunteer_options['favicon'] ) && $volunteer_options['favicon'] != '' ) {
				echo '<link rel="shortcut icon" href="'. esc_url( $volunteer_options['favicon'] ) .'" type="image/x-icon" />' . "\n";
			}
			
			// iPhone Icon
			if( isset( $volunteer_options['apple_iphone_icon'] ) && $volunteer_options['apple_iphone_icon'] != '' ) {
				echo '<link rel="apple-touch-icon" href="'. esc_url( $volunteer_options['apple_iphone_icon'] ) .'">' . "\n";
			}
			// iPhone Retina Display Icon
			if( isset( $volunteer_options['apple_iphone_retina_icon'] ) && $volunteer_options['apple_iphone_retina_icon'] != '') {
				echo '<link rel="apple-touch-icon" sizes="114x114" href="'. esc_url( $volunteer_options['apple_iphone_retina_icon'] ) .'">' . "\n";
			}
			// iPad Icon	
			if( isset( $volunteer_options['apple_iphone_retina_icon'] ) && $volunteer_options['apple_ipad_icon'] != '' ) {
				echo '<link rel="apple-touch-icon" sizes="72x72" href="'. esc_url( $volunteer_options['apple_ipad_icon'] ) .'">' . "\n";
			}
			// iPad Retina Display Icon
			if( isset( $volunteer_options['apple_iphone_retina_icon'] ) && $volunteer_options['apple_ipad_retina_icon'] != '' ) {
				echo '<link rel="apple-touch-icon" sizes="144x144" href="'. esc_url( $volunteer_options['apple_ipad_retina_icon'] ) .'">' . "\n";
			}
			
		}
		
	} // End volunteer_meta_tags()
	
}

/* ======================================
 * Enqueue Custom Styling
 * ====================================== */

if ( ! function_exists( 'volunteer_enqueue_custom_styling' ) ) {

	function volunteer_enqueue_custom_styling () {
		echo '<!-- Custom CSS -->'. "\n";
		echo '<style type="text/css">' . "\n";
		echo volunteer_custom_styles();
		echo '</style>' . "\n";
	} // End volunteer_enqueue_custom_styling()
	
}

/* ==============================================
 * Visual Composer Front CSS for Block Post Type
 * ============================================== */

function volunteer_vc_addfrontcss( $id = null ) {

	global $post;

	if ( ! is_singular() ) {
		return;
	}
	if ( ! $id ) {
		$id = get_the_ID();
	}

	if ( $id ) {
		$additional_block_id = get_post_meta( $id, 'volunteer_additional_content_block', true );
		
		if( isset( $additional_block_id ) && ( $additional_block_id != '' && $additional_block_id != 0 ) ) {
			
			$args = array(
				'post_type'  		=> 'tpath_block',
				'post__in'   		=> array( $additional_block_id )
		    );
			
			$block_query = new WP_Query($args);
			
			if( $block_query->have_posts() ) { 
				while ($block_query->have_posts()) {
					$block_query->the_post();
					
					$shortcodes_custom_css = get_post_meta( $post->ID, '_wpb_shortcodes_custom_css', true );
					if ( ! empty( $shortcodes_custom_css ) ) {
						echo '<style type="text/css" data-type="vc_shortcodes-custom-css">' . $shortcodes_custom_css . '</style>';
					}
				}
			}
			
			wp_reset_postdata();
			
		}
	}
}

/* ======================================
 * Add theme custom styles 
 * ====================================== */
if ( ! function_exists( 'volunteer_custom_styles' ) ) {

	function volunteer_custom_styles() {
		global $volunteer_options, $post;

		$output = '';
		
		// Custom CSS Code
		if( isset( $volunteer_options['custom_css'] ) && $volunteer_options['custom_css'] != '' ) {
			$output .= $volunteer_options['custom_css'] . "\n";
		}
		
		// Custom Color Scheme Styles
		$color_scheme_1 = '';
		if( isset( $volunteer_options['custom_scheme_color'] ) && $volunteer_options['custom_scheme_color'] != '' ) {
			
			$color_scheme_1 = $volunteer_options['custom_scheme_color'];
		
			$output .= 'a { color: '. $color_scheme_1 .'; }';
			$output .= '.navbar-brand .site-logo-text, .navbar-brand .site-logo-text:hover { color: '. $color_scheme_1 .'; }';
			$output .= '#back-to-top { background-color: '. $color_scheme_1 .'; }';
			$output .= '.header-top-section a:hover { color: '. $color_scheme_1 .'; }';
			$output .= '.page-breadcrumbs ul li a:hover, .page-breadcrumbs ul li a:active, .page-breadcrumbs ul li a:focus, .woocommerce .woocommerce-breadcrumb a:hover, .woocommerce .woocommerce-breadcrumb a:active, .woocommerce .woocommerce-breadcrumb a:focus, .page-breadcrumbs ul li a, .page-breadcrumbs span > a, .woocommerce .woocommerce-breadcrumb a { color: '. $color_scheme_1 .'; }';
			
			$output .= '.vc_general.vc_btn3.vc_btn3-style-border.vc_btn3-color-theme_light { border-color: '. $color_scheme_1 .'; color: '. $color_scheme_1 .'; }';
			$output .= '.vc_general.vc_btn3.vc_btn3-style-border.vc_btn3-color-theme_light:hover { background: '. $color_scheme_1 .'; }';
			$output .= '.vc_general.vc_btn3.vc_btn3-style-background.vc_btn3-color-theme_light { background: '. $color_scheme_1 .'; border-color: '. $color_scheme_1 .'; }';
			$output .= '.vc_general.vc_btn3.vc_btn3-style-background.vc_btn3-color-theme_light:hover { color: '. $color_scheme_1 .'; }';
			$output .= '.vc_general.vc_btn3.vc_btn3-style-background.vc_btn3-color-white:hover { background: '. $color_scheme_1 .'; border-color: '. $color_scheme_1 .'; }';
			$output .= '.vc_general.vc_btn3.vc_btn3-style-background:hover { background: '. $color_scheme_1 .'; border-color: '. $color_scheme_1 .'; }';
			
			$output .= '.header-top-section .tpath-top-nav .navbar-nav > li > a:hover, .header-top-section .tpath-top-nav .navbar-nav > li > a:active, .header-top-section .tpath-top-nav .navbar-nav > li > a:focus { color: '. $color_scheme_1 .'; }';
			$output .= '.header-section .header-main-section .nav.navbar-nav.tpath-main-nav a:hover, .header-section .header-main-section .nav.navbar-nav.tpath-main-nav a:focus, .header-section .header-main-section .nav.navbar-nav.tpath-main-nav a:active, .header-section .header-main-section .nav.navbar-nav.tpath-main-nav > li:hover > a, .header-section .header-main-section .nav.navbar-nav.tpath-main-nav li.active > a, .header-section .nav.navbar-nav .side-menu a:hover, .header-section .nav.navbar-nav .side-menu a:active, .header-section .nav.navbar-nav .side-menu a:focus, .link-url:hover, .link-url:hover a, .link-url a:hover, .header-section .nav.navbar-nav.tpath-main-nav li .tpath-megamenu-container .tpath-megamenu li.active > a, .header-section .nav.navbar-nav.tpath-main-nav li .tpath-megamenu-container .tpath-megamenu a:hover, .header-section .nav.navbar-nav.tpath-main-nav li .tpath-megamenu-container .tpath-megamenu a:active, .header-section .nav.navbar-nav.tpath-main-nav li .tpath-megamenu-container .tpath-megamenu a:focus, .header-main-section .nav.navbar-nav ul.dropdown-menu li.current-menu-parent a, .header-main-section .nav.navbar-nav li.current-menu-parent a, .header-main-section .nav.navbar-nav.tpath-main-nav li.current-menu-ancestor > a { color: '. $color_scheme_1 .'; }';
			
			$output .= '.dropdown-menu, .dropdown-menu .sub-menu { border-color: '. $color_scheme_1 .'; }';
			$output .= '.navbar-toggle, .flex-direction-nav a {	background-color: '. $color_scheme_1 .'; }';
			$output .= '.tpath-section-title.text-center::before { background-color: '. $color_scheme_1 .'; }';
			$output .= '.header-top-section a:hover, .header-top-section a:focus, .header-top-section a:active { color: '. $color_scheme_1 .'; }';
			$output .= '.header-main-section ul li.header-main-cart a:hover, .header-main-section ul li.header-main-cart a:focus, .header-main-section ul li.header-main-cart a:active { color: '. $color_scheme_1 .'; }';
			
			$output .= '.feature-icon.iconfb-bordered.iconfb-skin-default.icon-shape, .feature-icon.iconfb-none.iconfb-skin-default, .feature-icon.iconfb-bg.iconfb-skin-dark.icon-shape { color: '. $color_scheme_1 .'; }';
			$output .= '.feature-icon.iconfb-bordered.iconfb-skin-default.icon-shape { border-color: '. $color_scheme_1 .'; }';
			$output .= '.feature-icon.iconfb-bg.iconfb-skin-default.icon-shape { background-color: '. $color_scheme_1 .'; color: #fff; }';
			$output .= '.feature-box-item:hover .feature-icon.iconfb-bordered.iconfb-skin-default.icon-shape, .feature-box-item:hover .feature-icon.iconfb-bordered.iconfb-skin-light.icon-shape, .feature-box-item:hover .feature-icon.iconfb-bordered.iconfb-skin-dark.icon-shape {	background-color: '. $color_scheme_1 .';	border-color: '. $color_scheme_1 .'; color: #fff; }';
			$output .= '.feature-box-item:hover .feature-icon.iconfb-bg-none.iconfb-skin-light, .feature-box-item:hover .feature-icon.iconfb-bg-none.iconfb-skin-dark, .feature-box-item:hover .feature-icon.iconfb-none.iconfb-skin-light, .feature-box-item:hover .feature-icon.iconfb-none.iconfb-skin-dark { color: '. $color_scheme_1 .'; }';
			$output .= '.tpath-listitem li:hover::before { color: '. $color_scheme_1 .'; }';
			
			$output .= '.testimonial-slider-wrapper.type-author_top .testimonial-info {	border-color: '. $color_scheme_1 .'; }';
			$output .= '.testimonial-slider-wrapper .owl-controls .owl-nav i:hover, .testimonial-slider-wrapper .owl-controls .owl-nav div:hover i { color: '. $color_scheme_1 .'; }';
			
			$output .= '.tpath-counter-icon .counter-icon, .tpath-count-number .counter { color: '. $color_scheme_1 .'; }';
			$output .= '.portfolio-tabs li a.active span, .portfolio-tabs li:hover a { color: '. $color_scheme_1 .'; }';
			$output .= '.portfolio-tabs li a.active::before { background: '. $color_scheme_1 .'; }';
			$output .= '.portfolio-tabs li a.active { color: '. $color_scheme_1 .'; }';
			
			$output .= '.btn.btn-style-outline { border-color: '. $color_scheme_1 .'; color: '. $color_scheme_1 .'; }';
			$output .= '.primary-dark-color .btn.btn-style-color:hover, .primary-dark-color .btn.btn-style-color:active, .primary-dark-color .btn.btn-style-color:focus { 	background-color: '. $color_scheme_1 .'; border-color: '. $color_scheme_1 .'; }';
			$output .= '.btn.btn-style-outline:hover, .btn.btn-style-outline:active, .btn.btn-style-outline:focus {	background-color: '. $color_scheme_1 .'; border-color: '. $color_scheme_1 .'; }';
			$output .= '.btn.btn-style-outline.btn-white:hover, .btn.btn-style-outline.btn-white:active, .btn.btn-style-outline.btn-white:focus { color: '. $color_scheme_1 .'; }';
			$output .= '.btn.btn-style-color1 { background-color: '. $color_scheme_1 .'; border-color: '. $color_scheme_1 .'; }';
			$output .= '.btn, .form-submit .submit { background: '. $color_scheme_1 .'; border-color: '. $color_scheme_1 .'; }';
			
			$output .= '.btn.btn-simple-text { color: '. $color_scheme_1 .'; }';
			$output .= '.btn.btn-active { background: '. $color_scheme_1 .'; border-color: '. $color_scheme_1 .'; }';
			$output .= '.btn.btn-active:hover, .btn.btn-active:active, .btn.btn-active:focus, .vc-btn-active .vc_general.vc_btn3.vc_btn3-style-custom:hover, .vc-btn-active .vc_general.vc_btn3.vc_btn3-style-custom:active, .vc-btn-active .vc_general.vc_btn3.vc_btn3-style-custom:focus { color: '. $color_scheme_1 .'; }';
			$output .= '.btn.btn-style-2 { background: '. $color_scheme_1 .'; border-color: '. $color_scheme_1 .'; }';
			$output .= '.btn.btn-style-2:hover, .btn.btn-style-2:active, .btn.btn-style-2:focus { border-color: '. $color_scheme_1 .'; background: '. $color_scheme_1 .'; }';
			
			$output .= 'input[type="submit"]:hover, input[type="submit"]:active, input[type="submit"]:focus, .wpcf7 input[type="submit"]:hover, .wpcf7 input[type="submit"]:active, .wpcf7 input[type="submit"]:focus { border-color: '. $color_scheme_1 .'; }';
			$output .= '.tpath-input-submit .btn.tpath-submit::before, .comment-form .form-submit .submit::before { background: '. $color_scheme_1 .'; }';
			
			$output .= '.pricing-plan-list.pricing-box:hover { border-color: '. $color_scheme_1 .'; }';
			$output .= '.tpath-pricing-item .table-header { background-color: '. $color_scheme_1 .'; }';
			$output .= '.tpath-pricing-item .table-cost sup { color: '. $color_scheme_1 .'; }';
			$output .= '.table-divider .pricing-line { background: '. $color_scheme_1 .'; }';
			
			$output .= '.rev_slider .border-bottom { background-color: '. $color_scheme_1 .'; }';
			$output .= '.tp-bullets.custom .bullet.selected { border-color:'. $color_scheme_1 .'; } ';
			
			$output .= '.widget-title::before, .tpath-section-title::before, .page-title-header::before { background-color: '. $color_scheme_1 .'; }';
			$output .= '.tpath-quote-box { background: '. $color_scheme_1 .'; }';
			$output .= '.tpath-quote-box .author-title { border-color:'. $color_scheme_1 .'; }';
			$output .= '.tpath-quote-box .author-title .author-designation { color: '. $color_scheme_1 .'; }';
			$output .= '.team-item .team-item-wrapper:hover { border-color: '. $color_scheme_1 .'; }';
			$output .= '.team-item h5.team-member-name a:hover, .team-item h5.team-member-name a:active, .team-item h5.team-member-name a:focus, .team-member_email > a:hover, .team-member_email > a:active, .team-member_email > a:focus { color: '. $color_scheme_1 .'; }';
			$output .= '.tpath-team-slider-wrapper .tpath-owl-carousel .owl-controls .owl-nav:hover .fa { color: '. $color_scheme_1 .'; }';
			$output .= '.single-tpath_team_member .team-member-designation { color: '. $color_scheme_1 .'; }';
			$output .= '.testimonial-slider-wrapper.type-default .client-author-info:before { border-color: '. $color_scheme_1 .'; }';
			$output .= '.testimonial-info .author-designation { color: '. $color_scheme_1 .'; }';
			$output .= '.owl-carousel.owl-theme .owl-controls .owl-nav div:hover i { color: '. $color_scheme_1 .'; border-color: '. $color_scheme_1 .'; }';
			$output .= '.wpb_wrapper .wpb_tabs .wpb_tabs_nav li:hover > a, .wpb_wrapper .wpb_tabs .wpb_tabs_nav li.active > a:hover, .wpb_wrapper .wpb_tabs .wpb_tabs_nav li.active > a:focus, .wpb_wrapper .wpb_tabs .wpb_tabs_nav li.active a, .wpb_wrapper .wpb_content_element .wpb_tabs_nav li.ui-tabs-active a { background: '. $color_scheme_1 .'; }';
			$output .= '.tpath-tabs-widget .nav-tabs > li:hover > a, .tpath-tabs-widget .nav-tabs > li.active > a:hover, .tpath-tabs-widget .nav-tabs > li.active > a:focus, .tpath-tabs-widget .nav-tabs > li.active a, .tpath-tabs-widget .nav-tabs > li.ui-tabs-active a { background: '. $color_scheme_1 .'; }';
			
			$output .= '.testimonial-carousel-slider.owl-carousel.owl-theme .owl-controls .owl-nav div:hover { border-color: '. $color_scheme_1 .'; }';
			$output .= '.testimonial-carousel-slider.owl-carousel.owl-theme .owl-controls .owl-nav div:hover i { color: '. $color_scheme_1 .'; }';
			$output .= '.tpath-testimonial .testimonial-content .fa { color: '. $color_scheme_1 .'; }';
			$output .= '.owl-carousel.owl-theme .owl-controls .owl-dot.active span { background: '. $color_scheme_1 .'; border-color: '. $color_scheme_1 .'; }';
			
			$output .= '.tpath-social-share-box .tpath-social-share-icons li a:hover { color: '. $color_scheme_1 .'; border-color: '. $color_scheme_1 .'; }';
			$output .= '.footer-widgets ul li:hover a, .footer-section a:hover { color: '. $color_scheme_1 .'; }';
			$output .= '.footer-section .footer-widgets .widget_nav_menu li:before { background: '. $color_scheme_1 .'; }';
			$output .= '.footer-widgets h5 { color: '. $color_scheme_1 .'; }';
			$output .= '.tpath_contact_info_widget .contact_info-inner .fa, .tpath_contact_info_widget .contact_info-inner .simple-icon { color: '. $color_scheme_1 .'; }';
			$output .= '.tpath-social-icons.widget-soc-icon li:hover a { border-color: '. $color_scheme_1 .'; color: '. $color_scheme_1 .'; }';
			$output .= '.tpath-social-icons.widget-soc-icon li:hover a i { color: '. $color_scheme_1 .'; }';
			$output .= '.tweet-item:before { color: '. $color_scheme_1 .'; }';
			$output .= '.tweet-item a { color: '. $color_scheme_1 .'; }';
			$output .= '.sidebar .contact_info-inner .fa, .sidebar .contact_info-inner .simple-icon { color: '. $color_scheme_1 .'; }';
			$output .= '.widget.widget_tag_cloud .tagcloud a:hover, .widget.widget_tag_cloud .tagcloud a:active, .widget.widget_tag_cloud .tagcloud a:focus { background: '. $color_scheme_1 .'; }';
			$output .= '.posts-content-container .entry-meta li:hover a, .posts-content-container .entry-meta li:hover { color: '. $color_scheme_1 .'; }';
			$output .= '.post .entry-title a:hover, .post .entry-title a:active, .post .entry-title a:focus, .tpath-search-results .entry-title a:hover, .tpath-search-results .entry-title a:active, .tpath-search-results .entry-title a:focus {	color: '. $color_scheme_1 .';}';
			$output .= 'blockquote::before { background: '. $color_scheme_1 .'; }';
			$output .= '.large-posts .entry-header h3.entry-title:hover a {	color: '. $color_scheme_1 .'; }';
			$output .= '.large-posts .post-media .post-overlay-icons .blog-icon { background: '. $color_scheme_1 .'; }';
			$output .= '.pagination > li > a:hover, .pagination > li > span.page-numbers.current { background: '. $color_scheme_1 .'; }';
			$output .= '.has-post-thumbnail .posts-content-container .entry-thumbnail .owl-controls .owl-nav div { background: '. $color_scheme_1 .'; }';
			$output .= '.post-navigation .pager li a { background: '. $color_scheme_1 .'; }';
			$output .= 'input[type="submit"], .wpcf7 input[type="submit"], .tpath-booking-form-wrapper .rtb-booking-form button { background: '. $color_scheme_1 .'; border-color: '. $color_scheme_1 .'; }';
			$output .= '.comment-post-meta span a { background: '. $color_scheme_1 .'; border-color:'. $color_scheme_1 .'; }';
			$output .= '.comment-post-meta span a:hover { color: '. $color_scheme_1 .'; }';
			
			$color_rgb = '';
			$color_rgb = volunteer_hex2rgb( $volunteer_options['custom_scheme_color'] );
			
			$output .= '.tpath-posts-container .post-media .entry-thumbnail > a:before { background: rgba('. $color_rgb[0] .', '. $color_rgb[1] .', '. $color_rgb[2] .', 0.8); }';
			$output .= '.bg-style.primary-color { background-color: '. $color_scheme_1 .'; }';
			$output .= '.bg-style.overlay-wrapper.bg-overlay.theme-overlay-color:before { background-color: rgba('. $color_rgb[0] .', '. $color_rgb[1] .', '. $color_rgb[2] .', 0.8); }';
			$output .= '.bg-style.overlay-wrapper.bg-overlay.theme-overlay-color.light-opacity:before { background-color: rgba('. $color_rgb[0] .', '. $color_rgb[1] .', '. $color_rgb[2] .', 0.4); }';
			$output .= '.tpath-contact-info i {	color: '. $color_scheme_1 .'; }';
			$output .= '.vc_progress_bar .vc_single_bar .vc_bar { background-color: '. $color_scheme_1 .'; }';
			$output .= '#main .vc_images_carousel .vc_carousel-control { background: '. $color_scheme_1 .'; }';
			$output .= '#main .vc_images_carousel .vc_carousel-control:hover { background: '. $color_scheme_1 .'; }';
			$output .= '#main .vc_images_carousel .vc_carousel-indicators li { background-color: '. $color_scheme_1 .'; border-color: '. $color_scheme_1 .'; }';
			$output .= '.vc_images_carousel .vc_carousel-indicators .vc_active { border-color: '. $color_scheme_1 .'; }';
			
			$output .= '.vc_toggle_content > h2 { color: '. $color_scheme_1 .'; }';
			$output .= '.vc_tta-accordion .vc_tta-panels-container .vc_tta-panel .vc_tta-panel-heading:hover a { color: '. $color_scheme_1 .'; }';
			$output .= '.vc_tta-accordion .vc_tta-panels-container .vc_tta-panel .vc_tta-panel-heading:hover a .vc_tta-controls-icon:before, .vc_tta-accordion .vc_tta-panels-container .vc_tta-panel .vc_tta-panel-heading:hover a .vc_tta-controls-icon:after { border-color: '. $color_scheme_1 .'; }';
			$output .= '#tribe-events .tribe-events-button, #tribe-events .tribe-events-button:hover, #tribe_events_filters_wrapper input[type="submit"], .tribe-events-button, .tribe-events-button.tribe-active:hover, .tribe-events-button.tribe-inactive, .tribe-events-button:hover, .tribe-events-calendar td.tribe-events-present div[id*="tribe-events-daynum-"], .tribe-events-calendar td.tribe-events-present div[id*="tribe-events-daynum-"] > a { background: '. $color_scheme_1 .'; }';
			$output .= '.ecs-img-overlay::after { background-color: rgba('. $color_rgb[0] .', '. $color_rgb[1] .', '. $color_rgb[2] .', 0.7); }';
			$output .= '.style_two .portfolio-item:hover .overlay-box::before{ background: '. $color_scheme_1 .'; }';
			$output .= '.style_two .portfolio-single-item .overlay-box:before { background: '. $color_scheme_1 .'; }';
			$output .= '.tpath-testimonial .testimonial-content { border-color: '. $color_scheme_1 .'; }';
			$output .= '.testimonial-item:hover { background: '. $color_scheme_1 .'; }';
			$output .= '.style_three .portfolio-single-item .portfolio-miniborder { background: '. $color_scheme_1 .'; }';
			$output .= '.vc_toggle .vc_toggle_content h3 { color: '. $color_scheme_1 .'; }';
			$output .= '.vc_wp_search .search-form .input-group-btn .btn-search { background: '. $color_scheme_1 .'; }';
			
			if( class_exists( 'Charitable' ) ) {
				$output .= '.countdown-period { color: '. $color_scheme_1 .'; }';
				$output .= '.campaign-loop.campaign-list .campaign-image-wrapper, .campaign-loop.campaign-grid .campaign-image-wrapper { border-color: '. $color_scheme_1 .'; }';
				$output .= '.campaign-loop .campaign-post .campaign-donation-wrapper .donate-button.button { background-color: '. $color_scheme_1 .'; }';
				$output .= '.single-campaign .pager li > a, .single-campaign .pager li > span { background-color: '. $color_scheme_1 .'; }';
				$output .= '.campaign-loop .campaign .campaign-progress-bar .bar { background-color: '. $color_scheme_1 .'; }';
				$output .= '.campaign-image-wrapper > a::after { background: '. $color_scheme_1 .'; }';
				$output .= '.charitable-form-field.charitable-submit-field .button { background: '. $color_scheme_1 .'; }';
				$output .= '.tpath-campaign-inner .campaign-donation-stats { color: '. $color_scheme_1 .'; }';
				$output .= '.entry-content .campaign-raised .amount, .entry-content .campaign-figures .amount, .entry-content .donors-count, .entry-content .time-left, .entry-content .charitable-form-field a:not(.button), .entry-content .charitable-form-fields .charitable-fieldset a:not(.button), .entry-content .charitable-notice, .entry-content .charitable-notice .errors a {	color: '. $color_scheme_1 .'; }';
			}
			
			if( class_exists( 'Woocommerce' ) ) {
				$output .= '.woo-cart-contents .woo-cart-buttons a:hover, .woo-cart-contents .woo-cart-buttons a:active, .woo-cart-contents .woo-cart-buttons a:focus { border-color: '. $color_scheme_1 .'; color: '. $color_scheme_1 .'; }';
				$output .= '.woocommerce ul.products li.product a:hover, .woocommerce ul.products li.product a:active, .woocommerce ul.products li.product a:focus { color:'. $color_scheme_1 .'; }';
				$output .= '.woocommerce ul.products li.product:hover img, .woocommerce-page ul.products li.product:hover img { border-color: '. $color_scheme_1 .'; }';
				$output .= '.woocommerce ul.products li.product .button:hover, .woocommerce ul.products li.product .button:active, .woocommerce ul.products li.product .button:focus { background: '. $color_scheme_1 .'; border-color: '. $color_scheme_1 .'; }';
				$output .= '.woocommerce .quantity .qty:focus { border-color: '. $color_scheme_1 .'; }';
				$output .= '.woocommerce ul.products li.product .product-image-wrapper > a:before { background: rgba('. $color_rgb[0] .', '. $color_rgb[1] .', '. $color_rgb[2] .', 0.6); }';
				$output .= '.woocommerce ul.products li .product-buttons-container .product-buttons a:hover, .woocommerce ul.products li .product-buttons-container .product-buttons a:active, .woocommerce ul.products li .product-buttons-container .product-buttons a:focus { color: '. $color_scheme_1 .'; }';
				$output .= '.woocommerce ul.products li.product .button, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce a.added_to_cart { color: '. $color_scheme_1 .'; border-color: '. $color_scheme_1 .'; }';
				$output .= '.woocommerce p.stars a.active::after, .woocommerce p.stars a:hover::after, .woocommerce .star-rating span:before { color: '. $color_scheme_1 .'; }';
				$output .= '.woocommerce .related.products h2:before, .woocommerce .upsells h2:before, .woocommerce .cross-sells h2:before, .woocommerce .cart_totals h2:before, .woocommerce .shipping_calculator h2:before { background: '. $color_scheme_1 .'; }';
				$output .= '.woocommerce nav.woocommerce-pagination ul li a:focus, .woocommerce nav.woocommerce-pagination ul li a:hover, .woocommerce nav.woocommerce-pagination ul li span.current { background: '. $color_scheme_1 .';}';
				$output .= '.woocommerce .widget_price_filter .ui-slider .ui-slider-range { background: '. $color_scheme_1 .'; }';
				$output .= '.widget_product_tag_cloud .tagcloud a {	background: '. $color_scheme_1 .'; }';
			}
			
		}
		
		// Light Color Scheme
		$color_scheme_2 = '';
		if( isset( $volunteer_options['custom_scheme_color_light'] ) && $volunteer_options['custom_scheme_color_light'] != '' ) {
			$color_scheme_2 = $volunteer_options['custom_scheme_color_light'];
			
			$output .= '.bg-style.primary-color .tpath-section-title .section-sub-title, .bg-style.primary-color .tpath-section-title .section-sub-title p, .bg-style.primary-color .tpath-feature-box .tpath-feature-icon i, .bg-style.primary-color .tpath-feature-box-content, .bg-style.primary-color .tpath-feature-box-content p, .author-description, .author-description p, .bg-style.primary-color .vc_cta3-content p { color: '. $color_scheme_2 . '; }';
			
			$output .= '.bg-style.primary-color .vc_column_container.border-right_only { border-color: '. $color_scheme_2 . '; }';
			
			$output .= '.bg-style.primary-color .tpath-section-title.title-bottom_border .section-title:before, .bg-style.primary-color .tpath-section-title.title-bottom_border .section-title:after, .bg-style.primary-color .tpath-section-title.title-bottom_border .section-sub-title:before, .bg-style.primary-color .tpath-section-title.title-bottom_border .section-sub-title:after { background-color: '. $color_scheme_2 . '; }';
		}
		
		// Dark Color Scheme
		$color_scheme_3 = '';
		if( isset( $volunteer_options['custom_scheme_color_dark'] ) && $volunteer_options['custom_scheme_color_dark'] != '' ) {
			$color_scheme_3 = $volunteer_options['custom_scheme_color_dark'];
			
			$output .= '.btn.btn-style-color { background-color: '. $color_scheme_3 .'; border-color: '. $color_scheme_3 .'; }';
			$output .= '.vc_general.vc_btn3.vc_btn3-style-border.vc_btn3-color-theme_dark { border-color: '. $color_scheme_3 .'; color: '. $color_scheme_3 .'; }';
			$output .= '.vc_general.vc_btn3.vc_btn3-style-border.vc_btn3-color-theme_dark:hover { background: '. $color_scheme_3 .'; }';
			$output .= '.vc_general.vc_btn3.vc_btn3-style-background.vc_btn3-color-theme_dark {	background: '. $color_scheme_3 .'; border-color: '. $color_scheme_3 .'; }';
			$output .= '.vc_general.vc_btn3.vc_btn3-style-background.vc_btn3-color-theme_dark:hover { color: '. $color_scheme_3 .'; border-color: '. $color_scheme_3 .'; }';
			
			$output .= '.bg-style.primary-dark-color { background-color: '. $color_scheme_3 . '; }';
			$output .= '.sidebar .widget { border-bottom-color: '. $color_scheme_3 . '; }';
			$output .= '.sidebar .widget a:hover, .sidebar .widget li:hover a, .sidebar .widget li.posts-item h5 a:hover, .sidebar .widget li.posts-item h5 a:active, .sidebar .widget li.posts-item h5 a:focus, .sidebar .widget ul ul li:hover a, .sidebar .widget ul ul li:hover:before, .sidebar .widget ul.children ul ul li:hover a, body .sidebar .widget ul ul ul li:hover a, body .sidebar .widget ul ul ul li:hover:before { color: '. $color_scheme_3 . '; }';
			$output .= '.sidebar .widget li:hover:before, .sidebar .widget li:before { color: '. $color_scheme_3 . '; }';
			
			$output .= '.vc_tta-style-tpath_tour_design .vc_tta-tabs-list li.vc_tta-tab > a::before { color: '. $color_scheme_3 . '; }';
			$output .= '.vc_tta-style-tpath_tour_design .vc_tta-tabs-list li.vc_tta-tab.vc_active > a {	background: '. $color_scheme_3 . '; }';			
			$output .= '.vc_tta-style-tpath_tour_design .vc_tta-tabs-list li.vc_tta-tab:hover, .vc_tta-style-tpath_tour_design .vc_tta-tabs-list li.vc_tta-tab.vc_active:hover { background: '. $color_scheme_3 . '; }';
			
			if( class_exists( 'Woocommerce' ) ) {
				$output .= '.woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover, .woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover, .woocommerce #respond input#submit.alt.disabled, .woocommerce #respond input#submit.alt.disabled:hover, .woocommerce #respond input#submit.alt:disabled, .woocommerce #respond input#submit.alt:disabled:hover, .woocommerce #respond input#submit.alt[disabled]:disabled, .woocommerce #respond input#submit.alt[disabled]:disabled:hover, .woocommerce a.button.alt.disabled, .woocommerce a.button.alt.disabled:hover, .woocommerce a.button.alt:disabled, .woocommerce a.button.alt:disabled:hover, .woocommerce a.button.alt[disabled]:disabled, .woocommerce a.button.alt[disabled]:disabled:hover, .woocommerce button.button.alt.disabled, .woocommerce button.button.alt.disabled:hover, .woocommerce button.button.alt:disabled, .woocommerce button.button.alt:disabled:hover, .woocommerce button.button.alt[disabled]:disabled, .woocommerce button.button.alt[disabled]:disabled:hover, .woocommerce input.button.alt.disabled, .woocommerce input.button.alt.disabled:hover, .woocommerce input.button.alt:disabled, .woocommerce input.button.alt:disabled:hover, .woocommerce input.button.alt[disabled]:disabled, .woocommerce input.button.alt[disabled]:disabled:hover, .woocommerce a.added_to_cart:hover { background: '. $color_scheme_3 . '; border-color: '. $color_scheme_3 . '; }';
			}
		}
		
		if( ( isset( $color_scheme_1 ) && $color_scheme_1 != '' ) && ( isset( $color_scheme_2 ) && $color_scheme_2 != '' ) ) {
			$output .= '.campaign-progress-bar .bar { background: repeating-linear-gradient(45deg, '. $color_scheme_1 .', '. $color_scheme_1 .' 10px, '. $color_scheme_2 .' 10px, '. $color_scheme_2 .' 20px); background: -webkit-repeating-linear-gradient(45deg, '. $color_scheme_1 .', '. $color_scheme_1 .' 10px, '. $color_scheme_2 .' 10px, '. $color_scheme_2 .' 20px); background: -o-repeating-linear-gradient(45deg, '. $color_scheme_1 .', '. $color_scheme_1 .' 10px, '. $color_scheme_2 .' 10px, '. $color_scheme_2 .' 20px); background: -ms-repeating-linear-gradient(45deg, '. $color_scheme_1 .', '. $color_scheme_1 .' 10px, '. $color_scheme_2 .' 10px, '. $color_scheme_2 .' 20px); background: -moz-repeating-linear-gradient(45deg, '. $color_scheme_1 .', '. $color_scheme_1 .' 10px, '. $color_scheme_2 .' 10px, '. $color_scheme_2 .' 20px); }';
		}
		
		if( ( isset( $color_scheme_1 ) && $color_scheme_1 != '' ) && ( isset( $color_scheme_2 ) && $color_scheme_2 != '' ) && ( isset( $color_scheme_3 ) && $color_scheme_3 != '' ) ) {
			$output .= '.header-main-section ul li > a::after {	background: '. $color_scheme_1 .'; background: -moz-linear-gradient(left, '. $color_scheme_1 .' 33%, '. $color_scheme_2 .' 33%, '. $color_scheme_2 .' 66%, '. $color_scheme_3 .' 66%); background: -webkit-gradient(linear, left top, right top, color-stop(33%,'. $color_scheme_1 .'), color-stop(33%,'. $color_scheme_2 .'), color-stop(66%,'. $color_scheme_2 .'), color-stop(66%,'. $color_scheme_3 .')); background: -webkit-linear-gradient(left, '. $color_scheme_1 .' 33%,'. $color_scheme_2 .' 33%,'. $color_scheme_2 .' 66%,'. $color_scheme_3 .' 66%); background: -o-linear-gradient(left, '. $color_scheme_1 .' 33%,'. $color_scheme_2 .' 33%,'. $color_scheme_2 .' 66%,'. $color_scheme_3 .' 66%); background: -ms-linear-gradient(left, '. $color_scheme_1 .' 33%,'. $color_scheme_2 .' 33%,'. $color_scheme_2 .' 66%,'. $color_scheme_3 .' 66%); background: linear-gradient(to right, '. $color_scheme_1 .' 33%,'. $color_scheme_2 .' 33%,'. $color_scheme_2 .' 66%,'. $color_scheme_3 .' 66%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr="'. $color_scheme_1 .'", endColorstr="'. $color_scheme_3 .'",GradientType=1 ); }' . "\n";
		}
		
		// Global Stylings
		if( isset( $volunteer_options['link_color'] ) && $volunteer_options['link_color'] != '' ) {
			$output .= 'a { color: '. $volunteer_options['link_color'] . '; }' . "\n";
		}
		
		if( isset( $volunteer_options['link_hover_color'] ) && $volunteer_options['link_hover_color'] != '' ) {
			$output .= 'a:hover, a:active, a:focus, .related-post-item h5 > a:hover, .related-post-item h5 > a:active, .related-post-item h5 > a:focus { color: '. $volunteer_options['link_hover_color'] . '; }' . "\n";			
		}
				
		// Header Stylings
		$header_bg_image = $volunteer_options['header_bg_image'];
		$header_bg_image_repeat = $volunteer_options['header_bg_repeat'];
		$header_bg_color = $volunteer_options['header_background_color'];
		
		$header_styles = '';
				
		if( isset( $header_bg_image ) && $header_bg_image != '' ) {
			$header_styles .= 'background-image: url('.$header_bg_image.');';
		}
		if( isset( $header_bg_image ) && $header_bg_image != '' && isset( $header_bg_image_repeat ) && $header_bg_image_repeat != '' ) {
			$header_styles .= 'background-repeat: '.$header_bg_image_repeat.';';
		}
		
		// Background Color
		$header_rgb_color = '';
		$header_rgb_color = volunteer_hex2rgb( $header_bg_color );
		
		if( isset( $header_bg_color ) && $header_bg_color != '' ) {
			$header_styles .= 'background-color: '.$header_bg_color.';';
		}
		
		if( isset( $volunteer_options['header_bg_full'] ) && $volunteer_options['header_bg_full'] ) {
			$header_styles .= 'background-size: cover;';
			$header_styles .= '-moz-background-size: cover;';
			$header_styles .= '-webkit-background-size: cover;';
			$header_styles .= '-o-background-size: cover;';
			$header_styles .= '-ms-background-size: cover;';
		}
		if( isset( $volunteer_options['header_padding_top'] ) && $volunteer_options['header_padding_top'] != '' ) {
			$header_styles .= 'padding-top: '.$volunteer_options['header_padding_top'].';';
		}			
		if( isset( $volunteer_options['header_padding_bottom'] ) && $volunteer_options['header_padding_bottom'] != '' ) {
			$header_styles .= 'padding-bottom: '.$volunteer_options['header_padding_bottom'].';';
		}			
		if( isset( $volunteer_options['header_padding_left'] ) && $volunteer_options['header_padding_left'] != '' ) {
			$header_styles .= 'padding-left: '.$volunteer_options['header_padding_left'].';';
		}			
		if( isset( $volunteer_options['header_padding_right'] ) && $volunteer_options['header_padding_right'] != '' ) {
			$header_styles .= 'padding-right: '.$volunteer_options['header_padding_right'].';';			
		}
		
		if( isset( $volunteer_options['header_parallax_bg'] ) && $volunteer_options['header_parallax_bg'] != '' ) {
			$header_styles .= 'background-attachment: fixed;';
			$header_styles .= 'background-position: top center;';
		}
		
		if( isset( $header_styles ) && $header_styles != '' ) {
			$output .= '#tpath_wrapper .header-section { '. $header_styles . ' }' . "\n";
		}
		
		// Header Top Background Color Stylings		
		$header_top_bg_color = $volunteer_options['header_top_background_color'];
		$header_top_bg_styles = '';				
		
		if( $header_top_bg_color ) {
			$header_top_bg_styles .= 'background: '.$header_top_bg_color.';';
		}
		
		if( $header_top_bg_styles ) {
			$output .= '#header-top-bar { '. $header_top_bg_styles . ' }' . "\n";
		}
		
		// Logo Stylings
		$logo_styles = '';
		if ( $volunteer_options['logo_font_styles'] ) {
			$logo_styles .= 'font-family: '.$volunteer_options['logo_font_styles']['face'].';';
			$logo_styles .= 'font-size: '.$volunteer_options['logo_font_styles']['size'].';';
			$logo_styles .= 'font-style: '.$volunteer_options['logo_font_styles']['style'].';';
			$logo_styles .= 'font-weight: '.$volunteer_options['logo_font_styles']['weight'].';';
			$logo_styles .= 'color: '.$volunteer_options['logo_font_styles']['color'].';';
		}
		
		if( $logo_styles ) {
			$output .= '.navbar-brand .site-logo-text { '. $logo_styles . ' }' . "\n";
		}
		
		// Main Site Width
		$big_container = '';
		if ( $volunteer_options['fullwidth_site_width'] ) {
			$output .= '.fullwidth .container, .tpath-owl-carousel .owl-controls { max-width: '.$volunteer_options['fullwidth_site_width'].'px; }' . "\n";
			$big_container = $volunteer_options['fullwidth_site_width'] + 60;
			$output .= '.fullwidth .container-big { max-width: '.$big_container.'px; }' . "\n";
		}
		
		if ( $volunteer_options['boxed_site_width'] ) {
			$output .= '.boxed #tpath_wrapper { max-width: '.$volunteer_options['boxed_site_width'].'px; }' . "\n";
			$output .= '.boxed .container, .tpath-owl-carousel .owl-controls, .boxed .is-sticky .header-main-section { max-width: '.$volunteer_options['boxed_site_width'].'px; }' . "\n";
			$big_container = $volunteer_options['boxed_site_width'] + 120;
			$output .= '.boxed .container-big { max-width: '.$big_container.'px; }' . "\n";
		}
				
		// Footer Widget Area Stylings
		$footer_bg_image = $volunteer_options['footer_bg_image'];
		$footer_bg_image_repeat = $volunteer_options['footer_bg_repeat'];
		$footer_bg_color = $volunteer_options['footer_widget_area_background_color'];
		
		$footer_styles = '';
				
		if( $footer_bg_image ) {
			$footer_styles .= 'background-image: url('.$footer_bg_image.');';
		}
		if( $footer_bg_image && $footer_bg_image_repeat ) {
			$footer_styles .= 'background-repeat: '.$footer_bg_image_repeat.';';
		}
		if( $footer_bg_color ) {
			$footer_styles .= 'background-color: '.$footer_bg_color.';';
		}
		if( $volunteer_options['footer_bg_full'] ) {
			$footer_styles .= 'background-size: cover;';
			$footer_styles .= '-moz-background-size: cover;';
			$footer_styles .= '-webkit-background-size: cover;';
			$footer_styles .= '-o-background-size: cover;';
			$footer_styles .= '-ms-background-size: cover;';
		}
		
		if( $volunteer_options['footer_widget_padding_top'] ) {
			$footer_styles .= 'padding-top: '.$volunteer_options['footer_widget_padding_top'].';';
		}			
		if( $volunteer_options['footer_widget_padding_bottom'] ) {
			$footer_styles .= 'padding-bottom: '.$volunteer_options['footer_widget_padding_bottom'].';';
		}			
		if( $volunteer_options['footer_widget_padding_left'] ) {
			$footer_styles .= 'padding-left: '.$volunteer_options['footer_widget_padding_left'].';';
		}			
		if( $volunteer_options['footer_widget_padding_right'] ) {
			$footer_styles .= 'padding-right: '.$volunteer_options['footer_widget_padding_right'].';';
		}	
		
		if( $footer_styles ) {
			$output .= '#footer .footer-widgets-section { '. $footer_styles . ' }' . "\n";
		}
				
		$footer_bar = '';
		
		$footer_bar_bg_color = $volunteer_options['footer_background_color'];
		
		if( $footer_bar_bg_color ) {
			$footer_bar .= 'background-color: '.$footer_bar_bg_color.';';
		}
		if( $volunteer_options['footer_padding_top'] ) {
			$footer_bar .= 'padding-top: '.$volunteer_options['footer_padding_top'].';';
		}			
		if( $volunteer_options['footer_padding_bottom'] ) {
			$footer_bar .= 'padding-bottom: '.$volunteer_options['footer_padding_bottom'].';';
		}			
		if( $volunteer_options['footer_padding_left'] ) {
			$footer_bar .= 'padding-left: '.$volunteer_options['footer_padding_left'].';';
		}			
		if( $volunteer_options['footer_padding_right'] ) {
			$footer_bar .= 'padding-right: '.$volunteer_options['footer_padding_right'].';';
		}
		
		if( isset( $volunteer_options['footer_divider_color'] ) && $volunteer_options['footer_divider_color'] != '' ) {
			$footer_bar .= 'border-color: '.$volunteer_options['footer_divider_color'].';';
		}
				
		if( $footer_bar ) {
			$output .= '#footer .footer-copyright-section { '. $footer_bar . ' }' . "\n";
		}
		
		// Body Background Stylings
		$post_meta_body_img = $body_bg_image_repeat = $body_bg_color = $body_bg_attachment = $body_bg_cover = '';
		if( is_home() ) {
			$home_id = get_option( 'page_for_posts' );
			if( is_object($post) ) { 
				$post_meta_body_img = get_post_meta( $home_id, 'volunteer_body_bg_image', true );
			}
		} else {
			if( is_object($post) ) { 
				$post_meta_body_img = get_post_meta( $post->ID, 'volunteer_body_bg_image', true );
			}
		}
		$body_bg_image = !empty( $post_meta_body_img ) ? $post_meta_body_img : $volunteer_options['body_bg_image'];
		
		if( is_home() ) {
			$home_id = get_option( 'page_for_posts' );
			if( is_object($post) ) { 
				$body_bg_image_repeat = get_post_meta( $home_id, 'volunteer_body_bg_repeat', true );
			}
		} else {
			if( is_object($post) ) { 
				$body_bg_image_repeat = get_post_meta( $post->ID, 'volunteer_body_bg_repeat', true );
			}
		}
		if( !$body_bg_image_repeat || $body_bg_image_repeat == 'default' ) {
			$body_bg_image_repeat = $volunteer_options['body_bg_repeat'];
		}
		
		if( is_home() ) {
			$home_id = get_option( 'page_for_posts' );
			if( is_object($post) ) { 
				$body_bg_color = get_post_meta( $home_id, 'volunteer_body_bg_color', true );
			}
		} else {
			if( is_object($post) ) { 
				$body_bg_color = get_post_meta( $post->ID, 'volunteer_body_bg_color', true );
			}
		}
		if( !$body_bg_color ) {
			$body_bg_color = $volunteer_options['body_bg_color'];
		}
		
		if( is_home() ) {
			$home_id = get_option( 'page_for_posts' );
			if( is_object($post) ) { 
				$body_bg_attachment = get_post_meta( $home_id, 'volunteer_body_bg_attachment', true );
			}
		} else {
			if( is_object($post) ) { 
				$body_bg_attachment = get_post_meta( $post->ID, 'volunteer_body_bg_attachment', true );
			}
		}		
		if( !$body_bg_attachment || $body_bg_attachment == 'default' ) {
			$body_bg_attachment = $volunteer_options['body_bg_attachment'];
		}
				
		if( is_home() ) {
			$home_id = get_option( 'page_for_posts' );
			if( is_object($post) ) { 
				$body_bg_cover = get_post_meta( $home_id, 'volunteer_body_bg_full', true );
			}
		} else {
			if( is_object($post) ) { 
				$body_bg_cover = get_post_meta( $post->ID, 'volunteer_body_bg_full', true );
			}
		}
		if( $body_bg_cover != 1 ) {
			$body_bg_cover = $volunteer_options['body_bg_full'];
		}
		
		$body_styles = '';
				
		if( $body_bg_image ) {
			$body_styles .= 'background-image: url('.$body_bg_image.');';
		}
		if( $body_bg_image && $body_bg_image_repeat ) {
			$body_styles .= 'background-repeat: '.$body_bg_image_repeat.';';
		}
		if( $body_bg_image && $body_bg_attachment ) {
			$body_styles .= 'background-attachment: '.$body_bg_attachment.';';
		}
		if( $body_bg_color ) {
			$body_styles .= 'background-color: '.$body_bg_color.';';
		}
		if( $body_bg_cover ) {
			$body_styles .= 'background-size: cover;';
			$body_styles .= '-moz-background-size: cover;';
			$body_styles .= '-webkit-background-size: cover;';
			$body_styles .= '-o-background-size: cover;';
			$body_styles .= '-ms-background-size: cover;';
		}
		
		if ( $volunteer_options['body_font'] ) {
			$body_styles .= 'font-family: '.$volunteer_options['body_font']['face'].';';
			$body_styles .= 'font-size: '.$volunteer_options['body_font']['size'].';';
			$body_styles .= 'font-style: '.$volunteer_options['body_font']['style'].';';
			$body_styles .= 'font-weight: '.$volunteer_options['body_font']['weight'].';';
			$body_styles .= 'color: '.$volunteer_options['body_font']['color'].';';
		}
		
		if( $body_styles ) {
			$output .= 'body { '. $body_styles . ' }' . "\n";
		}
		
		// Sticky Background Stylings
		if( $volunteer_options['sticky_background_color'] ) {
			$output .= '.is-sticky .header-main-section { background-color: '.$volunteer_options['sticky_background_color'].'; }' . "\n";
		}
				
		// Content Background Stylings					
		$content_styles = '';
				
		if( $volunteer_options['primary_content_bg_image'] ) {
			$content_styles .= 'background-image: url('.$volunteer_options['primary_content_bg_image'].');';
		}
		if( $volunteer_options['primary_content_bg_image'] && $volunteer_options['primary_content_bg_repeat'] ) {
			$content_styles .= 'background-repeat: '.$volunteer_options['primary_content_bg_repeat'].';';
		}
		if( $volunteer_options['primary_content_bg_color'] ) {		
			$content_styles .= 'background-color: '.$volunteer_options['primary_content_bg_color'].';';
		}
		if( $volunteer_options['primary_content_bg_full'] ) {
			$content_styles .= 'background-size: cover;';
			$content_styles .= '-moz-background-size: cover;';
			$content_styles .= '-webkit-background-size: cover;';
			$content_styles .= '-o-background-size: cover;';
			$content_styles .= '-ms-background-size: cover;';
		}
		
		if( $content_styles && $volunteer_options['enable_content_full_bg'] == 1 ) {
			$output .= '#main { '. $content_styles . ' }' . "\n";
		} elseif( $content_styles && $volunteer_options['enable_content_full_bg'] != 1 ) {
			$output .= '#main #primary { '. $content_styles . ' }' . "\n";
		}
		
		// Container Minimum Height
		if ( $volunteer_options['primary_content_min_height'] ) {
			$output .= '#main-wrapper, #main-wrapper #primary, #main-wrapper #sidebar, #main-wrapper #secondary-sidebar { min-height: '.$volunteer_options['primary_content_min_height'].'; }' . "\n";
		}
		
		// Primary Sidebar Background Stylings					
		$pm_sidebar_styles = '';
				
		if( $volunteer_options['primary_sidebar_bg_image'] ) {
			$pm_sidebar_styles .= 'background-image: url('.$volunteer_options['primary_sidebar_bg_image'].');';
		}
		if( $volunteer_options['primary_sidebar_bg_image'] && $volunteer_options['primary_sidebar_bg_repeat'] ) {
			$pm_sidebar_styles .= 'background-repeat: '.$volunteer_options['primary_sidebar_bg_repeat'].';';
		}
		if( $volunteer_options['primary_sidebar_bg_color'] ) {
			$pm_sidebar_styles .= 'background-color: '.$volunteer_options['primary_sidebar_bg_color'].';';
		}
		if( $volunteer_options['primary_sidebar_bg_full'] ) {
			$pm_sidebar_styles .= 'background-size: cover;';
			$pm_sidebar_styles .= '-moz-background-size: cover;';
			$pm_sidebar_styles .= '-webkit-background-size: cover;';
			$pm_sidebar_styles .= '-o-background-size: cover;';
			$pm_sidebar_styles .= '-ms-background-size: cover;';
		}
		
		if( $pm_sidebar_styles && $volunteer_options['enable_content_full_bg'] != 1 ) {
			$output .= '#main #sidebar { '. $pm_sidebar_styles . ' }' . "\n";
		}		
		
		// Dropdown Menu Width
		$sub_menu_width = '';
		if ( $volunteer_options['dropdown_menu_width'] ) {
			$output .= '.dropdown-menu { min-width: '.$volunteer_options['dropdown_menu_width'].'; }' . "\n";
		}
		
		// Top Menu Font & Color Stylings
		$top_menu_font_styles = '';
		if ( $volunteer_options['top_menu_font_styles'] ) {
			$top_menu_font_styles .= 'font-family: '.$volunteer_options['top_menu_font_styles']['face'].';';
			$top_menu_font_styles .= 'font-size: '.$volunteer_options['top_menu_font_styles']['size'].';';
			$top_menu_font_styles .= 'font-style: '.$volunteer_options['top_menu_font_styles']['style'].';';
			$top_menu_font_styles .= 'font-weight: '.$volunteer_options['top_menu_font_styles']['weight'].';';
			$top_menu_font_styles .= 'color: '.$volunteer_options['top_menu_font_styles']['color'].';';
		}
		if( $top_menu_font_styles ) {
			$output .= '.header-top-section .tpath-top-nav .navbar-nav > li > a { '. $top_menu_font_styles . ' }' . "\n";
		}
		
		// Main Menu Font & Color Stylings
		$menu_font_styles = '';
		if ( $volunteer_options['menu_font_styles'] ) {
			$menu_font_styles .= 'font-family: '.$volunteer_options['menu_font_styles']['face'].';';
			$menu_font_styles .= 'font-size: '.$volunteer_options['menu_font_styles']['size'].';';
			$menu_font_styles .= 'font-style: '.$volunteer_options['menu_font_styles']['style'].';';
			$menu_font_styles .= 'font-weight: '.$volunteer_options['menu_font_styles']['weight'].';';
			$menu_font_styles .= 'color: '.$volunteer_options['menu_font_styles']['color'].';';
		}
		if( $menu_font_styles ) {
			$output .= '.nav.navbar-nav.tpath-main-nav li a, .nav.navbar-nav.tpath-main-nav li span.menu-toggler, .menu-icon-box { '. $menu_font_styles . ' }' . "\n";
		}
		
		$menu_styles = '';
		
		if ( $volunteer_options['menu_font_hover_color'] ) {
			$menu_styles .= 'color: '.$volunteer_options['menu_font_hover_color'].';';
		}
		
		if( $menu_styles ) {
			$output .= '.nav.navbar-nav.tpath-main-nav a:hover, .nav.navbar-nav.tpath-main-nav a:focus, .nav.navbar-nav.tpath-main-nav a:active, .nav.navbar-nav.tpath-main-nav li.active > a, .nav.navbar-nav.tpath-main-nav .current-menu-ancestor > a, .nav.navbar-nav.tpath-main-nav .current-menu-ancestor .dropdown-menu .current-menu-item > a, .nav.navbar-nav .side-menu a:hover, .nav.navbar-nav .side-menu a:active, .nav.navbar-nav .side-menu a:focus { '. $menu_styles .' }' . "\n";
		}
		
		$submenu_link_styles = '';
		if ( $volunteer_options['submenu_font_styles'] ) {
			$submenu_link_styles .= 'font-family: '.$volunteer_options['submenu_font_styles']['face'].';';
			$submenu_link_styles .= 'font-size: '.$volunteer_options['submenu_font_styles']['size'].';';
			$submenu_link_styles .= 'font-style: '.$volunteer_options['submenu_font_styles']['style'].';';
			$submenu_link_styles .= 'font-weight: '.$volunteer_options['submenu_font_styles']['weight'].';';
			$submenu_link_styles .= 'color: '.$volunteer_options['submenu_font_styles']['color'].';';
		}
		
		if ( $volunteer_options['sub_menu_bg_color'] ) {
			$output .= '.dropdown-menu a, .mobile-sub-menu a, .header-main-section .nav.navbar-nav ul.dropdown-menu li.current-menu-parent ul li a, .header-main-section .nav.navbar-nav ul.dropdown-menu li.current-menu-parent ul li a { background-color: '.$volunteer_options['sub_menu_bg_color'].'; }';
		}
		
		if( $submenu_link_styles ) {
			$output .= '.header-section .nav.navbar-nav .dropdown-menu > li > a, .dropdown-menu .sub-menu a, .header-main-section .nav.navbar-nav ul.dropdown-menu li.current-menu-parent ul li a { '. $submenu_link_styles .' }' . "\n";
		}
		
		$hover_menu_styles = '';
				
		if ( $volunteer_options['sub_menu_font_hover_color'] ) {
			$hover_menu_styles .= 'color: '.$volunteer_options['sub_menu_font_hover_color'].';';			
		}
		if ( $volunteer_options['sub_menu_bg_hover_color'] ) {
			$hover_menu_styles .= 'background-color: '.$volunteer_options['sub_menu_bg_hover_color'].';';			
		}
		
		if( $hover_menu_styles ) {
			$output .= '.header-section .header-main-section .nav.navbar-nav.tpath-main-nav .dropdown-menu > li > a:hover, .header-section .header-main-section .nav.navbar-nav.tpath-main-nav .dropdown-menu ul.sub-menu li > a:hover, .header-section .header-main-section .nav.navbar-nav.tpath-main-nav .current-menu-ancestor .dropdown-menu .current-menu-item > a, .header-main-section .nav.navbar-nav ul.dropdown-menu li.current-menu-parent a { '. $hover_menu_styles .' }' . "\n";
		}
			
		// Heading Styles
		$h1_styles = '';
		if ( $volunteer_options['h1_font_styles'] ) {
			$h1_styles .= 'font-family: '.$volunteer_options['h1_font_styles']['face'].';';
			$h1_styles .= 'font-size: '.$volunteer_options['h1_font_styles']['size'].';';
			$h1_styles .= 'font-style: '.$volunteer_options['h1_font_styles']['style'].';';
			$h1_styles .= 'font-weight: '.$volunteer_options['h1_font_styles']['weight'].';';
			$h1_styles .= 'color: '.$volunteer_options['h1_font_styles']['color'].';';
		}
		if( $h1_styles ) {
			$output .= 'h1, .tp-caption.slider-title { '. $h1_styles . ' }' . "\n";
		}
		
		$h2_styles = '';
		if ( $volunteer_options['h2_font_styles'] ) {
			$h2_styles .= 'font-family: '.$volunteer_options['h2_font_styles']['face'].';';
			$h2_styles .= 'font-size: '.$volunteer_options['h2_font_styles']['size'].';';
			$h2_styles .= 'font-style: '.$volunteer_options['h2_font_styles']['style'].';';
			$h2_styles .= 'font-weight: '.$volunteer_options['h2_font_styles']['weight'].';';
			$h2_styles .= 'color: '.$volunteer_options['h2_font_styles']['color'].';';
		}
		if( $h2_styles ) {
			$output .= 'h2, h2 a, .tp-caption.slider-subtitle { '. $h2_styles . ' }' . "\n";
		}
		
		$h3_styles = '';
		if ( $volunteer_options['h3_font_styles'] ) {
			$h3_styles .= 'font-family: '.$volunteer_options['h3_font_styles']['face'].';';
			$h3_styles .= 'font-size: '.$volunteer_options['h3_font_styles']['size'].';';
			$h3_styles .= 'font-style: '.$volunteer_options['h3_font_styles']['style'].';';
			$h3_styles .= 'font-weight: '.$volunteer_options['h3_font_styles']['weight'].';';
			$h3_styles .= 'color: '.$volunteer_options['h3_font_styles']['color'].';';
		}
		if( $h3_styles ) {
			$output .= 'h3 { '. $h3_styles . ' }' . "\n";
		}
		
		$h4_styles = '';
		if ( $volunteer_options['h4_font_styles'] ) {
			$h4_styles .= 'font-family: '.$volunteer_options['h4_font_styles']['face'].';';
			$h4_styles .= 'font-size: '.$volunteer_options['h4_font_styles']['size'].';';
			$h4_styles .= 'font-style: '.$volunteer_options['h4_font_styles']['style'].';';
			$h4_styles .= 'font-weight: '.$volunteer_options['h4_font_styles']['weight'].';';
			$h4_styles .= 'color: '.$volunteer_options['h4_font_styles']['color'].';';
		}
		if( $h4_styles ) {
			$output .= 'h4, .comment-reply-title, .widget-title { '. $h4_styles . ' }' . "\n";
		}
		
		$h5_styles = '';
		if ( $volunteer_options['h5_font_styles'] ) {
			$h5_styles .= 'font-family: '.$volunteer_options['h5_font_styles']['face'].';';
			$h5_styles .= 'font-size: '.$volunteer_options['h5_font_styles']['size'].';';
			$h5_styles .= 'font-style: '.$volunteer_options['h5_font_styles']['style'].';';
			$h5_styles .= 'font-weight: '.$volunteer_options['h5_font_styles']['weight'].';';
			$h5_styles .= 'color: '.$volunteer_options['h5_font_styles']['color'].';';
		}
		if( $h5_styles ) {
			$output .= 'h5 { '. $h5_styles . ' }' . "\n";
		}
		
		$h6_styles = '';
		if ( $volunteer_options['h6_font_styles'] ) {
			$h6_styles .= 'font-family: '.$volunteer_options['h6_font_styles']['face'].';';
			$h6_styles .= 'font-size: '.$volunteer_options['h6_font_styles']['size'].';';
			$h6_styles .= 'font-style: '.$volunteer_options['h6_font_styles']['style'].';';
			$h6_styles .= 'font-weight: '.$volunteer_options['h6_font_styles']['weight'].';';
			$h6_styles .= 'color: '.$volunteer_options['h6_font_styles']['color'].';';
		}
		if( $h6_styles ) {
			$output .= 'h6, .tp-caption.slider-thumb-title { '. $h6_styles . ' }' . "\n";
		}
		
		$section_styles = '';
		if ( $volunteer_options['section_font_styles'] ) {
			$section_styles .= 'font-family: '.$volunteer_options['section_font_styles']['face'].';';
			//$section_styles .= 'font-size: '.$volunteer_options['section_font_styles']['size'].';';
			$section_styles .= 'font-style: '.$volunteer_options['section_font_styles']['style'].';';
			$section_styles .= 'font-weight: '.$volunteer_options['section_font_styles']['weight'].';';
			$section_styles .= 'color: '.$volunteer_options['section_font_styles']['color'].';';
			
			if( isset( $volunteer_options['section_font_styles']['size'] ) && $volunteer_options['section_font_styles']['size'] != '' ) {
				$output .= '.parallax-title, .tpath-section-title div.section-title { font-size: '.$volunteer_options['section_font_styles']['size'].'; }' . "\n";
			}
		}
		if( $section_styles ) {
			$output .= '.parallax-title, .tpath-section-title .section-title { '. $section_styles . ' }' . "\n";
		}	
				
		$single_post_title_styles = '';
		if ( isset( $volunteer_options['single_post_title_font_styles'] ) && $volunteer_options['single_post_title_font_styles'] ) {
			$single_post_title_styles .= 'font-family: '.$volunteer_options['single_post_title_font_styles']['face'].';';
			$single_post_title_styles .= 'font-size: '.$volunteer_options['single_post_title_font_styles']['size'].';';
			$single_post_title_styles .= 'font-style: '.$volunteer_options['single_post_title_font_styles']['style'].';';
			$single_post_title_styles .= 'font-weight: '.$volunteer_options['single_post_title_font_styles']['weight'].';';
			$single_post_title_styles .= 'color: '.$volunteer_options['single_post_title_font_styles']['color'].';';
		}
		if( isset( $single_post_title_styles ) && $single_post_title_styles != '' ) {
			$output .= '.single-post .entry-title, .category-title { '. $single_post_title_styles . ' }' . "\n";
		}
		
		$sidebar_widget_heading_styles = '';
		if ( $volunteer_options['widget_title_fonts'] ) {
			$sidebar_widget_heading_styles .= 'font-family: '.$volunteer_options['widget_title_fonts']['face'].';';
			$sidebar_widget_heading_styles .= 'font-size: '.$volunteer_options['widget_title_fonts']['size'].';';
			$sidebar_widget_heading_styles .= 'font-style: '.$volunteer_options['widget_title_fonts']['style'].';';
			$sidebar_widget_heading_styles .= 'font-weight: '.$volunteer_options['widget_title_fonts']['weight'].';';
			$sidebar_widget_heading_styles .= 'color: '.$volunteer_options['widget_title_fonts']['color'].';';
		}
		if( $sidebar_widget_heading_styles ) {
			$output .= '.sidebar h3.widget-title { '. $sidebar_widget_heading_styles . ' }' . "\n";
		}
		
		$sidebar_widget_text_styles = '';
		if ( $volunteer_options['widget_text_fonts'] ) {
			$sidebar_widget_text_styles .= 'font-family: '.$volunteer_options['widget_text_fonts']['face'].';';
			$sidebar_widget_text_styles .= 'font-size: '.$volunteer_options['widget_text_fonts']['size'].';';
			$sidebar_widget_text_styles .= 'font-style: '.$volunteer_options['widget_text_fonts']['style'].';';
			$sidebar_widget_text_styles .= 'font-weight: '.$volunteer_options['widget_text_fonts']['weight'].';';
			$sidebar_widget_text_styles .= 'color: '.$volunteer_options['widget_text_fonts']['color'].';';
		}
		if( $sidebar_widget_text_styles ) {
			$output .= '.sidebar .widget p, .sidebar .widget ul li, .sidebar .widget div, .sidebar .widget ul li a { '. $sidebar_widget_text_styles . ' }' . "\n";
		}
		
		$footer_widget_heading_styles = '';
		if ( $volunteer_options['footer_widget_title_fonts'] ) {
			$footer_widget_heading_styles .= 'font-family: '.$volunteer_options['footer_widget_title_fonts']['face'].';';
			$footer_widget_heading_styles .= 'font-size: '.$volunteer_options['footer_widget_title_fonts']['size'].';';
			$footer_widget_heading_styles .= 'font-style: '.$volunteer_options['footer_widget_title_fonts']['style'].';';
			$footer_widget_heading_styles .= 'font-weight: '.$volunteer_options['footer_widget_title_fonts']['weight'].';';
			$footer_widget_heading_styles .= 'color: '.$volunteer_options['footer_widget_title_fonts']['color'].';';
		}
		if( $footer_widget_heading_styles ) {
			$output .= '.footer-widgets .widget h3 { '. $footer_widget_heading_styles . ' }' . "\n";
		}
		
		$footer_widget_text_styles = '';
		if ( $volunteer_options['footer_widget_text_fonts'] ) {
			$footer_widget_text_styles .= 'font-family: '.$volunteer_options['footer_widget_text_fonts']['face'].';';
			$footer_widget_text_styles .= 'font-size: '.$volunteer_options['footer_widget_text_fonts']['size'].';';
			$footer_widget_text_styles .= 'font-style: '.$volunteer_options['footer_widget_text_fonts']['style'].';';
			$footer_widget_text_styles .= 'font-weight: '.$volunteer_options['footer_widget_text_fonts']['weight'].';';
			$footer_widget_text_styles .= 'color: '.$volunteer_options['footer_widget_text_fonts']['color'].';';
		}
		if( $footer_widget_text_styles ) {
			$output .= '.footer-widgets div, .footer-widgets p, .footer-widgets .widget_categories ul li a, .footer-copyright-section p { '. $footer_widget_text_styles . ' }' . "\n";
		}
		
		$map_styles = '';
		if( is_page_template( 'template-contact.php' ) && $volunteer_options['show_google_map_contact'] && $volunteer_options['gmap_address'] ) {
			$map_styles .= 'width: '.$volunteer_options['gmap_width'].'; margin:0 auto; height: '.$volunteer_options['gmap_height'].';';			
			if( $volunteer_options['gmap_width'] != '100%') {
				$map_styles .= 'margin-top: 20px;';		
			}
		}
		
		if( $map_styles ) {
			$output .= '#tpath_gmap { '.$map_styles.' }' . "\n";
		}
		
		// Front Page Parallax Styles
		if( is_page_template( 'template-parallax.php' ) ) {
			/* Check for Query */
			$page_query = volunteer_parallax_front_query();	
				
			if( !empty( $page_query ) ) {
			
				$query_styles = new WP_Query( $page_query );
					
				if( $query_styles->have_posts() ) :
					while ( $query_styles->have_posts() ) : $query_styles->the_post();
						global $post;							
						$backup = $post;
					
						$tpath_additional_sections_order = get_post_meta( $post->ID, 'volunteer_parallax_additional_sections_order', true );
						
						$output .= volunteer_parallax_custom_styles( $post );
						
						if( $tpath_additional_sections_order != '' ) {
							$additional_query = volunteer_parallax_additional_query( $tpath_additional_sections_order );
							
							if( !empty( $additional_query ) ) {							
								$custom_query = new WP_Query( $additional_query );
							}
							if ( $custom_query->have_posts() ):
								while ( $custom_query->have_posts() ): $custom_query->the_post();
								
									$output .= volunteer_parallax_custom_styles( $post );
									
								endwhile;
							endif;
							wp_reset_postdata();
						}
						
						$post = $backup;
						
					endwhile;
				endif;
				wp_reset_postdata();
			}			
		}
				
		// Output Custom Styles
		if (isset($output)) {			
			return $output;
		}
		
	} // End volunteer_custom_styles()
	
}

/* =========================================
 * Parallax Custom Styles Output
 * ========================================= */
 
if ( ! function_exists( 'volunteer_parallax_custom_styles' ) ) {
	function volunteer_parallax_custom_styles( $post ) {
	
		global $post;
		
		$output = '';
		
		// Section Padding Styles
		$tpath_section_padding_top = get_post_meta( $post->ID, 'volunteer_section_padding_top', true);
		$tpath_section_padding_bottom = get_post_meta( $post->ID, 'volunteer_section_padding_bottom', true);
		$tpath_section_header_padding = get_post_meta( $post->ID, 'volunteer_section_header_padding', true);
		
		if( ( isset($tpath_section_padding_top) && $tpath_section_padding_top != '' ) || ( isset($tpath_section_padding_bottom) && $tpath_section_padding_bottom != '' ) ) {
			$output .= '#page-' . $post->post_name . ' {';
			if( isset($tpath_section_padding_top) && $tpath_section_padding_top != '' ) {
				$output .= ' padding-top:' . $tpath_section_padding_top . '; ';
			}
			if( isset($tpath_section_padding_bottom) && $tpath_section_padding_bottom != '' ) {
				$output .= 'padding-bottom:' . $tpath_section_padding_bottom . ';';
			}
			$output .= ' }'. "\n";
		}
		
		if( isset($tpath_section_header_padding) && $tpath_section_header_padding != '' ) {
			$output .= '#page-' . $post->post_name . ' .parallax-header { padding-bottom:' . $tpath_section_header_padding . '; }'. "\n";
		}		
		
		// Section Color Styles
		$tpath_section_title_color = get_post_meta( $post->ID, 'volunteer_section_title_color', true);
		$tpath_section_slogan_color = get_post_meta( $post->ID, 'volunteer_section_slogan_color', true);
		$tpath_section_text_color = get_post_meta( $post->ID, 'volunteer_section_text_color', true);
		$tpath_section_content_heading_color = get_post_meta( $post->ID, 'volunteer_section_content_heading_color', true);
		
		if( !empty($tpath_section_title_color) ) {
			$output.= '#page-' . $post->post_name . ' .parallax-title { color: ' . $tpath_section_title_color . '; }'. "\n";
		}
		
		if( !empty($tpath_section_slogan_color) ) {
			$output.= '#page-' . $post->post_name . ' .parallax-desc { color: ' . $tpath_section_slogan_color . '; }'. "\n";
		}
		
		if( !empty($tpath_section_text_color) ) {
			$output.= '#page-' . $post->post_name . ' .parallax-content { color: ' . $tpath_section_text_color . '; }'. "\n";
		}
		
		if( !empty($tpath_section_content_heading_color) ) {
			$output.= '#page-' . $post->post_name . ' .parallax-content h1, 
						#page-' . $post->post_name . ' .parallax-content h2, 
						#page-' . $post->post_name . ' .parallax-content h3, 
						#page-' . $post->post_name . ' .parallax-content h4, 
						#page-' . $post->post_name . ' .parallax-content h5, 
						#page-' . $post->post_name . ' .parallax-content h6 { color: ' . $tpath_section_content_heading_color . '; }'. "\n";
		}
		
		// Section Background
		$tpath_parallax_bg_image = get_post_meta( $post->ID, 'volunteer_parallax_bg_image', true);
		$tpath_parallax_bg_repeat = get_post_meta( $post->ID, 'volunteer_parallax_bg_repeat', true);
		$tpath_parallax_bg_attachment = get_post_meta( $post->ID, 'volunteer_parallax_bg_attachment', true);
		$tpath_parallax_bg_postion = get_post_meta( $post->ID, 'volunteer_parallax_bg_postion', true);
		$tpath_parallax_bg_size = get_post_meta( $post->ID, 'volunteer_parallax_bg_size', true);
		
		$tpath_parallax_bg_repeat = !empty($tpath_parallax_bg_repeat) ? $tpath_parallax_bg_repeat : 'no-repeat';
		
		$parallax_background = '';
		
		if( !empty($tpath_parallax_bg_image) ) {
			$parallax_background = 'background-image: url(' . $tpath_parallax_bg_image . ');';
			$parallax_background .= 'background-repeat: ' . $tpath_parallax_bg_repeat . ';';
			if( !empty($tpath_parallax_bg_postion) ) {
				$parallax_background .= 'background-position: ' . $tpath_parallax_bg_postion . ';';
			}
			if( !empty($tpath_parallax_bg_size) ) {
				$parallax_background .= 'background-size: ' . $tpath_parallax_bg_size . ';';
			}
			if( !empty($tpath_parallax_bg_attachment) ) {
				$parallax_background .= 'background-attachment: ' . $tpath_parallax_bg_attachment . ';';
			}
		}
		if( !empty($tpath_parallax_bg_image) ) {						
			$output.= '#section-' . $post->post_name . ' { '. $parallax_background . ' }'. "\n";
		}
		
		$tpath_section_bg_color = get_post_meta( $post->ID, 'volunteer_section_bg_color', true);
		if( !empty($tpath_section_bg_color) ) {						
			$output.= '#page-' . $post->post_name . ' { background-color: ' . $tpath_section_bg_color . '; }'. "\n";
		}
		
		$tpath_parallax_bg_overlay = get_post_meta( $post->ID, 'volunteer_parallax_bg_overlay', true);
		if( $tpath_parallax_bg_overlay == 'yes' ) {
			$tpath_section_overlay_color = get_post_meta( $post->ID, 'volunteer_section_overlay_color', true);
			$tpath_overlay_color_opacity = get_post_meta( $post->ID, 'volunteer_overlay_color_opacity', true);
			
			$tpath_overlay_color_opacity = $tpath_overlay_color_opacity != 0 ? $tpath_overlay_color_opacity : '0.7';
			
			$rgb_color = '';
			$rgb_color = volunteer_hex2rgb( $tpath_section_overlay_color );
			
			$output.= '#page-' . $post->post_name . ' { background-color: rgba(' . $rgb_color[0] . ',' . $rgb_color[1] . ',' . $rgb_color[2] . ', ' . $tpath_overlay_color_opacity . '); }'. "\n";
		}
		
		return $output;
		
	}
}

/* =========================================
 * Load theme style and js in the <head>
 * ========================================= */

if ( ! function_exists( 'volunteer_load_theme_scripts' ) ) {

	function volunteer_load_theme_scripts () {
	
		global $volunteer_options, $is_IE;
		
		// Stylesheet
		wp_register_style( 'volunteer-prettyphoto-style', get_template_directory_uri() . '/css/prettyPhoto.css' );
		wp_enqueue_style( 'volunteer-prettyphoto-style' );
		
		wp_register_style( 'volunteer-font-awesome-style', get_template_directory_uri() . '/css/font-awesome.min.css' );
		wp_enqueue_style( 'volunteer-font-awesome-style' );
		
		wp_register_style( 'volunteer-simple-line-icons-style', get_template_directory_uri() . '/css/simple-line-icons.css' );
		wp_enqueue_style( 'volunteer-simple-line-icons-style' );
		
		wp_register_style( 'volunteer-font-awesome-social-style', get_template_directory_uri() . '/css/font-awesome-social.css' );
		wp_enqueue_style( 'volunteer-font-awesome-social-style' );
		
		wp_register_style( 'volunteer-flaticons-style', get_template_directory_uri() . '/css/flaticon.css' );
		wp_enqueue_style( 'volunteer-flaticons-style' );
		
		wp_register_style( 'volunteer-owlcarousel-style', get_template_directory_uri() . '/css/owl.carousel.css' );
		wp_enqueue_style( 'volunteer-owlcarousel-style' );
		
		wp_register_style( 'volunteer-effects-style', get_template_directory_uri() . '/css/animate.css' );
		wp_enqueue_style( 'volunteer-effects-style' );
		
		wp_enqueue_style( 'volunteer-ratings-stars', get_template_directory_uri() . '/css/rateit.css' );
		
		wp_enqueue_style('wp-mediaelement');
		
		if( is_singular( 'post' ) ) {
			wp_enqueue_style( 'js_composer_front' );
			wp_enqueue_style( 'js_composer_custom_css' );
		}
		
		wp_register_style( 'volunteer-mediaelement-style', get_template_directory_uri() . '/css/mediaelementplayer.css' );
		wp_enqueue_style( 'volunteer-mediaelement-style' );
		
		wp_register_style( 'volunteer-bootstrap-validation-style', get_template_directory_uri() . '/css/bootstrapValidator.min.css' );
		wp_enqueue_style( 'volunteer-bootstrap-validation-style' );
		
		wp_register_style( 'volunteer-theme-bootstrap-style', get_template_directory_uri() . '/css/bootstrap.min.css' );
		wp_enqueue_style( 'volunteer-theme-bootstrap-style' );
		
		wp_register_style( 'volunteer-theme-style', get_stylesheet_uri() );
		wp_enqueue_style( 'volunteer-theme-style' );		
		
		if($volunteer_options['enable_responsive']) {
			wp_register_style( 'volunteer-theme-responsive-style', get_template_directory_uri() . '/css/responsive.css' );
			wp_enqueue_style( 'volunteer-theme-responsive-style' );
		}
		
		if( isset( $volunteer_options['color_scheme'] ) && $volunteer_options['color_scheme'] != '' ) {
			wp_register_style( 'volunteer-color-scheme-style', get_template_directory_uri() . '/color-schemes/'.$volunteer_options['color_scheme'].'' );
			wp_enqueue_style( 'volunteer-color-scheme-style' );
		} else {
			wp_register_style( 'volunteer-color-scheme-style', get_template_directory_uri() . '/color-schemes/default.css' );
			wp_enqueue_style( 'volunteer-color-scheme-style' );
		}
				
		wp_register_style( 'volunteer-bg-videoplayer-style', get_template_directory_uri() . '/css/mb.YTPlayer.css' );
		wp_enqueue_style( 'volunteer-bg-videoplayer-style' );
		
		// Load Google Fonts
		$google_font = array();
		$fonts_options = array();
		
		$fonts_options = array( 
				$volunteer_options['body_font'],
				$volunteer_options['logo_font_styles'],
				$volunteer_options['h1_font_styles'], 
				$volunteer_options['h2_font_styles'], 
				$volunteer_options['h3_font_styles'], 
				$volunteer_options['h4_font_styles'], 
				$volunteer_options['h5_font_styles'], 
				$volunteer_options['h6_font_styles'],
				$volunteer_options['top_menu_font_styles'],
				$volunteer_options['menu_font_styles'],
				$volunteer_options['submenu_font_styles'],
				$volunteer_options['single_post_title_font_styles'],
				$volunteer_options['widget_title_fonts'],
				$volunteer_options['widget_text_fonts'],
				$volunteer_options['footer_widget_title_fonts'],
				$volunteer_options['footer_widget_text_fonts']
			);
		
		foreach( $fonts_options as $fonts_option ) {
			$font = urlencode( $fonts_option['face'] );
			if( !in_array($font, $google_font) ) {
				$google_font[] = $font;
			}
		}
		
		$font_family = '';
		foreach( $google_font as $font ) {
			if( isset( $font ) && $font != '' ) {
				$font_family .= $font . ':200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic%7C';
			}
		}
		
		if( $font_family ) {
			wp_register_style( 'volunteer-google-fonts', "//fonts.googleapis.com/css?family=" . $font_family . "&amp;subset=latin,greek-ext,cyrillic,latin-ext,greek,cyrillic-ext,vietnamese" );
			wp_enqueue_style( 'volunteer-google-fonts' );
		}
		
		// Javascripts
		wp_register_script( 'volunteer-html5-js', get_template_directory_uri() . '/js/html5.js', array() );
		wp_register_script( 'volunteer-respond-js', get_template_directory_uri() . '/js/respond.min.js', array() );
		wp_register_script( 'volunteer-lineicon-ie-js', get_template_directory_uri() . '/js/icons-lte-ie7.js', array() );
		
		if( $is_IE ) {
			wp_enqueue_script( 'volunteer-html5-js' );
			wp_enqueue_script( 'volunteer-respond-js' );
			wp_enqueue_script( 'volunteer-lineicon-ie-js' );
		}
		
		wp_register_script( 'volunteer-bootstrap-validator-js', get_template_directory_uri() . '/js/bootstrapValidator.min.js', array('jquery') );
		wp_enqueue_script( 'volunteer-bootstrap-validator-js' );
				
        wp_register_script( 'volunteer-bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery') );
		wp_enqueue_script( 'volunteer-bootstrap-js' );
		
		wp_register_script( 'volunteer-general-js', get_template_directory_uri() . '/js/general.js', array('jquery') );
		wp_enqueue_script( 'volunteer-general-js' );
		
		// Map Js load only where needed
		wp_register_script( 'volunteer-gmap-js', get_template_directory_uri() . '/js/gmap.min.js', array('jquery') );
		wp_register_script( 'volunteer-gmap-api-js', 'http://maps.google.com/maps/api/js?sensor=false&amp;language=en', array('jquery') );		
				
		// Easy Ticker Js		
		wp_register_script( 'volunteer-easy-ticker-js', get_template_directory_uri() . '/js/jquery.easy-ticker.min.js', array() );
		
		wp_enqueue_script( 'mediaelement' );
		wp_enqueue_script( 'wp-mediaelement' );
		
		// Video Slider Js
		wp_register_script( 'volunteer-video-slider-js', get_template_directory_uri() . '/js/jquery.mb.YTPlayer.js', array() );
		
		// Countdown Js
		wp_register_script( 'volunteer-countdown-plugin-js', get_template_directory_uri() . '/js/jquery.countdown-plugin.min.js', array('jquery'), null );
		wp_register_script( 'volunteer-countdown-js', get_template_directory_uri() . '/js/jquery.countdown.min.js', array('jquery'), null );
		
		// Counter Js
		wp_register_script( 'volunteer-countto-js', get_template_directory_uri() . '/js/jquery.countTo.js', array('jquery') );
		
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
		
		// Load Javascripts	in Footer
		wp_register_script( 'volunteer-main-js', get_template_directory_uri() . '/js/jquery.main.js', array(), null, true );
		wp_enqueue_script( 'volunteer-main-js' );
		
		wp_register_script( 'volunteer-modernizr-js', get_template_directory_uri() . '/js/modernizr.min.js', array('jquery'), '2.8.3', true );
		wp_enqueue_script( 'volunteer-modernizr-js' );
		
		wp_register_script( 'volunteer-prettyphoto-js', get_template_directory_uri() . '/js/jquery.prettyPhoto.js', array('jquery'), '3.1.5', true );
		wp_enqueue_script( 'volunteer-prettyphoto-js' );
		
		wp_register_script( 'volunteer-ratings', get_template_directory_uri() . '/js/jquery.rateit.min.js', array('jquery'), null, true );
		wp_enqueue_script( 'volunteer-ratings' );
		
		// Carousel Slider Js
		wp_register_script( 'volunteer-carousel-slider-js', get_template_directory_uri() . '/js/jquery.carousel.min.js', array('jquery'), null, true );
		wp_enqueue_script( 'volunteer-carousel-slider-js' );
		
		wp_register_script( 'volunteer-carousel-custom-js', get_template_directory_uri() . '/js/carousel-custom.js', array(), null, true );
		wp_enqueue_script( 'volunteer-carousel-custom-js' );
		
		wp_register_script( 'volunteer-match-height-js', get_template_directory_uri() . '/js/jquery.match-height.js', array(), null, true );
		wp_enqueue_script( 'volunteer-match-height-js' );
	
		if( is_page_template( 'template-contact.php' ) && $volunteer_options['show_google_map_contact'] && $volunteer_options['gmap_address'] ) {
			wp_enqueue_script( 'volunteer-gmap-api-js' );
			wp_enqueue_script( 'volunteer-gmap-js' );
		}
		
		$template_uri = get_template_directory_uri();
		
		wp_localize_script('jquery', 'volunteer_js_vars', array( 'volunteer_template_uri' 	=> $template_uri,
															  'volunteer_ajax_url'			=> admin_url('admin-ajax.php') ));

	} // End volunteer_load_theme_scripts()
	
}

/* =========================================
 * Load theme dynamic js in the <head>
 * ========================================= */

if ( ! function_exists( 'volunteer_enqueue_dynamic_js' ) ) {

	function volunteer_enqueue_dynamic_js () {
	
		ob_start();
		include_once VOLUNTEER_INCLUDES . '/theme-dynamic-js.php';
		$theme_dynamic_js = ob_get_contents();
		ob_get_clean();
		
		if( isset( $theme_dynamic_js ) && $theme_dynamic_js != '' ) {
			echo '<script type="text/javascript">'. $theme_dynamic_js . '</script>';
		}
		
	} // End volunteer_enqueue_dynamic_js()
}

/* =================================================================
 * Add [year] shortcode to display current year on copyright bar
 * ================================================================= */
 
if ( ! function_exists( 'volunteer_year_shortcode' ) ) { 
 
	function volunteer_year_shortcode() {
		$year = date('Y');
		return $year;
	}

}
add_shortcode('year', 'volunteer_year_shortcode');

/* =================================================================
 * Custom Excerpt Length used on archive/category and blog pages
 * ================================================================= */
 
function volunteer_custom_excerpt_length( $limit ) {	
	return '30';	
}

function volunteer_custom_excerpt_more( $more ) {
	return '...';
}

function volunteer_custom_excerpts($limit) {
    return wp_trim_words(get_the_excerpt(), $limit, '...');
}

/* =================================================================
 * Store Ajax admin url in head to call wherever need
 * ================================================================= */
if ( ! function_exists( 'volunteer_ajaxurl' ) ) { 

	function volunteer_ajaxurl() { ?>
	
<script type="text/javascript">
var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
</script>

	<?php } // End volunteer_ajaxurl()
}

/* =================================================================
 * Excerpt with allow some tags
 * ================================================================= */

function wpse_allowedtags() {
    // Add custom tags to this string
    return '<script>,<style>,<strong>,<br>,<em>,<i>,<ul>,<ol>,<li>,<a>,<p>,<img>,<video>,<audio>,<h1>,<h2>,<h3>,<h4>,<h5>,<h6>,<blockquote>,<table>,<thead>,<tbody>,<th>,<tr>,<td>,<address>,<pre>,<code>,<span>,<div>,<source>,<button>,<dl>,<dt>,<dd>,<figure>,<figcaption>';
}

if ( ! function_exists( 'volunteer_custom_wp_trim_excerpt' ) ) {

    function volunteer_custom_wp_trim_excerpt($wpse_excerpt, $limit) {
		global $volunteer_options, $post;
				
    	$raw_excerpt = $wpse_excerpt;
        if( '' == $wpse_excerpt ) {
		
			if( isset( $limit ) && $limit == '' ) {
				$limit = 168;
			}
		
			$post = get_post(get_the_ID());
			$pos = strpos($post->post_content, '<!--more-->');
			
			$readmore_link = '';
			
			$readmore_link = ' <span class="meta-link-more">&#91;...&#93;</span>';						

            $wpse_excerpt = get_the_content( $readmore_link );
			
			if( $post->post_excerpt || $pos !== false ) {
				if( ! $pos ) {
					$wpse_excerpt = rtrim( get_the_excerpt(), '[&hellip;]' );
				}				
			}
						
            $wpse_excerpt = apply_filters('the_content', $wpse_excerpt);
            $wpse_excerpt = str_replace(']]>', ']]&gt;', $wpse_excerpt);
            $wpse_excerpt = strip_tags($wpse_excerpt, wpse_allowedtags()); /*IF you need to allow just certain tags. Delete if all tags are allowed */

            //Set the excerpt word count and only break after sentence is complete.
			$excerpt_word_count = $limit;
			//$excerpt_length = apply_filters('volunteer_custom_excerpt_length', $excerpt_word_count); 
			$tokens = array();
			$excerptOutput = '';
			$count = 0;

			// Divide the string into tokens; HTML tags, or words, followed by any whitespace
			preg_match_all('/(<[^>]+>|[^<>\s]+)\s*/u', $wpse_excerpt, $tokens);

			foreach ($tokens[0] as $token) { 

				if ($count >= $excerpt_word_count && preg_match('/[\,\:\;\?\.\!]\s*$/uS', $token)) { 
					// Limit reached, continue until , : ; ? . or ! occur at the end
					$excerptOutput .= trim($token);
					break;
				}

				// Add words to complete sentence
				$count++;

				// Append what's left of the token
				$excerptOutput .= $token;
			}

            $wpse_excerpt = trim(force_balance_tags($excerptOutput));
			
			$wpse_excerpt = do_shortcode( $wpse_excerpt );
             
            return $wpse_excerpt;

        }
		
        return apply_filters('volunteer_custom_wp_trim_excerpt', $wpse_excerpt, $raw_excerpt);
    }

}

/* =================================================================
 * Charitable Plugin Filters
 * ================================================================= */
if( class_exists( 'Charitable' ) ) {
	
	remove_action( 'charitable_campaign_content_loop_after', 'charitable_template_campaign_progress_bar', 6 );
	remove_action( 'charitable_campaign_content_loop_after', 'charitable_template_campaign_loop_donation_stats', 8 );
	
	remove_action( 'charitable_campaign_content_loop_after', 'charitable_template_campaign_loop_donate_link', 10, 2 );
	add_action( 'charitable_campaign_content_loop_donate_link', 'charitable_template_campaign_loop_donate_link', 10, 2 );
		
	add_action( 'charitable_campaign_content_loop_after', 'charitable_template_campaign_loop_donation_stats', 6 );
	add_action( 'charitable_campaign_content_loop_after', 'charitable_template_campaign_progress_bar', 8 );	

	add_filter( 'charitable_donation_summary', 'volunteer_charitable_donation_summary', 10, 2 );
	
	function volunteer_charitable_donation_summary( $output, $campaign ) {
		$currency_helper = charitable_get_currency_helper();
		
		if ( $campaign->has_goal() ) {
			$ret = sprintf( _x( '%s %s / %s', 'amount donated of goal', 'volunteer' ), 
				'<span class="donation-status">' . esc_html__( 'Donation:', 'volunteer' ) . '</span>', 
				'<span class="amount">' . $currency_helper->get_monetary_amount( $campaign->get_donated_amount() ) . '</span>', 
				'<span class="goal-amount">' . $currency_helper->get_monetary_amount( $campaign->get( 'goal' ) ) . '</span>'
			);
		}
		else {
			$ret = sprintf( _x( '%s %s', 'amount donated', 'volunteer' ), 
				'<span class="donation-status">' . esc_html__( 'Donated:', 'volunteer' ) . '</span>', 
				'<span class="amount">' . $currency_helper->get_monetary_amount( $campaign->get_donated_amount() ) . '</span>'
			);
		}
			
		$output = $ret;
		
		return $output;
	}
	
	add_filter( 'the_content', 'volunteer_charitable_remove_filters', 5 );

	function volunteer_charitable_remove_filters( $content ) {
		if( ! is_singular('campaign') ) {
			remove_filter( 'the_content', 'charitable_template_donation_form_content' );
			remove_filter( 'the_content', 'charitable_template_donation_receipt_content');
		}
		return $content;
	}
	
	add_filter( 'charitable_required_field_exists', 'volunteer_charitable_required_field_exists', 10, 5 );
	
	function volunteer_charitable_required_field_exists( $exists, $key, $field, $submitted, $object ) {
		
		$exists = ! empty( $submitted[ $key ] );

		/* Verify that a value was provided. */
		if ( $exists ) {
			$exists = ! empty( $exists ) || ( is_string( $exists ) && strlen( $exists ) );
		}

		/* If a value was not provided, check if it's in the $_FILES array. */
		if ( ! $exists ) {
			$exists = ( 'picture' == $field[ 'type' ] && isset( $_FILES[ $key ] ) && ! empty( $_FILES[ $key ][ 'name' ] ) );
		}
				
		return $exists;
	
	}
	
}