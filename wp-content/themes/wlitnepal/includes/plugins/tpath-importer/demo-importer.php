<?php
/**
 * Version 0.0.2
 *
 */

require_once get_template_directory() . '/includes/plugins/tpath-importer/importer/tpath-importer.php' ; //load admin theme data importer

class Volunteer_Demo_Data_Importer extends Volunteer_Theme_Importer {

	/**
	 * Holds a copy of the object for easy reference.
	 *
	 * @since 0.0.1
	 *
	 * @var object
	 */
	private static $instance;

	/**
	 * Set the key to be used to store theme options
	 *
	 * @since 0.0.2
	 *
	 * @var object
	 */
	public $theme_option_name       = 'volunteer_options'; //set theme options name here
	public $theme_options_file_name = 'theme_options.txt';
	public $widgets_file_name       = 'widgets.json';
	public $content_demo_file_name  = 'content.xml';

	/**
	 * Holds a copy of the widget settings
	 *
	 * @since 0.0.2
	 *
	 * @var object
	 */
	public $widget_import_results;

	/**
	 * Constructor. Hooks all interactions to initialize the class.
	 *
	 * @since 0.0.1
	 */
	public function __construct() {

		$this->demo_files_path = get_template_directory() . '/includes/plugins/tpath-importer/demo-files/';

		self::$instance = $this;
		parent::__construct();

	}

	/**
	 * Add menus
	 *
	 * @since 0.0.1
	 */
	public function set_demo_menus(){

		// Menus to Import and assign - you can remove or add as many as you want
		$top_menu    = get_term_by('name', 'Top Menu', 'nav_menu');
		$main_menu   = get_term_by('name', 'Main Menu', 'nav_menu');

		set_theme_mod( 'nav_menu_locations', array(
				'top-menu' 		=> $top_menu->term_id,
				'primary-menu' 	=> $main_menu->term_id,
			)
		);
	}
}

new Volunteer_Demo_Data_Importer;