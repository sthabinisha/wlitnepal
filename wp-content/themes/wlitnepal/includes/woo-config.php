<?php
/**
* Woocommerce Config
*
* @package TemplatePath
*/

// Exit if accessed directly
if( ! defined( 'ABSPATH' ) ) {
    die;
}

if( ! class_exists( 'VolunteerWooConfig' ) ) {
	
    class VolunteerWooConfig {

    	function __construct() {

    		add_filter( 'woocommerce_show_page_title', array( $this, 'volunteer_woo_shop_title'), 10 );
			        
    		remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
    		remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );
			// Add Woocommerce Shop Title with Divider
    		add_action( 'woocommerce_before_main_content', array( $this, 'volunteer_woo_before_container' ), 10 );
    		add_action( 'woocommerce_after_main_content', array( $this, 'volunteer_woo_after_container' ), 10 );			
						
			// Remove Woocommerce Default Sidebar
			remove_action( 'woocommerce_sidebar' , 'woocommerce_get_sidebar', 10 );
			
			// Products Loop
    		add_action( 'woocommerce_after_shop_loop_item', array( $this, 'volunteer_woo_before_shop_loop_item' ), 9 );
    		add_action( 'woocommerce_after_shop_loop_item', array( $this, 'volunteer_woo_after_shop_loop_item' ), 11 );

    	}
		
		function volunteer_woo_before_container() {
			global $volunteer_options, $post;
			
			$shop_page_id = $layout = $content_class = $primary_class = '';
			
			if( is_shop() ) {
				$shop_page_id = get_option('woocommerce_shop_page_id');
				if( ! empty( $shop_page_id ) ) {
					$layout = get_post_meta( $shop_page_id, 'volunteer_layout', true );
				}
				if( isset( $layout ) && $layout == '' ) {
					$layout = 'one-col';
				}
			} 
			
			if( is_product_category() || is_product_tag() ) {
				$layout = $volunteer_options['woo_archive_layout'];
				if( isset( $layout ) && $layout == '' ) {
					$layout = 'one-col';
				}
			}
			
			if( is_product() ) {
				$layout = $volunteer_options['woo_single_layout'];
				if( isset( $layout ) && $layout == '' ) {
					$layout = 'two-col-right';
				}
			}
			
			if( $layout == 'two-col-left' || $layout == 'two-col-right' ) {
				$primary_class = 'content-col-small';
			}
			elseif( $layout == 'one-col' ) {
				$primary_class = 'content-col-full';
			}

			echo '<div class="container tpath-woocommerce-wrapper">
				<div id="main-wrapper" class="tpath-row row">
					<div id="single-sidebar-container" class="single-sidebar-container main-col-full">
						<div class="tpath-row row">
							<div id="primary" class="content-area '.$primary_class.'">
								<div id="content" class="site-content">';
		}
		
		function volunteer_woo_after_container() {
			global $volunteer_options, $post;
			
			echo '</div>
				</div>';
			
			$shop_page_id = $layout = $layouts = $pm_sidebar_widget = '';
			
			if( is_shop() ) {
				$shop_page_id = get_option('woocommerce_shop_page_id');
				if( ! empty( $shop_page_id ) ) {
					$layout = get_post_meta( $shop_page_id, 'volunteer_layout', true );
				}
				if( isset( $layout ) && $layout == '' ) {
					$layout = 'one-col';
				}
				$pm_sidebar_widget = get_post_meta( $post->ID, 'volunteer_primary_sidebar', true );
			} 
			
			if( is_product_category() || is_product_tag() ) {
				$layout = $volunteer_options['woo_archive_layout'];
				if( isset( $layout ) && $layout == '' ) {
					$layout = 'one-col';
				}
			}
			
			if( is_product() ) {
				$layout = $volunteer_options['woo_single_layout'];
				if( isset( $layout ) && $layout == '' ) {
					$layout = 'two-col-right';
				}
			}
			
			if( $pm_sidebar_widget == '' || $pm_sidebar_widget == '0' ) {
				$pm_sidebar_widget = 'shop-sidebar';
			}
			
			if( $layout != 'one-col' ) {
				if ( is_active_sidebar( $pm_sidebar_widget ) ) {				
					echo '<div id="sidebar" class="primary-sidebar sidebar pm-sidebar">';
						dynamic_sidebar( $pm_sidebar_widget );
					echo '</div>';
				}
			}
			
				echo '</div>
					</div>
				</div>
			</div>';
		}
				
		function volunteer_woo_shop_title() {
			return false;
		}
		
		function volunteer_woo_before_shop_loop_item() {
			echo '<div class="product-buttons-container"><div class="product-buttons">';
		}

		function volunteer_woo_after_shop_loop_item() {
			echo '<a href="' . get_permalink() . '" class="woo-show-details" title="' . esc_html__( 'Show Details', 'volunteer' ) . '">' . esc_html__( 'Show Details', 'volunteer' ) . '</a></div></div>';
		}
		
	}
}
new VolunteerWooConfig();

global $volunteer_options;

// Remove Breadcrumbs
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );

// Remove result count
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );

// Move Rating After Price
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 5 );
add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 10 );

// Change number of products per page
add_filter('loop_shop_per_page', 'volunteer_woo_loop_shop_per_page');

function volunteer_woo_loop_shop_per_page() {
	global $volunteer_options;
	
	parse_str($_SERVER['QUERY_STRING'], $params);

	if( isset( $volunteer_options['loop_products_per_page'] ) && $volunteer_options['loop_products_per_page'] != '' ) {
		$products_per_page = $volunteer_options['loop_products_per_page'];
	} else {
		$products_per_page = 12;
	}
	
	$product_count = !empty($params['product_count']) ? $params['product_count'] : $products_per_page;

	return $product_count;
}

// Change number of products per row to 4
add_filter('loop_shop_columns', 'volunteer_woo_loop_columns');

function volunteer_woo_loop_columns() {

	global $volunteer_options;
	
	if( isset( $volunteer_options['loop_shop_columns'] ) && $volunteer_options['loop_shop_columns'] != '' ) {
		$product_columns = $volunteer_options['loop_shop_columns'];
	} else {
		$product_columns = 3;
	}
	
	return $product_columns;
}

// Related Products Count
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
add_action('woocommerce_after_single_product_summary', 'volunteer_woocommerce_output_related_products', 15);

function volunteer_woocommerce_output_related_products() {
	global $volunteer_options;
	
	if( isset( $volunteer_options['related_products_count'] ) && $volunteer_options['related_products_count'] != '' ) {
		$related_count = $volunteer_options['related_products_count'];
	} else {
		$related_count = 3;
	}
	
	$args = array(
		'posts_per_page' => $related_count,
		'columns' => $related_count,
		'orderby' => 'rand'
	);

	woocommerce_related_products( apply_filters( 'woocommerce_output_related_products_args', $args ) );
}

// Ajax Add to Cart Update
add_filter( 'add_to_cart_fragments', 'volunteer_woocommerce_add_to_cart_fragment' );
function volunteer_woocommerce_add_to_cart_fragment( $fragments ) {
	global $woocommerce;

	ob_start();
	?>
	<div class="woo-header-cart">
		<?php if( ! $woocommerce->cart->cart_contents_count ) { ?>
		<a class="cart-icon cart-empty cart-contents" href="<?php echo get_permalink(get_option('woocommerce_cart_page_id')); ?>"><i class="fa fa-shopping-cart"></i></a>
		<?php } else { ?>
		<a class="cart-icon cart-contents" href="<?php echo get_permalink(get_option('woocommerce_cart_page_id')); ?>"><i class="fa fa-shopping-cart"></i><span class="cart-count"><?php echo esc_attr( $woocommerce->cart->cart_contents_count ); ?></span></a>
		
		<div class="woo-cart-contents">			
			<?php foreach( $woocommerce->cart->cart_contents as $cart_item_key => $cart_item ) { ?>
				<div class="woo-cart-item clearfix">
					<a href="<?php echo get_permalink($cart_item['product_id']); ?>" title="<?php echo esc_html( $cart_item['data']->post->post_title ); ?>">
						<?php $thumbnail_id = ($cart_item['variation_id']) ? $cart_item['variation_id'] : $cart_item['product_id']; ?>
						<?php echo get_the_post_thumbnail($thumbnail_id, 'thumbnail'); ?>
						<div class="cart-item-content">
							<h5 class="cart-product-name"><?php echo wp_kses_post( $cart_item['data']->post->post_title ); ?></h5>
							<h5 class="cart-product-quantity"><?php echo wp_kses_post( $cart_item['quantity'] ); ?> x <?php echo wp_kses_post( $woocommerce->cart->get_product_subtotal($cart_item['data'], 1) ); ?></h5>
						</div>
					</a>					
					<?php echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf('<a href="%s" class="remove remove-cart-item" title="%s" data-cart_id="%s">&times;</a>', esc_url( WC()->cart->get_remove_url( $cart_item_key ) ), esc_html__('Remove this item', 'volunteer'), $cart_item_key ), $cart_item_key ); ?>
                    <div class="ajax-loading"></div>
				</div>
			<?php } ?>
			
			<div class="woo-cart-total clearfix">
				<h5 class="cart-total"><?php esc_html_e('Total: ', 'volunteer'); ?> <?php echo wp_kses_post( $woocommerce->cart->get_cart_total() ); ?></h5>
			</div>
					
			<div class="woo-cart-buttons clearfix">
				<div class="cart-button"><a href="<?php echo get_permalink(get_option('woocommerce_cart_page_id')); ?>" title="<?php esc_html_e('Cart', 'volunteer'); ?>"><?php esc_html_e('View Cart', 'volunteer'); ?></a></div>
				<div class="checkout-button"><a href="<?php echo get_permalink(get_option('woocommerce_checkout_page_id')); ?>" title="<?php esc_html_e('Checkout', 'volunteer'); ?>"><?php esc_html_e('Checkout', 'volunteer'); ?></a></div>
			</div>
		</div>
		<?php } ?>
	</div>
	
	<?php $fragments['.header-main-cart .woo-header-cart'] = ob_get_clean();
	
	return $fragments;

}

// Ajax Remove Item
add_action( 'wp_ajax_volunteer_product_remove', 'volunteer_ajax_product_remove' );
add_action( 'wp_ajax_nopriv_volunteer_product_remove', 'volunteer_ajax_product_remove' );
function volunteer_ajax_product_remove() {

    $cart = WC()->instance()->cart;
    $cart_id = $_POST['cart_id'];
    $cart_item_id = $cart->find_product_in_cart($cart_id);

    if ($cart_item_id) {
        $cart->set_quantity($cart_item_id, 0);
    }

    $cart_ajax = new WC_AJAX();
    $cart_ajax->get_refreshed_fragments();

    exit();
}

// Shop Ordering
if( isset( $volunteer_options['woo_shop_ordering'] ) && ! $volunteer_options['woo_shop_ordering'] ) {
	remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
	add_action('woocommerce_before_shop_loop', 'volunteer_woocommerce_catalog_ordering', 30);
	add_action('woocommerce_get_catalog_ordering_args', 'volunteer_woocommerce_overwrite_catalog_ordering', 20);
}

function volunteer_woocommerce_catalog_ordering() {

	global $wp_query, $volunteer_options;
	
	$product_order = array();
	$product_sort = array();
	
	$product_order['default'] 	 = esc_html__('Default Order', 'volunteer');
	$product_order['title'] 	 = esc_html__('Name', 'volunteer');
	$product_order['price'] 	 = esc_html__('Price', 'volunteer');
	$product_order['date'] 		 = esc_html__('Date', 'volunteer');
	$product_order['popularity'] = esc_html__('Popularity', 'volunteer');

	$product_sort['asc'] 		 = esc_html__('Products ascending',  'volunteer');
	$product_sort['desc'] 		 = esc_html__('Products descending',  'volunteer');
	
	// Set the products per page options
	if( $volunteer_options['loop_products_per_page'] ) {
		$per_page = $volunteer_options['loop_products_per_page'];
	} else {
		$per_page = 12;
	}
	
	parse_str($_SERVER['QUERY_STRING'], $params);

	$product_order_key = !empty($params['product_order']) ? $params['product_order'] : 'default';
	$product_sort_key  = !empty($params['product_sort'])  ? $params['product_sort'] : 'asc';
	$product_count_key = !empty($params['product_count']) ? $params['product_count'] : $per_page;

	$product_sort_key = strtolower($product_sort_key);
	
	$output = '';
	
	$output .= '<div class="woo-catalog-ordering clearfix">';
		
		// Products Orderby
		$output .= '<div class="tpath-woo-orderby-container">';
		$output .= '<ul class="orderby-dropdown woo-ordering woo-dropdown">';
			$output .= '<li>';
				$output .= '<span class="current-li"><span class="current-li-content">'.esc_html__('Sort by: ', 'volunteer').' <strong>'.$product_order[$product_order_key].'</strong></span></span>';
				$output .= '<ul class="order-sub-dropdown">';
					// Default
					$output .= '<li class="'.(($product_order_key == 'default') ? 'current': '').'"><a href="'.volunteer_woo_build_query_string($params, 'product_order', 'default').'"><strong>'.$product_order['default'].'</strong></a></li>';
					// Title
					$output .= '<li class="'.(($product_order_key == 'title') ? 'current': '').'"><a href="'.volunteer_woo_build_query_string($params, 'product_order', 'title').'"><strong>'.$product_order['title'].'</strong></a></li>';
					// Price
					$output .= '<li class="'.(($product_order_key == 'price') ? 'current': '').'"><a href="'.volunteer_woo_build_query_string($params, 'product_order', 'price').'"><strong>'.$product_order['price'].'</strong></a></li>';
					// Date
					$output .= '<li class="'.(($product_order_key == 'date') ? 'current': '').'"><a href="'.volunteer_woo_build_query_string($params, 'product_order', 'date').'"><strong>'.$product_order['date'].'</strong></a></li>';
					// Popularity
					$output .= '<li class="'.(($product_order_key == 'popularity') ? 'current': '').'"><a href="'.volunteer_woo_build_query_string($params, 'product_order', 'popularity').'"><strong>'.$product_order['popularity'].'</strong></a></li>';
				$output .= '</ul>';
			$output .= '</li>';
		$output .= '</ul>';
		$output .= '</div>';
		
		// Products Sorting
		$output .= '<div class="tpath-woo-sorting-container">';
		$output .= '<ul class="sorting-dropdown woo-sort-ordering">';
			if($product_sort_key == 'desc') {			
				$output .= '<li class="sort-desc"><a title="'.$product_sort['asc'].'" href="'.volunteer_woo_build_query_string($params, 'product_sort', 'asc').'"><i class="fa fa-arrow-down"></i></a></li>';
			}
			
			if($product_sort_key == 'asc') {			
				$output .= '<li class="sort-asc"><a title="'.$product_sort['desc'].'" href="'.volunteer_woo_build_query_string($params, 'product_sort', 'desc').'"><i class="fa fa-arrow-up"></i></a></li>';
			}
		$output .= '</ul>';
		$output .= '</div>';
		
		// Products Count
		$output .= '<div class="tpath-woo-count-container">';
		$output .= '<ul class="count-dropdown woo-product-count woo-dropdown">';
		
			$output .= '<li>';
				$output .= '<span class="current-li"><span class="current-li-content">'.esc_html__('Show: ', 'volunteer').' <strong>'.$product_count_key.' '.esc_html__(' Products', 'volunteer').'</strong></span></span>';
				$output .= '<ul class="order-sub-dropdown">';
					$output .= '<li class="'.(($product_count_key == $per_page) ? 'current': '').'"><a href="'.volunteer_woo_build_query_string($params, 'product_count', $per_page).'"><strong>'.$per_page.' '.esc_html__(' Products', 'volunteer').'</strong></a></li>';
					
					$output .= '<li class="'.(($product_count_key == $per_page * 2) ? 'current': '').'"><a href="'.volunteer_woo_build_query_string($params, 'product_count', $per_page * 2).'"><strong>'.( $per_page * 2 ).' '.esc_html__(' Products', 'volunteer').'</strong></a></li>';
					
					$output .= '<li class="'.(($product_count_key == $per_page * 3) ? 'current': '').'"><a href="'.volunteer_woo_build_query_string($params, 'product_count', $per_page * 3).'"><strong>'.( $per_page * 3 ).' '.esc_html__(' Products', 'volunteer').'</strong></a></li>';
					
				$output .= '</ul>';
			$output .= '</li>';
		$output .= '</ul>';
		$output .= '</div>';
			
	$output .= '</div>';
	
	echo wp_kses_post( $output );
}

// Function to overwrite default query parameters
if( !function_exists('volunteer_woocommerce_overwrite_catalog_ordering') ) {	

	function volunteer_woocommerce_overwrite_catalog_ordering($args) {
			
		global $woocommerce;

		// Check parameters and session vars. if they are set overwrite the defaults
		$check = array('product_order', 'product_count', 'product_sort');		

		foreach($check as $key) {
			if(isset($_GET[$key]) ) $_SESSION['volunteer_woocommerce'][$key] = esc_attr($_GET[$key]);			
		}

		// is user wants to use new product order remove the old sorting parameter
		if(isset($_GET['product_order']) && !isset($_GET['product_sort']) && isset($_SESSION['volunteer_woocommerce']['product_sort'])) {
			unset($_SESSION['volunteer_woocommerce']['product_sort']);
		}
		
		parse_str($_SERVER['QUERY_STRING'], $params);
		
		$product_order = !empty($params['product_order']) ? $params['product_order'] : 'default';
		$product_sort = !empty($params['product_sort'])  ? $params['product_sort'] : 'asc';

		// Product order
		if(!empty($product_order)) {
			switch( $product_order ) {
				case 'date': 
					$orderby = 'date'; 
					$order = 'desc'; 
					$meta_key = '';  
					break;
				case 'price': 
					$orderby = 'meta_value_num'; 
					$order = 'asc'; 
					$meta_key = '_price'; 
					break;
				case 'popularity': 
					$orderby = 'meta_value_num'; 
					$order = 'desc'; 
					$meta_key = 'total_sales'; 
					break;
				case 'title': 
					$orderby = 'title'; 
					$order = 'asc'; 
					$meta_key = ''; 
					break;
				case 'default':
				default: 
					$orderby = 'menu_order title'; 
					$order = 'asc'; 
					$meta_key = ''; 
					break;
			}
		}

		// Product sorting
		if(!empty($product_sort))
		{
			switch( $product_sort ) {
				case 'desc': 
					$order = 'desc'; 
					break;
				case 'asc': 
					$order = 'asc'; 
					break;
				default: 
					$order = 'asc'; 
					break;
			}
		}


		if(isset($orderby)) $args['orderby'] = $orderby;
		if(isset($order)) 	$args['order'] = $order;
		
		if (!empty($meta_key)) {
			$args['meta_key'] = $meta_key;
		}
				
		return $args;
	}
}