<?php
/**
 * The template for displaying the footer.
 *
 * @package TemplatePath
 */
 
 global $volunteer_options;
?>
	</div><!-- #main -->
	
	<div id="footer" class="footer-section">	
		<?php $is_footer_widgets = $volunteer_options['footer_widgets_enable'];	
		if( isset( $is_footer_widgets ) && $is_footer_widgets ) { ?>
			<div id="footer-widgets-container" class="footer-widgets-section">
				<div class="container">
					<div class="tpath-row row">
						<?php
							$columns = $volunteer_options['footer_widget_layout'];
							if( ! $columns ) $columns = 4;
							for( $i = 1; $i <= intval( $columns ); $i++ ) { 
								if( is_active_sidebar( 'footer-widget-' . $i ) ) { ?>
								<div id="footer-widgets-<?php echo esc_attr( $i ); ?>" class="footer-widgets <?php footer_widget_classes( $columns ); ?>">
									<?php dynamic_sidebar( 'footer-widget-' . $i ); ?>
								</div>
								<?php }	
							} ?>
					</div><!-- .row -->
				</div>
			</div><!-- #footer-widgets-container -->
		<?php } ?>
		<div id="footer-copyright-container" class="footer-copyright-section">
			<div class="container">
				<div class="tpath-row">					
					<div id="copyright-text" class="copyright-info">
						<?php if( isset( $volunteer_options['copyright_text'] ) && $volunteer_options['copyright_text'] != '' ) {
							echo '<p>'.do_shortcode( esc_html( $volunteer_options['copyright_text'] ) ).'</p>';
						} else { ?>
							<p><?php esc_html_e('Copyright', 'volunteer'); ?> <?php echo date('Y'); ?> <?php esc_html_e('by', 'volunteer'); ?> <a href="<?php echo esc_url( home_url() ); ?>"><?php echo bloginfo( 'name' ); ?></a> | <?php esc_html_e('All rights reserved', 'volunteer'); ?></p>
						<?php } ?>
					</div><!-- #copyright-text -->
					<?php if( $volunteer_options['enable_social_icons_footer'] ) { ?>
					<div id="social-icons" class="footer-social">
						<?php echo volunteer_display_social_icons(); ?>
					</div><!-- #social-icons -->
					<?php } ?>
					
				</div>
			</div>
		</div><!-- #footer-copyright-container -->		
	</div><!-- #footer -->
</div>
<?php wp_footer(); ?>

</body>
</html>