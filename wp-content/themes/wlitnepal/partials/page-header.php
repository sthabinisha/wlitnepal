<?php global $post, $volunteer_options;
$object_id = get_queried_object_id();

if( ( get_option('show_on_front') && get_option('page_for_posts') && is_home() ) || 
( get_option('page_for_posts') && is_archive() && ! is_post_type_archive() ) && 
!( is_tax('product_cat') || is_tax('product_tag') ) || 
( get_option('page_for_posts') && is_search() ) ) {

	$post_id = get_option('page_for_posts');		
} else {
	if( isset($object_id) ) {
		$post_id = $object_id;
	}

	if( class_exists('Woocommerce') ) {
		if( is_shop() ) {
			$post_id = get_option('woocommerce_shop_page_id');
		}
		
		if ( ! is_singular() && ! is_shop() ) {
			$post_id = false;
		}
	} else {
		if( ! is_singular() ) {
			$post_id = false;
		}
	}
}

$hide_title_bar 	= get_post_meta( $post_id, 'volunteer_hide_page_title_bar', true );
$show_title 		= get_post_meta( $post_id, 'volunteer_show_page_title', true );
$show_breadcrumbs 	= get_post_meta( $post_id, 'volunteer_show_breadcrumbs', true );
$header_image 		= get_post_meta( $post_id, 'volunteer_page_header_image', true );
$custom_title		= get_post_meta( $post_id, 'volunteer_custom_page_title', true );

if( isset( $show_breadcrumbs ) && $show_breadcrumbs == '' ) {	
	$show_breadcrumbs = 'yes';
}

if( class_exists('Woocommerce') ) {
	if( is_product_category() || is_product_tag() ) {
		$header_image = $volunteer_options['woo_archive_image'];
	}
}	

$extra_style = '';
if( isset($header_image) && $header_image != '' ) {
	$extra_style = " style='background-image: url(". esc_url( $header_image ) .");'";
}

$title = '';
$title = get_the_title( $post_id );

if( is_home() ) {
	$title = $volunteer_options['blog_title'];
}

if( is_search() ) {
	$title = esc_html__( 'Search results for:', 'volunteer' ) . ' ' . get_search_query();
}

if( is_404() ) {
	$title = esc_html__('Error 404 Page', 'volunteer');
}

if( ( class_exists( 'TribeEvents' ) && tribe_is_event() && ! is_single() && ! is_home() ) ||
	( class_exists( 'TribeEvents' ) && is_events_archive() ) ||
	( class_exists( 'TribeEvents' ) && is_events_archive() && is_404() ) ) { 
	$title = tribe_get_events_title();	
}

if( is_archive() && ! ( class_exists('bbPress') && is_bbpress() ) && ! is_search() ) {
	if ( is_day() ) {
		$title = esc_html__( 'Daily Archives:', 'volunteer' ) . '<span> ' . get_the_date() . '</span>';
	} else if ( is_month() ) {
		$title = esc_html__( 'Monthly Archives:', 'volunteer' ) . '<span> ' . get_the_date( _x( 'F Y', 'monthly archives date format', 'volunteer' ) ) . '</span>';
	} elseif ( is_year() ) {
		$title = esc_html__( 'Yearly Archives:', 'volunteer' ) . '<span> ' . get_the_date( _x( 'Y', 'yearly archives date format', 'volunteer' ) ) . '</span>';
	} elseif ( is_author() ) {
		$current_auth = get_user_by( 'id', get_query_var( 'author' ) );
		$title = $current_auth->nickname;
	} elseif( is_post_type_archive() ) {
		$title = post_type_archive_title( '', false );
		
		$sermon_settings = get_option('wpfc_options');
		if( is_array( $sermon_settings ) ) {
			$title = $sermon_settings['archive_title'];
		}				
	} else {
		$title = single_cat_title( '', false );
	}
}

if( class_exists('Woocommerce') && is_woocommerce() && ( is_product() || is_shop() ) && ! is_search() ) {
	if( ! is_product() ) {
		$title = woocommerce_page_title( false );
	}
}

if( ! is_archive() && ! is_search() && ! ( is_home() && ! is_front_page() ) ) {
	if ( $hide_title_bar != 'yes' ) {
		if( $show_title == 'no' ) {
			$title = '';
		}
	}
} 
else {
	if ( $hide_title_bar != 'yes' ) {
		if( $show_title == 'no' ) {
			$title = '';
		}				
	}
}
		
$output_title_bar = '';
if( ! is_archive() && ! is_search() && ! ( is_home() && ! is_front_page() ) ) {
	if ( $hide_title_bar != 'yes' ) {
		if( is_home() && is_front_page() && ! $volunteer_options['blog_page_title_bar'] ) {
			$output_title_bar = 'no';
		} else {
			$output_title_bar = 'yes';
		}
	}
} else {

	if( is_home() && ! $volunteer_options['blog_page_title_bar'] ) {
		$output_title_bar = 'no';
	} else {	
		if( $hide_title_bar != 'yes' ) {
			$output_title_bar = 'yes';
		}
	}
}

if( isset( $output_title_bar ) && $output_title_bar == 'yes' ) { ?>
<div class="page-title-section"<?php echo esc_attr( $extra_style ); ?>>
	<div class="page-breadcrumbs-wrapper clearfix">
		<div class="container page-breadcrumbs-container">
			<?php if( isset( $show_breadcrumbs ) && $show_breadcrumbs == 'yes' ) { ?>
				<div class="page-breadcrumbs">
					<?php volunteer_breadcrumbs(); ?>
				</div>			
			<?php } ?>
		</div>
	</div>
	<?php if( isset( $show_title ) && $show_title != 'no' ) { ?>
		<div class="page-title-wrapper clearfix">
			<div class="container page-title-container">
				<div class="page-title-header">
					<?php if( isset( $custom_title ) && $custom_title != '' ) { 
						 echo sprintf( '<h2 class="entry-title">%s</h2>', $custom_title ); 
					} 
					else { 
						echo sprintf( '<h2 class="entry-title">%s</h2>', $title ); 
					} ?>
				</div>
			</div>
		</div>
	<?php } ?>
</div>
<?php } ?>