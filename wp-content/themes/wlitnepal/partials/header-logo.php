<?php global $volunteer_options;

$site_title = get_bloginfo( 'name' );
$site_url = home_url( '/' );
$site_description = get_bloginfo( 'description' );
 
$heading_tag = ( is_home() || is_front_page() ) ? 'h1' : 'div'; ?>

<!-- ==================== Toggle Icon ==================== -->
<div class="navbar-header nav-respons tpath-logo">
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".tpath-mainnavbar-collapse">
		<span class="sr-only"><?php esc_html_e('Toggle navigation', 'volunteer'); ?></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
	
	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?> - <?php bloginfo( 'description' ); ?>" rel="home">
		<?php if( isset( $volunteer_options['logo'] ) && $volunteer_options['logo'] != '' ) {
			echo '<img class="img-responsive" src="' . esc_url( $volunteer_options['logo'] ) . '" alt="' . esc_attr( get_bloginfo( 'name', 'display' ) ) . '" width="'. esc_attr( $volunteer_options['logo_width'] ) .'" height="'. esc_attr( $volunteer_options['logo_height'] ) .'" />';
		} elseif( isset($volunteer_options['logo_text']) && $volunteer_options['logo_text'] != '' ) { 
			echo '<div class="site-logo-text">'. esc_attr( $volunteer_options['logo_text'] ) .'</div>'; 
		} else {				
			?>
			<img src="<?php echo VOLUNTEER_THEME_URL; ?>/images/logo.png" class="img-responsive" alt="<?php the_title(); ?>" />	<?php
			
		} ?>
	</a>
</div>