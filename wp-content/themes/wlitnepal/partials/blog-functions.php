<?php 
/* Blog Functions */

if ( ! function_exists( 'volunteer_blog_featured_image' ) ) {
	function volunteer_blog_featured_image( $image_size = '', $post_format = '', $page_type_layout = '', $type = '' ) {
		global $volunteer_options, $post;
		
		$post_format_class = '';
		if( $post_format == 'image' ) {
			$post_format_class = ' image-format';
		} elseif( $post_format == 'quote' ) {
			$post_format_class = ' quote-image';
		}
		
		// Autoplay
		if( isset( $volunteer_options['blog_slideshow_autoplay'] ) && $volunteer_options['blog_slideshow_autoplay'] != '' ) { 
			$auto_play = esc_attr( $volunteer_options['blog_slideshow_autoplay'] ); 
		} else { 
			$auto_play = 'false'; 
		}
		// Autoplay timeout
		if( isset( $volunteer_options['blog_slideshow_autoplay_speed'] ) && $volunteer_options['blog_slideshow_autoplay_speed'] != '' ) { 
			$autoplay_timeout = esc_attr( $volunteer_options['blog_slideshow_autoplay_speed'] ); 
		} else { 
			$autoplay_timeout = '5000'; 
		}
		// Animation Out Type
		if( isset( $volunteer_options['blog_animation_out_type'] ) && $volunteer_options['blog_animation_out_type'] != '' ) { 
			$animate_out = esc_attr( $volunteer_options['blog_animation_out_type'] ); 
		} else { 
			$animate_out = 'fadeOut'; 
		}
		// Animation In Type
		if( isset( $volunteer_options['blog_animation_in_type'] ) && $volunteer_options['blog_animation_in_type'] != '' ) { 
			$animate_in = esc_attr( $volunteer_options['blog_animation_in_type'] ); 
		} else { 
			$animate_in = 'fadeIn'; 
		}
		// Smart Speed
		if( isset( $volunteer_options['blog_slideshow_speed'] ) && $volunteer_options['blog_slideshow_speed'] != '' ) { 
			$smart_speed = esc_attr( $volunteer_options['blog_slideshow_speed'] ); 
		} else { 
			$smart_speed = '450'; 
		}
		$data_attr = '';
		
		$data_attr .= ' data-autoplay="'. $auto_play .'" ';
		$data_attr .= ' data-autoplay-timeout="'. $autoplay_timeout .'" ';
		$data_attr .= ' data-animate-out="'. $animate_out .'" ';
		$data_attr .= ' data-animate-in="'. $animate_in .'" ';
		$data_attr .= ' data-smart-speed="'. $smart_speed .'" ';
		
		$link_url = '';
		$link_url = get_post_meta( $post->ID, 'volunteer_external_link_url', true );
	
		$output = '';
		
		$output .= '<div class="post-media">';
		if( $post_format == 'gallery' ) {
			$output .= '<div class="entry-thumbnail">';
			$output .= '<div class="owl-carousel blog-carousel-slider"'.$data_attr .'>';
			$output .= get_gallery_post_images( $image_size, $post->ID );
			$output .= '</div>';
			$output .= '</div>';
		} else {
			$output .= '<div class="entry-thumbnail'. esc_attr( $post_format_class ) .'">';
			if( isset( $type ) && $type != 'single' ) {
				if( $post_format == 'link' && isset( $link_url ) && $link_url != '' ) {
					$output .= '<a href="'. esc_url( $link_url ) .'" title="'. get_the_title() .'" target="_blank" class="post-img-overlay">';
				} else {
					$output .= '<a href="'. get_permalink() .'" title="'. get_the_title() .'" class="post-img-overlay">';
				}
			} else {
				if( $post_format == 'link' && isset( $link_url ) && $link_url != '' ) {
					$output .= '<a href="'. esc_url( $link_url ) .'" title="'. get_the_title() .'" target="_blank" class="post-img-overlay">';
				}
			}
			$output .= get_the_post_thumbnail( $post->ID, $image_size );
			
			if( isset( $type ) && $type != 'single' ) {
				$output .= '</a>';
			} else {
				if( $post_format == 'link' && isset( $link_url ) && $link_url != '' ) {
					$output .= '</a>';
				}
			}
			if( $post_format != 'link' && $page_type_layout == 'large' && ( isset( $type ) && $type != 'single' ) ) {
				$output .= '<div class="post-overlay-icons">';
					$output .= '<a class="blog-icon plus-icon img-circle" href="'. get_permalink() .'"><span class="flaticon flaticon-add30"></span></a>';
					$output .= '<a class="blog-icon heart-icon img-circle" href="'. get_permalink() .'#respond"><span class="flaticon flaticon-favorite21"></span></a>';
				$output .= '</div>';
			}
			$output .= '</div>';
		}
		$output .= '</div>';
		
		return $output;
	}
}

if ( ! function_exists( 'volunteer_blog_posted_info' ) ) { 
	function volunteer_blog_posted_info() {
		global $post;
		
		$posted_date = '';
		
		$posted_date .= '<div class="entry-post-date">';
		$posted_date .= '<span class="date">'. get_the_time('d') .'</span>';
		$posted_date .= '<span class="month">'. get_the_time('M') .'</span>';
		$posted_date .= '</div>';
		
		return $posted_date;
	}
}

if ( ! function_exists( 'volunteer_blog_title' ) ) { 
	function volunteer_blog_title() {
		global $post;
		
		$post_title = '';
		
		$post_format = get_post_format();
		$link_url = '';
		$link_url = get_post_meta( $post->ID, 'volunteer_external_link_url', true );
		
		$post_title .= '<h3 class="entry-title">';
		if( $post_format == 'link' && isset( $link_url ) && $link_url != '' ) {
			$post_title .= '<a href="'. esc_url( $link_url ) .'" rel="bookmark" target="_blank" title="'. get_the_title() .'">';
		} else {
			$post_title .= '<a href="'. get_permalink() .'" rel="bookmark" title="'. get_the_title() .'">';
		}
		$post_title .= get_the_title();
		$post_title .= '</a>';
		$post_title .= '</h3>';
		
		return $post_title;
	}
}

if ( ! function_exists( 'volunteer_blog_entry_meta' ) ) { 
	function volunteer_blog_entry_meta( $page_type_layout ) {
		global $volunteer_options, $post;
		
		$post_meta = '';
		
		$post_meta .= '<div class="entry-meta-list">';
		$post_meta .= '<ul class="entry-meta">';
		if( ! $volunteer_options['blog_post_meta_author'] ) {
			$post_meta .= '<li class="author"><i class="flaticon flaticon-user197"></i>';
			ob_start();
			the_author_posts_link();
			$post_meta .= ob_get_clean() . '</li>';
		}
		if( $page_type_layout == 'large' ) {
			if( ! $volunteer_options['blog_post_meta_categories'] && get_the_category_list(', ') != '' ) {
				$post_meta .= '<li class="category"><i class="flaticon flaticon-tag79"></i>'. get_the_category_list(', ') .'</li>';
			}
		}
		if( ! $volunteer_options['blog_post_meta_comments'] ) {
			if ( comments_open() ) {
				$post_meta .= '<li class="comments-link"><i class="flaticon flaticon-speechbubble65"></i>';
				ob_start();
				comments_popup_link( '<span class="leave-reply">' . esc_html__( '0 Comment', 'volunteer' ) . '</span>', esc_html__( '1 Comment', 'volunteer' ), esc_html__( '% Comments', 'volunteer' ) );
				$post_meta .= ob_get_clean();
				$post_meta .= '</li>';
			}
		}
		$post_meta .= '</ul>';
		$post_meta .= '</div>';
		
		return $post_meta;
	}
}

if ( ! function_exists( 'volunteer_blog_content' ) ) { 
	function volunteer_blog_content( $excerpt_limit, $type = '' ) {
		global $post;
		
		$post_content = '';
		$post_format = get_post_format();
		
		if( $post_format == 'quote' ) {			
			$post_content .= '<div class="entry-quotes quote-format">';
			$post_content .= '<blockquote>';
			if( isset( $type ) && $type == 'full_content' ) {
				$post_content .= apply_filters('the_content', get_the_content() );
			} else {
				$post_content .= volunteer_custom_wp_trim_excerpt('', $excerpt_limit);
			}
			$post_content .= '</blockquote>';
			$post_content .= '</div>';
			
		} else {
			if( isset( $type ) && $type == 'full_content' ) {
				$post_content .= apply_filters('the_content', get_the_content() );
			} else {
				$post_content .= volunteer_custom_wp_trim_excerpt('', $excerpt_limit);
			}
		}
		
		return $post_content;
	}
}

if ( ! function_exists( 'volunteer_blog_footer' ) ) { 
	function volunteer_blog_footer() {
		global $volunteer_options, $post;
		
		$post_footer = '';
		$post_format = get_post_format();
		$link_url = '';
		$link_url = get_post_meta( $post->ID, 'volunteer_external_link_url', true );
		
		$post_footer .= '<div class="read-more">';
		if( $post_format == 'link' && isset( $link_url ) && $link_url != '' ) {
			$post_footer .= '<a href="'. esc_url( $link_url ) .'" class="post-btn btn-more read-more-link" target="_blank" title="'. get_the_title() .'">';
		} else {
			$post_footer .= '<a href="'. get_permalink() .'" class="post-btn btn-more read-more-link" title="'. get_the_title() .'">';
		}
		$post_footer .= '<span class="blog-icon fa fa-angle-right"></span>';
		if( ! $volunteer_options['blog_read_more_text'] ) {
			$post_footer .= esc_html__('Read More', 'volunteer'); 
		} else {
			$post_footer .= esc_attr( $volunteer_options['blog_read_more_text'] );
		}
		$post_footer .= '</a>';
		$post_footer .= '</div>';
		
		return $post_footer;
	}
}