<?php
/**
 * Single Team Page
 *
 * @package TemplatePath
 */
 
global $volunteer_options;
get_header(); ?>

<div class="container">
	<div id="main-wrapper" class="tpath-row row">
		<div id="single-sidebar-container" class="single-sidebar-container main-col-full">
			<div class="tpath-row row">	
				<div id="primary" class="content-area <?php volunteer_primary_content_classes(); ?>">
					<div id="content" class="site-content">	
						<?php if ( have_posts() ):
							while ( have_posts() ): the_post();
							
							$member_role 			= get_post_meta( $post->ID, 'volunteer_member_role', true );
							$member_address 		= get_post_meta( $post->ID, 'volunteer_member_address', true );
							$member_phone 			= get_post_meta( $post->ID, 'volunteer_member_phone', true );
							$member_content_block 	= get_post_meta( $post->ID, 'volunteer_additional_content_block', true );
							
							$member_facebook 		= get_post_meta( $post->ID, 'volunteer_member_facebook', true );
							$member_twitter 		= get_post_meta( $post->ID, 'volunteer_member_twitter', true );
							$member_linkedin 		= get_post_meta( $post->ID, 'volunteer_member_linkedin', true );
							$member_pinterest 		= get_post_meta( $post->ID, 'volunteer_member_pinterest', true );
							$member_googleplus 		= get_post_meta( $post->ID, 'volunteer_member_googleplus', true );
							$member_dribbble 		= get_post_meta( $post->ID, 'volunteer_member_dribbble', true );
							$member_flickr 			= get_post_meta( $post->ID, 'volunteer_member_flickr', true );
							$member_yahoo 			= get_post_meta( $post->ID, 'volunteer_member_yahoo', true );
							$member_blog 			= get_post_meta( $post->ID, 'volunteer_member_blog', true );
							$member_email 			= get_post_meta( $post->ID, 'volunteer_member_email', true );
							
							$team_full_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
				
							<div class="team-single-wrapper">
								<div class="team-single-inner clearfix">
									
									<div <?php post_class(); ?> id="team-member-<?php the_ID(); ?>">
										<div class="entry-content">												
											<div class="row">
												<!-- ============ Team Content Wrapper ============ -->
												<div class="col-md-12 col-xs-12 team-content-wrapper">
													<div class="row">
														<div class="col-sm-4 col-xs-12 team-single-image">
															<div class="team-member-image">
																<a href="<?php echo esc_url( $team_full_img[0] ); ?>" data-rel="prettyPhoto" title="<?php the_title(); ?>"><img class="img-responsive" src="<?php echo esc_url( $team_full_img[0] ); ?>" alt="<?php the_title(); ?>" /></a>
															</div>
														</div><!-- .col-sm-4 -->
														<div class="col-sm-8 col-xs-12 team-member-info">
															<div class="team-member-details">
																<h2 class="team-member-name"><?php the_title(); ?></h2>
																<?php if( isset( $member_role ) && $member_role != '' ) { ?>
																<div class="team-member_role">
																	<i class="flaticon flaticon-award52"></i>
																	<span><?php esc_html_e( 'Role', 'volunteer' ); ?></span> : <?php echo esc_html( $member_role ); ?>
																</div>
																<?php } ?>
																<?php if( isset( $member_phone ) && $member_phone != '' ) { ?>
																<div class="team-member_phone">
																	<i class="flaticon flaticon-telephone114"></i>
																	<span><?php esc_html_e( 'Mobile', 'volunteer' ); ?></span> : <?php echo esc_html( $member_phone ); ?>
																</div>
																<?php } ?>
																<?php if( isset( $member_email ) && $member_email != '' ) { ?>
																<div class="team-member_email">
																	<i class="fa fa-envelope-o"></i>
																	<span><?php esc_html_e( 'Email', 'volunteer' ); ?></span> :<a href="mailto:<?php echo esc_html( $member_email ); ?>"> <?php echo esc_html( $member_email ); ?></a>
																</div>
																<?php } ?>
																																
																<!-- ============ Team Socials ============ -->
																<div class="team-member-social member-social">
																<ul class="tpath-member-social-icons list-inline">
																	<?php if( isset( $member_facebook ) && $member_facebook != '' ) { ?>
																	<li class="facebook"><a target="_blank" href="<?php echo esc_url($member_facebook); ?>"><i class="fa fa-facebook"></i></a></li>
																	<?php } ?>
																	<?php if( isset( $member_twitter ) && $member_twitter != '' ) { ?>
																	<li class="twitter"><a target="_blank" href="<?php echo esc_url($member_twitter); ?>"><i class="fa fa-twitter"></i></a></li>
																	<?php } ?>
																	<?php if( isset( $member_linkedin ) && $member_linkedin != '' ) { ?>
																	<li class="linkedin"><a target="_blank" href="<?php echo esc_url($member_linkedin); ?>"><i class="fa fa-linkedin"></i></a></li>
																	<?php } ?>
																	<?php if( isset( $member_pinterest ) && $member_pinterest != '' ) { ?>
																	<li class="pinterest"><a target="_blank" href="<?php echo esc_url($member_pinterest); ?>"><i class="fa fa-pinterest"></i></a></li>
																	<?php } ?>
																	<?php if( isset( $member_googleplus ) && $member_googleplus != '' ) { ?>
																	<li class="googleplus"><a target="_blank" href="<?php echo esc_url($member_googleplus); ?>"><i class="fa fa-google-plus"></i></a></li>
																	<?php } ?>
																	<?php if( isset( $member_dribbble ) && $member_dribbble != '' ) { ?>
																	<li class="dribbble"><a target="_blank" href="<?php echo esc_url($member_dribbble); ?>"><i class="fa fa-dribbble"></i></a></li>
																	<?php } ?>
																	<?php if( isset( $member_flickr ) && $member_flickr != '' ) { ?>
																	<li class="flickr"><a target="_blank" href="<?php echo esc_url($member_flickr); ?>"><i class="fa fa-flickr"></i></a></li>
																	<?php } ?>
																	<?php if( isset( $member_yahoo ) && $member_yahoo != '' ) { ?>
																	<li class="yahoo"><a target="_blank" href="<?php echo esc_url($member_yahoo); ?>"><i class="fa fa-yahoo"></i></a></li>
																	<?php } ?>
																	<?php if( isset( $member_blog ) && $member_blog != '' ) { ?>
																	<li class="blogger"><a target="_blank" href="<?php echo esc_url($member_blog); ?>"><i class="icon-blogger"></i></a></li>
																	<?php } ?>
																</ul>
																</div>																
															</div>
														</div><!-- .col-sm-8 -->
													</div>
													
													<div class="row">
														<div class="col-xs-12 team-member-content">
															<?php the_content(); ?>
														</div>
													</div>
													
												</div><!-- .col-md-8 -->
												
											<?php volunteer_related_team_members(); ?>
											
										</div>											
									</div>
									
								</div>
							</div><!-- .team-single-wrapper -->
							<?php endwhile;
								
							else : ?>
							<?php get_template_part( 'content', 'none' ); ?>
						<?php endif; ?>
					</div><!-- #content -->
				</div><!-- #primary -->
		
				<?php get_sidebar(); ?>	
			</div>
		</div><!-- #single-sidebar-container -->

	</div><!-- #main-wrapper -->
</div><!-- .container -->

<?php if( isset( $member_content_block ) && ( $member_content_block != '' && $member_content_block != 0 ) ) {
	echo volunteer_block( $member_content_block );
} ?>
<?php get_footer(); ?>