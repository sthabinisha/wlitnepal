<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wlitnepal' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '])R0:oV_d(4o>>GTKX,bk4^S:GV./0*XZd1Z [,25RFDj1VrN_`(ZE?+rNPhw0U@' );
define( 'SECURE_AUTH_KEY',  '?URc(KS&.1f8@@C08Nd3sEa8GNK2eXL8kik#w90B07C7ArBXL-cFJ/Mv(UJF/h0%' );
define( 'LOGGED_IN_KEY',    'r,M?qlP {hkIF$Y<%^zHL@eBnE[WLyOP_rLql+5oOHxXfiU4}wfe_EO0Wq|3FawL' );
define( 'NONCE_KEY',        'q,RY&{v*8[K>Civ=vqq}!|j, [J)tLU&Mm[R,_h |Izi,KYJ2g:%HlXLSbT#_|?&' );
define( 'AUTH_SALT',        '/!Q-f0xYTJv6%5mV{wIKa3Xy<9xij8>Zmkx9S_Qs&,2X31RdO|M%Kvn{rsm9j}:T' );
define( 'SECURE_AUTH_SALT', '0oeS#J%Cy2l|:!3a*t!zV~Nt5i+5_+wj|7Y8QaS`pPIJHj(cXFpSp^^.Zp8GvEUh' );
define( 'LOGGED_IN_SALT',   'KYME^t(&+fQd1vnsRvS$bkWM:DZ92%$E!) 3*]Fk|3J<N5uB*vSlU# =b-jcvf$ ' );
define( 'NONCE_SALT',       '->u0D$F~?7/-@DmKzU$_eLD&! woyD:BaW?x)}Q_E2@+AI!3?ZC=wr1Mvn~$tI9H' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
define('FS_METHOD','direct');
